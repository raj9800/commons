package com.nswbc.commons.core.services.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.commons.core.configuration.products.ModernAwardsAPIConfigurationsInstance;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.ModernAwardsListService;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * ModernAwardsListServiceImpl Class.
 *
 * @author riccardo.teruzzi
 */
@Component(immediate = true, service = ModernAwardsListService.class)
public class ModernAwardsListServiceImpl implements ModernAwardsListService {

    /**
     * Constant for Logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ModernAwardsListServiceImpl.class);
    /**
     * Constant for status.
     */
    private static final String STATUS = "status";
    /**
     * Constant for Authorization.
     */
    private static final String AUTHORIZATION = "Authorization";

    /**
     * Constant for Subscription key.
     */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";

    /**
     * The constant AWARDS.
     */
    public static final String AWARDS = "awards";

    /**
     * private reference variable for my modern awards configuration.
     */
    @Reference
    private ModernAwardsAPIConfigurationsInstance modernAwardsConfig;

    /**
     * This method returns the modern awards list.
     */
    @Override
    public JsonObject getModernAwardsList(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        String token = RestUtil.getTokenFromHeader(request, "token");
        if (Objects.nonNull(token)) {
            Map<String, String> headerMap = setHeadersMap(token);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            StringBuilder builder = new StringBuilder();
            String modernAwardsAPI = modernAwardsConfig.getConfig().modernAwardsListAPI();
            HttpResponse restResponse = RestUtil.processRestGetRequest(client, modernAwardsAPI,
                    builder.append(Constants.Application.EMPTY), headerMap);
            if (Objects.nonNull(restResponse)
                    && !(restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED)) {
                if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED) {
                    RestUtil.closeHttpClient(client);
                    return RestUtil.getServerErrorResponse(response);
                } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED_ONE) {
                    RestUtil.closeHttpClient(client);
                    return RestUtil.getUnAuthorizedResponse(response);
                } else {
                    try {
                        String responseEntity = EntityUtils.toString(restResponse.getEntity());
                        JsonObject modernAwardsList = new JsonParser().parse(responseEntity).getAsJsonObject();
                        modernAwardsList.addProperty(STATUS, Constants.Number.ONE);
                        return modernAwardsList;
                    } catch (ParseException | IOException e) {
                        LOG.error("Unable to get JSON : ", e.getMessage());
                        return RestUtil.getServerErrorResponse(response);
                    } finally {
                        RestUtil.closeHttpClient(client);
                    }
                }
            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

    /**
     * @return header map in request.
     */
    private Map<String, String> setHeadersMap(String token) {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        headerMap.put(AUTHORIZATION, "Bearer " + token);
        headerMap.put(SUBSCRIPTION_KEY, modernAwardsConfig.getConfig().subscriptionKey());
        return headerMap;
    }

}
