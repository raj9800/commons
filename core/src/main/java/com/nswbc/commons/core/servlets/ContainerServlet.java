package com.nswbc.commons.core.servlets;

import java.io.IOException;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author yuvraj.bansal Servlet for container selector.
 *
 */
@Component(service = {javax.servlet.Servlet.class},
property = {"sling.servlet.methods=POST",
        "sling.servlet.resourceTypes=nswbc-commons/components/content/stickymenu",
        "sling.servlet.resourceTypes=nswbc-ba/components/structure/footer",
        "sling.servlet.resourceTypes=nswbc-first/components/structure/footer",
        "sling.servlet.resourceTypes=nswbc-commons/components/content/timeline",
        "sling.servlet.resourceTypes=nswbc-commons/components/content/category",
        "sling.servlet.resourceTypes=nswbc-businessnsw/components/structure/footer",
        "sling.servlet.selectors=container", "sling.servlet.extensions=html"})
public class ContainerServlet extends SlingAllMethodsServlet {

    /** private variable for serial version id. */
    private static final long serialVersionUID = 1924448541433958466L;

    /** private variable for logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(ContainerServlet.class);

    /** private variable for selector. */
    protected static final String SELECTOR = "container";

    /** private variable for extension. */
    protected static final String EXTENSION = "html";

    /** private constant variable for delete. */
    private static final String PARAM_DELETED_CHILDREN = "delete";

    /** private variable for order. */
    private static final String PARAM_ORDERED_CHILDREN = "order";

    /** private variable for 500. */
    private static final int FIVE_HUNDRED = 500;

    /**
     * @throws ServletException exception.
     * @throws IOException      exception. do post method.
     */
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        ResourceResolver resolver = request.getResourceResolver();
        Resource container = request.getResource();

        try {
            String[] deletedChildrenNames = request.getParameterValues(PARAM_DELETED_CHILDREN);
            if (deletedChildrenNames != null && deletedChildrenNames.length > 0) {
                for (String childName : deletedChildrenNames) {
                    Resource child = container.getChild(childName);
                    if (child != null) {
                        resolver.delete(child);
                    }
                }
            }
            resolver.commit();
        } catch (PersistenceException e) {
            LOGGER.error("Could not delete items of the container at {}: {}", container.getPath(), e);
            response.sendError(FIVE_HUNDRED);
        }

        try {
            String[] orderedChildrenNames = request.getParameterValues(PARAM_ORDERED_CHILDREN);
            if (orderedChildrenNames != null && orderedChildrenNames.length > 0) {
                Node containerNode = container.adaptTo(Node.class);
                if (containerNode != null) {

                    for (int i = 0; i < orderedChildrenNames.length; i++) {
                        if (!containerNode.hasNode(orderedChildrenNames[i])) {
                            containerNode.addNode(orderedChildrenNames[i]);
                        }
                    }

                    for (int i = orderedChildrenNames.length - 1; i >= 0; i--) {

                        if (i == orderedChildrenNames.length - 1) {
                            containerNode.orderBefore(orderedChildrenNames[i], null);
                        } else {
                            containerNode.orderBefore(orderedChildrenNames[i], orderedChildrenNames[i + 1]);
                        }
                    }

                    resolver.commit();
                }
            }
        } catch (RepositoryException | PersistenceException e) {
            LOGGER.error("Could not order items of the container at {}: {}", container.getPath(), e);
            response.sendError(FIVE_HUNDRED);
        }
    }
}
