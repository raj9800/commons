package com.nswbc.commons.core.services.impl;

import static com.nswbc.commons.core.models.modernawardslist.ModernAwardsListModel.PRODUCT_GUID;
import static com.nswbc.commons.core.services.impl.ModernAwardsListServiceImpl.AWARDS;
import static com.nswbc.commons.core.services.impl.ProductCheckGuidServiceImpl.PRODUCT_PAGE_TEMPLATE;
import static com.nswbc.commons.core.utils.ResourceUtils.getPageFilterTemplateAndProperty;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.commons.core.configuration.myaccount.MyAccountAPIConfigurationsInstance;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.MyAccountMemberDetailsService;
import com.nswbc.commons.core.services.ProductCheckGuidService;
import com.nswbc.commons.core.utils.ResourceUtils;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * @author yuvraj.bansal
 */
@Component(immediate = true, service = MyAccountMemberDetailsService.class)
public class MyAccountMemberDetailsServiceImpl implements MyAccountMemberDetailsService {

    /**
     * Constant for Logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(MyAccountMemberDetailsServiceImpl.class);
    /**
     * Constant for Subscription key.
     */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    /**
     * Constant for status.
     */
    private static final String STATUS = "status";
    /**
     * Constant for Authorization.
     */
    private static final String AUTHORIZATION = "Authorization";

    /**
     * The constant PRODUCT_ID.
     */
    public static final String BA_PRODUCT_VALUE = "_ba_product_value";

    /**
     * private reference variable for my account configuration.
     */
    @Reference
    private MyAccountAPIConfigurationsInstance myAccountConfig;

    /**
     * private reference variable for my modern awards check guid service
     * configuration.
     */
    @Reference
    private ProductCheckGuidService productCheckGuidService;

    /**
     * This method returns the member details.
     */
    @Override
    public JsonObject getMemberDetails(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        String token = RestUtil.getTokenFromHeader(request, "token");
        if (Objects.nonNull(token)) {
            Map<String, String> headerMap = setHeadersMap(token);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            StringBuilder builder = new StringBuilder();
            HttpResponse restResponse = RestUtil.processRestGetRequest(client,
                    myAccountConfig.getConfig().memberDetailsAPI(), builder.append(Constants.Application.EMPTY),
                    headerMap);
            if (Objects.nonNull(restResponse)
                    && !(restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED)) {
                if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED) {
                    RestUtil.closeHttpClient(client);
                    return RestUtil.getServerErrorResponse(response);
                } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED_ONE) {
                    RestUtil.closeHttpClient(client);
                    return RestUtil.getUnAuthorizedResponse(response);
                } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.TWO_HUNDRED_FOUR) {
                    RestUtil.closeHttpClient(client);
                    JsonObject resultJson = new JsonObject();
                    resultJson.addProperty(STATUS, Constants.Number.ZERO);
                    return resultJson;
                } else {
                    try {
                        String responseEntity = EntityUtils.toString(restResponse.getEntity());
                        JsonObject memberDetailsResult = new JsonParser().parse(responseEntity).getAsJsonObject();
                        addAwardsDataToMemberDetailsJson(request, memberDetailsResult);
                        memberDetailsResult.addProperty(STATUS, Constants.Number.ONE);
                        return memberDetailsResult;
                    } catch (ParseException | IOException e) {
                        LOG.error("Unable to register : ", e.getMessage());
                        return RestUtil.getServerErrorResponse(response);
                    } finally {
                        RestUtil.closeHttpClient(client);
                    }
                }
            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

    /**
     * Add awards data to member details json.
     *
     * @param request             the request
     * @param memberDetailsResult the member details result
     */
    private void addAwardsDataToMemberDetailsJson(SlingHttpServletRequest request, JsonObject memberDetailsResult) {
        if (memberDetailsResult.has(AWARDS)) {
            JsonArray listOfGuid = memberDetailsResult.getAsJsonArray(AWARDS);
            Page securePage = productCheckGuidService.getSecurePage(request.getResourceResolver());
            for (JsonElement jsonElement : listOfGuid) {
                JsonObject awardJson = jsonElement.getAsJsonObject();
                if (securePage != null && awardJson.has(BA_PRODUCT_VALUE)
                        && StringUtils.isNotBlank(awardJson.get(BA_PRODUCT_VALUE).getAsString())) {
                    String guid = awardJson.get(BA_PRODUCT_VALUE).getAsString();
                    PageFilter modernAwardPageFilter = getPageFilterTemplateAndProperty(PRODUCT_PAGE_TEMPLATE,
                            PRODUCT_GUID, guid);
                    Iterator<Page> pageIterator = securePage.listChildren(modernAwardPageFilter, true);
                    // We have just one page matching the guid
                    if (pageIterator.hasNext()) {
                        Page awardPage = pageIterator.next();
                        awardJson.addProperty("product_pagePath",
                                ResourceUtils.getResolvedPath(request.getResourceResolver(), awardPage.getPath()));
                        awardJson.addProperty("product_pageTitle", awardPage.getTitle());
                    }
                }
            }
        }
    }

    /**
     * @return header map in request.
     */
    private Map<String, String> setHeadersMap(String token) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        headerMap.put(SUBSCRIPTION_KEY, myAccountConfig.getConfig().subscriptionKey());
        headerMap.put(AUTHORIZATION, "Bearer " + token);
        return headerMap;
    }

}
