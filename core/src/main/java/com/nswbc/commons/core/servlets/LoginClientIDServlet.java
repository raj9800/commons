package com.nswbc.commons.core.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;

import com.google.gson.JsonArray;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;

import com.google.gson.JsonObject;
import com.nswbc.commons.core.configuration.login.LoginConfigurationInstance;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * @author yuvraj.bansal Servlet for osgi configuration
 */
@Component(
        service = Servlet.class,
        property = {
                "sling.servlet.paths=" + LoginClientIDServlet.SERVLET_PATH,
                "sling.servlet.extensions=" + Constants.Text.JSON,
                "sling.servlet.methods=GET"
        })
public class LoginClientIDServlet extends SlingSafeMethodsServlet {

    /** private generated serialVersionUID. */
    private static final long serialVersionUID = -8753725954417479694L;
    /** protected variable for servlet path. */
    protected static final String SERVLET_PATH = "/bin/nswbc/loginclient";
    /** private configuration list variable. */
    @Reference(service = LoginConfigurationInstance.class, cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
    private final List<LoginConfigurationInstance> configList = new ArrayList<LoginConfigurationInstance>();

    /**
     * do get method.
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        response.setCharacterEncoding(Constants.Application.UTF8);
        int count = Constants.Number.ZERO;
        String brand = request.getRequestPathInfo().getSelectorString();
        JsonObject jsonObj = new JsonObject();
        if (StringUtils.isEmpty(brand)) {
            jsonObj = RestUtil.getBadRequestResponse(response);
        } else {
            // Json array for the allowed redirect domains from all the configurations
            JsonArray allowedDomainArray = new JsonArray();
            for (LoginConfigurationInstance config : configList) {
                if (StringUtils.equals(config.getConfig().brandName(), brand)) {
                    jsonObj.addProperty("envHostname", config.getConfig().envHostname());
                    jsonObj.addProperty("apiHostname", config.getConfig().apiHostname());
                    jsonObj.addProperty("clientID", config.getConfig().clientID());
                    jsonObj.addProperty("authorityID", config.getConfig().authorityID());
                    jsonObj.addProperty("postRedirectHomePage", config.getConfig().postRedirectHomePage());
                    jsonObj.addProperty("hopPagePath", config.getConfig().hopPagePath());
                    count++;
                }
                //code for reading the allowed domains from all configs
                String[] allowedRedirectDomains = config.getConfig().allowedRedirectDomains();
                for (String domain : allowedRedirectDomains) {
                    if (StringUtils.isNotBlank(domain)) {
                        allowedDomainArray.add(domain);
                    }
                }
                jsonObj.add("allowedRedirectDomains", allowedDomainArray);
            }
        }
        if (count == Constants.Number.ZERO) {
            jsonObj = RestUtil.getBadRequestResponse(response);
        }
        response.getWriter().print(jsonObj);

    }

}
