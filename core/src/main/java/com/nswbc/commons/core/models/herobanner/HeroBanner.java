package com.nswbc.commons.core.models.herobanner;

import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.nswbc.commons.core.models.image.ImageModel;

/**
 * Benefits component sling model.
 *
 * @author rajat.pachouri
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-ba/components/content/membership-advantages",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class HeroBanner {

    /** private current resource object. */
    @Inject
    private Resource resource;

    /** private variable to show desktop image in carousel panel. */
    private ImageModel desktopimage;

    /** private variable to show mobile image in carousel panel. */
    private ImageModel mobileimage;

    /**
     * Getter method for desktopimage object.
     *
     * @return desktopimage object
     */
    public ImageModel getDesktopimage() {
        return desktopimage;
    }

    /**
     * Getter method for mobileimage object.
     *
     * @return mobileimage object
     */
    public ImageModel getMobileimage() {
        return mobileimage;
    }

    /**
     * heading.
     */
    @Inject
    @Via("resource")
    private String variant;

    /**
     * color of the text.
     */
    @Inject
    @Via("resource")
    private String alignment;

    /**
     * getter for variant.
     *
     * @return variant
     */
    public String getVariant() {
        return variant;
    }

    /**
     * getter for alignment.
     *
     * @return alignment
     */
    public String getAlignment() {
        return alignment;
    }

    /**
     * Post construct method to inject images.
     */
    @PostConstruct
    public void init() {
        if (!Objects.isNull(resource)) {
            Resource desktopimage = resource.getChild("desktopimage");
            if (!Objects.isNull(desktopimage)) {
                this.desktopimage = desktopimage.adaptTo(ImageModel.class);
            }
            Resource mobileimage = resource.getChild("mobileimage");
            if (!Objects.isNull(mobileimage)) {
                this.mobileimage = mobileimage.adaptTo(ImageModel.class);
            }
        }
    }

}
