package com.nswbc.commons.core.services;

/**
 * Interface for UsersSerive.
 *
 * @author rajat.pachouri
 *
 */
public interface UsersService {
    /**
     * This method should return the string of json object which contains the
     * details of the users.
     *
     * @param groupName the group name which will be used to collect the users.
     * @return json object which contains the users and their full names
     */
    String getUsers(String groupName);
}
