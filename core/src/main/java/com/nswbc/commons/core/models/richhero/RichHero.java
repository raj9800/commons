package com.nswbc.commons.core.models.richhero;

import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.nswbc.commons.core.models.image.ImageModel;

/**
 * @author rajat.pachouri. Image Model Class.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class RichHero {

    /** private variable for desktop image. */
    private ImageModel image;
    /** private variable for mobile image. */
    private ImageModel imagemobile;
    /** private variable for partof logo image. */
    private ImageModel partofimage;
    /** private current resource object. */
    @Inject
    private Resource resource;
    /** headline. */
    @Inject
    private String headline;
    /** subheading. */
    @Inject
    private String subheading;
    /** bgcolor of the component. */
    @Inject
    private String bgColor;
    /** variation of the component. */
    @Inject
    private String variation;

    /**
     * Post construct method to inject image.
     */
    @PostConstruct
    public void init() {
        if (!Objects.isNull(resource)) {
            // adding desktop image
            Resource image = resource.getChild("image");
            if (!Objects.isNull(image)) {
                this.image = image.adaptTo(ImageModel.class);
            }
            // adding mobile image
            image = resource.getChild("imagemobile");
            if (!Objects.isNull(image)) {
                this.imagemobile = image.adaptTo(ImageModel.class);
            }
            // adding mobile image
            image = resource.getChild("partofimage");
            if (!Objects.isNull(image)) {
                this.partofimage = image.adaptTo(ImageModel.class);
            }
        }
    }

    /**
     * Returns image.
     *
     * @return imageModel object
     */
    public ImageModel getImage() {
        return image;
    }

    /**
     * Returns mobile image.
     *
     * @return imageModel object
     */
    public ImageModel getMobileImage() {
        return imagemobile;
    }

    /**
     * Returns partOf Logo image.
     *
     * @return imageModel object
     */
    public ImageModel getPartofimage() {
        return partofimage;
    }

    /**
     * getter for headline.
     *
     * @return String.
     */
    public String getHeadline() {
        return headline;
    }

    /**
     * getter for variation.
     *
     * @return image or video.
     */
    public String getVariation() {
        return variation;
    }

    /**
     * getter for subheading.
     *
     * @return String.
     */
    public String getSubheading() {
        return subheading;
    }

    /**
     * getter for bg color for component.
     *
     * @return on of the brand colors.
     */
    public String getBgColor() {
        return bgColor;
    }
}
