package com.nswbc.commons.core.workflow.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.HistoryItem;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.Workflow;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.adobe.granite.workflow.model.WorkflowModel;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationOptions;
import com.day.cq.replication.ReplicationStatus;
import com.day.cq.replication.Replicator;
import com.day.cq.wcm.api.reference.Reference;
import com.day.cq.wcm.api.reference.ReferenceProvider;

/**
 * @author Yuvraj.Bansal This is a util class to get various sling resources
 */
public final class WorkflowUtils {
    /** Constant for Logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowUtils.class);
    /**
     * Constant for dynamic participant.
     */
    public static final String DYAMIC_PARTICIPANT = "DYNAMIC_PARTICIPANT";
    /**
     * Format of the date before it is converted to absolute time.
     */
    public static final String DATETIME_FORMAT = "dd/MM/yyyy - HH:mmZ";

    /**
     * default constructor.
     */
    private WorkflowUtils() {
    }

    /**
     * This method fetches the effective activation and deactivatiion date and the userID which gave those dates from the list of history items.
     * It does so by iterating through the history items in the reversed order and by grabbing the last occurrence
     * of the properties which stores activation and deactivation date.
     * Unless given by initiator, the date is only considered valid if it is accompanied by a property which denotes
     * it has been intentionally overridden.
     * @param list the list of the history items.
     * @param key the key which signifies it has been overriden.
     * @param dateProp name of the property
     * @return date and the user id of the participant who gave those dates.
     */
    public static String geteffectiveUserAndDateFromHistory(List<HistoryItem> list, String key, String dateProp) {
        String dateTime = StringUtils.EMPTY;
        String assignee = StringUtils.EMPTY;
        if (null != list && !list.isEmpty()) {
            Collections.reverse(list);
            for (HistoryItem hi : list) {
                if (hi.getWorkItem().getNode().getType().equals("DYNAMIC_PARTICIPANT")) {
                    MetaDataMap map = hi.getWorkItem().getMetaDataMap();
                    if (map.containsKey(key) && map.containsKey(dateProp)) {
                        dateTime = map.get(dateProp, String.class);
                        assignee = hi.getWorkItem().getCurrentAssignee();
                        break;
                    }
                }
            }
            //if no overriden dates have been found, grab the dates given by initiator
            if (StringUtils.isBlank(dateTime)) {
                HistoryItem hi = list.get(list.size() - 2);
                MetaDataMap map = hi.getWorkItem().getMetaDataMap();
                if (map.containsKey(dateProp)) {
                    dateTime = map.get(dateProp, String.class);
                    assignee = hi.getWorkItem().getCurrentAssignee();
                }
            }
            Collections.reverse(list);
        }
        return dateTime + "|" + assignee;
    }

    /**
     * This method is used to return the user that were chosen from ba-approver or ba-publisher during the page activation workflow process.
     * the method checks the last occurrence of user2 or user3 property to get the value
     * @param list the list of history items.
     * @return the user id
     */
    public static String getAssignToUserFromLastItems(List<HistoryItem> list) {
        String value = StringUtils.EMPTY;
        if (null != list && !list.isEmpty()) {
            Collections.reverse(list);
            for (HistoryItem hi : list) {
                if (hi.getWorkItem().getNode().getType().equals("DYNAMIC_PARTICIPANT")) {
                    MetaDataMap map = hi.getWorkItem().getMetaDataMap();
                    if (map.containsKey("user2")) {
                        value = map.get("user2", String.class);
                        break;
                    }
                    if (map.containsKey("user3")) {
                        value = map.get("user3", String.class);
                        break;
                    }
                }
            }
            Collections.reverse(list);
        }
        return value;
    }

    /**
     * Returns the ID of the user that the last participant step was assigned to.
     * @param list the list of the history items
     * @return the user id of the user.
     */
    public static String getLastAssigneeFromHistory(List<HistoryItem> list) {
        String assignee = StringUtils.EMPTY;
        if (null != list && !list.isEmpty()) {
            Collections.reverse(list);
            for (HistoryItem hi : list) {
                if (hi.getWorkItem().getNode().getType().equals("DYNAMIC_PARTICIPANT")) {
                    assignee = hi.getWorkItem().getCurrentAssignee();
                    break;
                }
            }
            Collections.reverse(list);
        }
        return assignee;
    }
    /**
     * returns a hashset of all the users who have participated in a workflow at any step.
     * Note: hashset is used to remove duplicate values cause a participant may take part multiple times in a workflow.
     * @param list the list of history items.
     * @return Hashset of all the users that participated in a workflow
     */
    public static HashSet<String> getAllAssigneeFromHistory(List<HistoryItem> list) {
        HashSet<String> usersSet = null;
        List<String> usersList = new ArrayList<String>();
        if (null != list && !list.isEmpty()) {
            Collections.reverse(list);
            for (HistoryItem hi : list) {
                if (hi.getWorkItem().getNode().getType().equals("DYNAMIC_PARTICIPANT")) {
                    String assignee  = StringUtils.EMPTY;
                    assignee = hi.getWorkItem().getCurrentAssignee();
                    usersList.add(assignee);
                }
            }
            Collections.reverse(list);
        }
        if (!usersList.isEmpty()) {
            usersSet = new HashSet<String>(usersList);
        }
        return usersSet;
    }
    /**
     * Splits the string based on a separator and return an array.
     * @param value the string
     * @return array of separated strings.
     */
    public static String[] splitDetails(String value) {
        String[] values = value.split("\\|");
        return values;
    }
    /**
     * This method converts the given map of args to list which contains args.
     * @param metaDataMap the metadata map given by workflow step
     * @return list of args
     */
    public static List<String> getArgsArray(MetaDataMap metaDataMap) {
        List<String> args = new ArrayList<String>();
        String argsString =  StringUtils.EMPTY;
        if (metaDataMap.containsKey("PROCESS_ARGS")) {
            argsString = metaDataMap.get("PROCESS_ARGS", String.class);
        }
        if (StringUtils.isNotBlank(argsString)) {
            args = Arrays.asList(argsString.split(","));
        }
        return args;
    }
    /**
     * This method returns the list of the history items of a worflow instance.
     * @param wfsession the workflow session object
     * @param workitem the workitem.
     * @return workflow history list
     */
    public static List<HistoryItem> getWorkflowHistoryList(WorkflowSession wfsession, WorkItem workitem) {
        List<HistoryItem> list = null;
        if (null != wfsession && null != workitem && null != workitem.getWorkflow()) {
            try {
                list = wfsession.getHistory(workitem.getWorkflow());
            } catch (WorkflowException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }
        return list;
    }
    /**
     * This method is used to get the absolute time from the given date string based on the format <tt>DATETIME_FORMAT</tt>.
     * @param datetime
     * @return absoluteTime
     */
    public static String getAbsoluteTime(String datetime) {
        System.out.println(datetime.lastIndexOf(":"));
        datetime = charRemoveAt(datetime, datetime.lastIndexOf(":"));
        String absoluteTime  = StringUtils.EMPTY;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATETIME_FORMAT);
            Date date = simpleDateFormat.parse(datetime);
            absoluteTime = Long.toString(date.getTime());
        } catch (ParseException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return absoluteTime;
    }
    /**
     * This method is used to get the value of the last occurrence of a property from list of history items.
     * @param list the history items list
     * @param prop the name of the property
     * @return the value of the property
     */
    public static String getPropertyFromHistory(List<HistoryItem> list, String prop) {
        String value = StringUtils.EMPTY;
        if (null != list && !list.isEmpty()) {
            Collections.reverse(list);
            for (HistoryItem hi : list) {
                if (hi.getWorkItem().getNode().getType().equals("DYNAMIC_PARTICIPANT")) {
                    MetaDataMap map = hi.getWorkItem().getMetaDataMap();
                    if (map.containsKey(prop)) {
                        value = map.get(prop, String.class);
                        break;
                    }
                }
            }
            Collections.reverse(list);
        }
        return value;
    }
    /**
     * This method is used to remove a char from a string at a given index.
     * @param str the actual string
     * @param atIndex the char
     * @return the updated string with removed char.
     */
    private static String charRemoveAt(String str, int atIndex) {
        return str.substring(0, atIndex) + str.substring(atIndex + 1);
    }

    /**
     * This method return all the outdated and unpublished assets which are referenced on a page.
     * those assets are activated later as a part of the workflow.
     * @param resolver the resource resolver
     * @param payload the payload
     * @param referenceProviders list of various referenceProviders
     * @return list of references if they are outdated or unpublished
     */
    public static List<Reference> getOutdatedAndUnpublishedReferences(ResourceResolver resolver,
            String payload, List<ReferenceProvider> referenceProviders) {
        List<Reference> refsToPublish = new ArrayList<Reference>();
        List<Reference> allReferences = new ArrayList<Reference>();
        Resource plResource = null;
        plResource = resolver.getResource(payload + "/" + "jcr:content");
        if (plResource == null) {
            plResource = resolver.getResource(payload);
        }
        for (ReferenceProvider referenceProvider : referenceProviders) {
            allReferences.addAll(referenceProvider.findReferences(plResource));
        }
        for (Reference ref: allReferences) {
            boolean published = false;
            boolean outdated = false;
            long lastPublished = 0L;
            Resource resource = ref.getResource();
            //boolean canReplicate = canReplicate(reference.getResource().getPath(), session);
            if (resource != null) {
                ReplicationStatus replStatus = (ReplicationStatus) resource.adaptTo(ReplicationStatus.class);
                if (replStatus != null) {
                    published = (replStatus.isDelivered()) || (replStatus.isActivated());
                    if (published) {
                        lastPublished = replStatus.getLastPublished().getTimeInMillis();
                        outdated = lastPublished < ref.getLastModified();

                    }
                }
            }
            if ((!published) || (outdated)) {
                refsToPublish.add(ref);
                LOGGER.debug(ref.getType() + "||" + ref.getResource().getPath() + "||" + ref.getName());
            }
        }
        return refsToPublish;
    }

    /**
     * This method is used to scheduler activation or deactivation at scheduled time.
     * @param wfsession the workflow session obeject
     * @param worklflowModel the workflow model to be used
     * @param time absolute time
     * @param payload payload path
     */
    public static void scheduleActivationAndDeactivation(WorkflowSession wfsession, String worklflowModel, String time, String payload) {
        try {
            WorkflowModel model = wfsession.getModel(worklflowModel);
            if (model != null) {
                WorkflowData data = wfsession.newWorkflowData("JCR_PATH", payload);
                Map<String, Object> metaData = new HashMap<String, Object>();
                metaData.put("absoluteTime", time);
                Workflow workflow = wfsession.startWorkflow(model, data, metaData);
                LOGGER.debug(workflow.getId());
            }
        } catch (WorkflowException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    /**
     * This method is used to activate or deactivate the page immediately.
     * @param session The session object
     * @param replicator The replicator object
     * @param activationType type of the activation, i.e ACTIVATE or DEACTIVATE
     * @param payload payload to subject to activation
     * @param opts activation object that can be configured
     */
    public static void scheduleImmediately(Session session, Replicator replicator, ReplicationActionType activationType,
            String payload, ReplicationOptions opts) {
        try {
            replicator.replicate(session, activationType, payload, opts);
        } catch (ReplicationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
