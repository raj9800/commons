package com.nswbc.commons.core.utils;

import static com.day.cq.wcm.api.NameConstants.NN_TEMPLATE;
import static com.nswbc.commons.core.constants.Constants.Application.CQ_TAGS;
import static com.nswbc.commons.core.constants.Constants.Application.EMPTY;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.jcr.Session;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.Template;
import com.day.cq.wcm.api.policies.ContentPolicy;
import com.day.cq.wcm.api.policies.ContentPolicyManager;
import com.day.cq.wcm.api.policies.ContentPolicyMapping;
import com.nswbc.commons.core.constants.Constants;

/**
 * The type Resource utils.
 *
 * @author Yuvraj.Bansal This is a util class to get various sling resources
 */
public final class ResourceUtils {
    /**
     * Constant for Read Write Service.
     */
    private static final String READ_WRITE_SERVICE = "readWriteService";
    /**
     * Constant for Logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ResourceUtils.class);

    /**
     * default constructor.
     */
    private ResourceUtils() {
    }

    /**
     * This method returns resource node using resource type.
     *
     * @param resolver     resourceResolver
     * @param path         path of the page
     * @param resourceType resourceType of the comp
     * @return resource
     */
    public static Resource getResourceByResourceType(ResourceResolver resolver, String path, String resourceType) {
        Resource finalResource = null;
        Resource root = resolver.getResource(path);
        finalResource = iterateThroughResourceByResourceType(finalResource, root, resourceType);
        if (!finalResource.isResourceType(resourceType)) {
            return null;
        }
        return finalResource;

    }

    /**
     * iterate through resources recursively and returns resource based on resource
     * type.
     *
     * @param finalResource final resource
     * @param root          root resource
     * @param resourceType  resourcetype
     * @return resource
     */
    private static Resource iterateThroughResourceByResourceType(Resource finalResource, Resource root,
            String resourceType) {
        if (root.isResourceType(resourceType)) {
            finalResource = root;
        }
        LOG.error(root.getPath());
        for (Resource child : root.getChildren()) {
            LOG.error(child.getPath());
            if (child.isResourceType(resourceType)) {
                finalResource = child;
            } else if (child.hasChildren()) {
                finalResource = iterateThroughResourceByResourceType(finalResource, child, resourceType);
            }
        }

        return finalResource;
    }

    /**
     * Gets session.
     *
     * @param rFactory the r factory
     * @return session This method is used to get the the session by providing the
     *         resource resolver factory.
     */
    public static Session getSession(final ResourceResolverFactory rFactory) {
        Session session = null;
        ResourceResolver resolver = null;
        Map<String, Object> param = getServiceParams();
        try {
            resolver = rFactory.getServiceResourceResolver(param);
            session = resolver.adaptTo(Session.class);
        } catch (LoginException e) {
            LOG.error(e.getMessage(), e);
        }
        return session;
    }

    /**
     * Gets resource resolver.
     *
     * @param rFactory the r factory
     * @return resource resolver This method is used to get the resource resolver by
     *         providing the resource resolver factory.
     */
    public static ResourceResolver getResourceResolver(final ResourceResolverFactory rFactory) {
        ResourceResolver resolver = null;
        Map<String, Object> param = getServiceParams();
        try {
            resolver = rFactory.getServiceResourceResolver(param);
        } catch (LoginException e) {
            LOG.error(e.getMessage(), e);
        }
        return resolver;
    }

    /**
     * Gets resource.
     *
     * @param rFactory the r factory
     * @param path     the path
     * @return resource based on path This method is used to provide the resource by
     *         providing the resource resolver factory and path as parameter.
     */
    public static Resource getResource(final ResourceResolverFactory rFactory, final String path) {
        ResourceResolver resolver = null;
        Resource resource = null;
        Map<String, Object> param = getServiceParams();
        try {
            resolver = rFactory.getServiceResourceResolver(param);
            resource = resolver.getResource(path);
        } catch (LoginException e) {
            LOG.error(e.getMessage(), e);
        }
        return resource;
    }

    /**
     * Gets resolved path.
     *
     * @param resourceResolver the resource resolver
     * @param path             the path
     * @return resolved path This method is used to provide resolved path from
     *         resource resolver.
     */
    public static String getResolvedPath(ResourceResolver resourceResolver, String path) {
        if (Objects.nonNull(path) && Objects.nonNull(resourceResolver)) {
            if (path.startsWith("/content/dam") && FilenameUtils.indexOfExtension(path) > 0) {
                return path;
            } else if (path.startsWith(Constants.Application.NSWBC_CONTENT_ROOT)) {
                PageManager manager = resourceResolver.adaptTo(PageManager.class);
                if (Objects.nonNull(manager)) {
                    Page page = manager.getPage(path);
                    if (Objects.nonNull(page)) {
                        path = getURL(resourceResolver, page.getPath() + Constants.Extensions.HTML_EXTENSION);
                    }
                }
            }

        }
        return path;
    }

    /**
     * Gets url.
     *
     * @param resolver the resolver
     * @param pagePath the page path
     * @return This method returns mapped page path.
     */
    public static String getURL(ResourceResolver resolver, String pagePath) {
        if (StringUtils.isNotEmpty(pagePath)) {
            return resolver.map(pagePath);
        }
        return null;
    }

    /**
     * @return params
     */
    private static Map<String, Object> getServiceParams() {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put(ResourceResolverFactory.SUBSERVICE, READ_WRITE_SERVICE);
        return param;
    }

    /**
     * Gets url.
     *
     * @param resolver the resolver
     * @param page     the page
     * @return This method returns the url of a page with extension.
     */
    public static String getURL(ResourceResolver resolver, Page page) {
        String vanityURL = page.getVanityUrl();
        return resolver.map(StringUtils.isEmpty(vanityURL) ? page.getPath() : vanityURL);
    }

    /**
     * Gets url.
     *
     * @param request the request
     * @param page    the page
     * @return This method returns the url of a page with extension.
     */
    public static String getURL(SlingHttpServletRequest request, Page page) {
        String vanityURL = page.getVanityUrl();
        return request.getResourceResolver()
                .map(StringUtils.isEmpty(vanityURL) ? request.getContextPath() + page.getPath()
                        : request.getContextPath() + vanityURL);
    }

    /**
     * Gets policy details.
     *
     * @param request      the request
     * @param resourceType the resource type
     * @return This method returns the policy details associated with a given
     *         resource type.
     */
    public static ValueMap getPolicyDetails(SlingHttpServletRequest request, String resourceType) {
        Page currentPage = null;
        ValueMap contentPolicyProperties = new ValueMapDecorator(new HashMap<>());
        PageManager pageManager = request.getResource().getResourceResolver().adaptTo(PageManager.class);
        if (pageManager != null) {
            currentPage = pageManager.getContainingPage(request.getResource().getPath());
        }
        Template template = currentPage.getTemplate();
        if (template != null) {
            Resource templateResource = request.getResourceResolver().getResource(template.getPath());
            if (templateResource != null) {
                ContentPolicyManager contentPolicyManager = templateResource.getResourceResolver()
                        .adaptTo(ContentPolicyManager.class);
                List<ContentPolicy> contentPolicyList = contentPolicyManager.getPolicies(templateResource.getPath(),
                        resourceType);
                for (ContentPolicy contentPolicy : contentPolicyList) {
                    List<ContentPolicyMapping> cm = contentPolicyManager.getPolicyMappings(contentPolicy);
                    for (ContentPolicyMapping comPolicyMap : cm) {
                        if (comPolicyMap.getTemplate().getPath().equals(template.getPath())) {
                            contentPolicyProperties = contentPolicy.getProperties();
                        }
                    }
                }
            }
        }
        return contentPolicyProperties;
    }

    /**
     * This section populates brand ID for page analytics.
     *
     * @param currentPage the current page
     * @return brand string
     */
    public static String populateBrand(Page currentPage) {
        String brand = StringUtils.EMPTY;
        Resource pageResource = currentPage.adaptTo(Resource.class);
        if (Objects.nonNull(pageResource)) {
            InheritanceValueMap inheritanceValueMap = new HierarchyNodeInheritanceValueMap(pageResource);
            brand = inheritanceValueMap.getInherited("brandName", StringUtils.EMPTY);
        }
        return brand;
    }

    /**
     * Gets Page Filter.
     * <p>
     * Property Value can be empty. In this case will be only evaluated that the
     * property is not blank
     *
     * @param filterTemplatePath the filter template path
     * @param property           the property to check
     * @param propertyValue      the property value to check
     * @return the Page filter
     */
    public static PageFilter getPageFilterTemplateAndProperty(final String filterTemplatePath, final String property,
            final String propertyValue) {
        return new PageFilter() {
            @Override
            public boolean includes(Page p) {
                if (p != null) {
                    ValueMap props = p.getProperties();
                    String templatePath = props.get(NN_TEMPLATE, String.class);
                    if (StringUtils.isNotBlank(propertyValue)) {
                        return filterTemplatePath != null && filterTemplatePath.equals(templatePath)
                                && props.containsKey(property)
                                && StringUtils.equals(props.get(property, String.class), propertyValue);
                    } else {
                        return filterTemplatePath != null && filterTemplatePath.equals(templatePath)
                                && props.containsKey(property)
                                && StringUtils.isNotBlank(props.get(property, String.class));
                    }
                }
                return false;
            }
        };
    }

    /**
     * Gets Page Filter.
     * <p>
     * Property Value can be empty. In this case will be only evaluated that the
     * property is not blank
     *
     * @param filterTemplatePattern the filter template path pattern
     * @param property              the property to check
     * @param propertyValue         the property value to check
     * @return the Page filter
     */
    public static PageFilter getPageFilterTemplatePatternAndProperty(final String filterTemplatePattern,
            final String property, final String propertyValue) {
        return new PageFilter() {
            @Override
            public boolean includes(Page p) {
                if (p != null) {
                    ValueMap props = p.getProperties();
                    String templatePath = props.get(NN_TEMPLATE, String.class);
                    if (StringUtils.isNotBlank(propertyValue)) {
                        return templatePath != null && StringUtils.contains(templatePath, filterTemplatePattern)
                                && props.containsKey(property)
                                && StringUtils.equals(props.get(property, String.class), propertyValue);
                    } else {
                        return templatePath != null && StringUtils.contains(templatePath, filterTemplatePattern)
                                && props.containsKey(property)
                                && StringUtils.isNotBlank(props.get(property, String.class));
                    }
                }
                return false;
            }
        };
    }

    /**
     * Gets Page Filter.
     * <p>
     * Property Value can be empty. In this case will be only evaluated that the
     * property is not blank
     *
     * @param filterTemplatePattern the filter template path
     * @param tagsToCompare         the tags to compare
     * @return the Page filter
     */
    public static PageFilter getPageFilterTemplateAndTags(final String filterTemplatePattern,
            final Set<String> tagsToCompare) {
        return new PageFilter() {
            @Override
            public boolean includes(Page p) {
                if (p != null) {
                    ValueMap props = p.getProperties();
                    String templatePath = props.get(NN_TEMPLATE, String.class);
                    String[] tags = (String[]) props.getOrDefault(CQ_TAGS, new String[0]);
                    Set<String> tagsSet = new HashSet<>(Arrays.asList(tags));
                    tagsSet.retainAll(tagsToCompare);
                    return StringUtils.contains(templatePath, filterTemplatePattern) && tagsSet.size() > 0;
                }
                return false;
            }
        };
    }

    /**
     * Gets page list based on page filter.
     *
     * @param page       the page
     * @param pageFilter the page filter
     * @param descendant the descendant
     * @return the page list
     */
    public static List<Page> getListOfPageFromIterator(Page page, PageFilter pageFilter, boolean descendant) {
        Iterator<Page> pageIterator = page.listChildren(pageFilter, descendant);
        List<Page> pageList = new ArrayList<>();
        while (pageIterator.hasNext()) {
            Page next = pageIterator.next();
            pageList.add(next);
        }
        return pageList;
    }

    /**
     * Gets rendition path.
     *
     * @param resourceResolver  the resource resolver
     * @param originalImagePath the original image path
     * @param rendition         the rendition
     * @return the rendition path
     */
    public static String getRenditionPath(ResourceResolver resourceResolver, String originalImagePath,
            String rendition) {
        if (StringUtils.isNotBlank(originalImagePath) && StringUtils.isNotBlank(rendition)) {
            Resource resource = resourceResolver.getResource(originalImagePath);
            if (resource != null) {
                Asset asset = resource.adaptTo(Asset.class);
                if (asset != null) {
                    String mimeType = asset.getMimeType();
                    String extension = EMPTY;
                    String[] split = mimeType.split("/");
                    if (split.length > 1) {
                        extension = "." + split[1];
                    }
                    Rendition renditionImage = asset.getRendition(rendition.concat(extension));
                    if (renditionImage != null) {
                        return renditionImage.getPath();
                    }
                }
            }
        }
        return EMPTY;
    }

}
