package com.nswbc.commons.core.services.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.commons.core.configuration.emailsubscribe.SubscriptionContentAPIServiceConfigurationInstance;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.SubscriptionEmailSubscriptionService;
import com.nswbc.commons.core.utils.RestUtil;
import com.nswbc.commons.core.utils.ServerSideValidationUtil;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = SubscriptionEmailSubscriptionService.class)
public class SubscriptionEmailSubscriptionServiceImpl implements SubscriptionEmailSubscriptionService {
    /** Constant for Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(SubscriptionEmailSubscriptionServiceImpl.class);
    /** Constant for Subscription key. */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    /** Constant for status. */
    private static final String STATUS = "status";
    /**
     * private reference for gated content configuration.
     */
    @Reference
    private SubscriptionContentAPIServiceConfigurationInstance gatedContentConfig;

    /**
     * return JsonObject of status. This method makes a post call to integration api
     * and get the response as 200 or 400 status code.
     */
    @Override
    public JsonObject getToken(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        JsonObject bodyJson = RestUtil.getBodyJson(request);
        if (!bodyJson.isJsonNull()) {
            boolean validate = ServerSideValidationUtil.checkEmail(bodyJson.get("email").getAsString());
            if (validate) {
                Map<String, String> headerMap = setHeadersMap();
                CloseableHttpClient client = HttpClientBuilder.create().build();
                HttpResponse restResponse = RestUtil.processRestPostRequest(client, gatedContentConfig.getConfig().subscriptionAPI(), headerMap,
                        bodyJson);
                if (Objects.nonNull(restResponse) && !(restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED)) {
                    if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED) {
                        return RestUtil.getServerErrorResponse(response);
                    }
                    try {
                        String responseEntity = EntityUtils.toString(restResponse.getEntity());
                        client.close();
                        JsonObject emailSubscriptionAPIResult = new JsonParser().parse(responseEntity).getAsJsonObject();
                        emailSubscriptionAPIResult.addProperty(STATUS, Constants.Number.ONE);
                        return emailSubscriptionAPIResult;
                    } catch (ParseException | IOException e) {
                        LOG.error("Unable to register : ", e.getMessage());
                        return RestUtil.getServerErrorResponse(response);
                    }

                }
            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

    /**
     * @return header map in request.
     */
    private Map<String, String> setHeadersMap() {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        headerMap.put(SUBSCRIPTION_KEY, gatedContentConfig.getConfig().subscriptionKey());
        return headerMap;
    }

}
