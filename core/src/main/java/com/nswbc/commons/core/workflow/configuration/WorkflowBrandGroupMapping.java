package com.nswbc.commons.core.workflow.configuration;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Interface for configuration.
 *
 * @author rajat.pachouri
 *
 *
 */
@ObjectClassDefinition(name = "NSWBC: Workflow Brand and User Group Mapping",
        description = "This mapping is used to get group names related to a brand")
public @interface WorkflowBrandGroupMapping {
    /**
     * @return brandID
     */
    @AttributeDefinition(name = "Brand Id", description = "Provide the brand ID. E.g. first for sydney first")
    String brandID();

    /**
     * @return approverGroup
     */
    @AttributeDefinition(name = "Approver group", description = "Name of the group of the approver for this brand.")
    String approverGroup();

    /**
     * @return publisherGroup
     */
    @AttributeDefinition(name = "Publisher group", description = "Name of the group of the approver for this brand.")
    String publisherGroup();

    /**
     * @return webconsole namehint
     */
    String webconsole_configurationFactory_nameHint() default "Brand:<b>{brandID}</b>|<i>{approverGroup}</i>|<i>{publisherGroup}</i>";
}
