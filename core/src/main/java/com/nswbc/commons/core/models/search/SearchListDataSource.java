package com.nswbc.commons.core.models.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.adobe.acs.commons.genericlists.GenericList;
import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.nswbc.commons.core.constants.Constants;

/**
 * @author yuvraj.bansal
 *
 */
@Model(adaptables = SlingHttpServletRequest.class,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class SearchListDataSource {
    /**
     * Resource resolver object.
     */
    @Inject
    private ResourceResolver resourceResolver;

    /**
     * private sling request object.
     */
    @Self
    private SlingHttpServletRequest request;

    /**
     * Init method for this model class.
     */
    @PostConstruct
    private void initModel() {
        PageManager pageMgr = resourceResolver.adaptTo(PageManager.class);
        Resource categoryResource = request.getRequestPathInfo().getSuffixResource();
        if (Objects.nonNull(categoryResource)) {
            Page page = pageMgr.getContainingPage(categoryResource);
            InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(page.getContentResource());
            String brandName = ivm.getInherited("brandName", String.class);
            Page genericListPage = pageMgr.getPage(Constants.Application.SEARCH_CATEGORIES_LIST_PATH + Constants.Application.HYPHEN + brandName);
            if (Objects.nonNull(genericListPage)) {
                GenericList list = genericListPage.adaptTo(GenericList.class);
                if (list != null) {
                    List<Resource> searchCategoryList = new ArrayList<Resource>();
                    for (GenericList.Item item : list.getItems()) {
                        ValueMap vm = new ValueMapDecorator(new HashMap<String, Object>());
                        vm.put("value", item.getValue());
                        vm.put("text", item.getTitle());
                        searchCategoryList.add(new ValueMapResource(resourceResolver, new ResourceMetadata(), "nt:unstructured", vm));
                    }
                    DataSource ds = new SimpleDataSource(searchCategoryList.iterator());
                    request.setAttribute(DataSource.class.getName(), ds);
                }
            }
        }
    }
}
