package com.nswbc.commons.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author yuvraj.bansal
 *
 */
public interface MyAccountChangePasswordService {
    /**
     * @param request
     * @param response
     * @return change password
     */
    JsonObject changePassword(SlingHttpServletRequest request, SlingHttpServletResponse response);

}
