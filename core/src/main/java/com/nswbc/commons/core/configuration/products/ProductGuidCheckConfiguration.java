package com.nswbc.commons.core.configuration.products;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * ProductGuidCheckConfiguration Class.
 *
 * @author riccardo.teruzzi
 */
@ObjectClassDefinition(name = "Products/Modern Awards Configurations", description = "Service Configuration")
public @interface ProductGuidCheckConfiguration {

    /**
     * @return securePaths
     */
    @AttributeDefinition(name = "Secure Path", description = "Secure Path")
    String securePath();

}
