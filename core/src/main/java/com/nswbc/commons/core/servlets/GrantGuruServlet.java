package com.nswbc.commons.core.servlets;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.JsonObject;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.GrantGuruService;

/**
 * This servlet is used to get the redirect link to grant guru product if the
 * author is logged in.
 *
 * @author rajat.pachouri
 *
 */
@Component(service = Servlet.class, property = {"sling.servlet.selectors=" + GrantGuruServlet.URL,
        "sling.servlet.paths=" + GrantGuruServlet.SERVLET_PATH, "sling.servlet.extensions=" + Constants.Text.JSON,
        "sling.servlet.methods=GET", "sling.servlet.methods=POST"})
public class GrantGuruServlet extends SlingAllMethodsServlet {

    /** generated serial version uid. */
    private static final long serialVersionUID = 7865632056267603993L;
    /** protected variable for servlet path. */
    protected static final String SERVLET_PATH = "/bin/nswbc/grantguru";
    /** protected selector variable for url selector. */
    protected static final String URL = "url";

    /**
     * Grant guru service.
     */
    @Reference
    private GrantGuruService grantGuruService;

    /**
     * doGetMethod to call grant guru token api.
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        String selector = request.getRequestPathInfo().getSelectorString();
        if (selector.equals(URL)) {
            JsonObject result = grantGuruService.getGrantGuruURI(request, response);
            printResponseResult(response, result);
        }
    }

    /**
     * @param response
     * @param result
     * @throws IOException This method checks the output result
     */
    private void printResponseResult(SlingHttpServletResponse response, JsonObject result) throws IOException {
        if (Objects.nonNull(result)) {
            response.getWriter().print(result);
        }
    }

}
