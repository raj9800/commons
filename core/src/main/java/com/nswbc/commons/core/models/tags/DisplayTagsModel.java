package com.nswbc.commons.core.models.tags;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * @author yuvraj.bansal. DisplayTagsModel Class
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/tagDisplay",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class DisplayTagsModel {

    /** private sling object for resource resolver. */
    @SlingObject
    private ResourceResolver resourceResolver;

    /** Script Variable for servlet request. */
    @Self
    private SlingHttpServletRequest request;

    /** Script Variable for current page. */
    @ScriptVariable
    @Optional
    private Page currentPage;

    /** private arrayList variable for tagTitles. */
    private List<Tag> tagList;

    /**
     * private inject variable to display tags.
     */
    @Inject
    @Via("resource")
    @Default(values = "false")
    private String displayTags;
    /**
     * private variable for tagSearchPage.
     */
    private String tagSearchPage;

    /**
     * init model method.
     */
    @PostConstruct
    private void init() {
        tagList = new ArrayList<Tag>();
        TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
        String[] tagArray = (String[]) currentPage.getContentResource().getValueMap()
                .get(Constants.Application.CQ_TAGS);
        if (Objects.nonNull(tagArray)) {
            for (int i = 0; i < tagArray.length; i++) {
                Tag tag = tagManager.resolve(tagArray[i]);
                if (Objects.nonNull(tag)) {
                    tagList.add(tag);
                }
            }
        }

        ValueMap policyMap = ResourceUtils.getPolicyDetails(request, Constants.Application.DISPLAYTAG_RESOURCE_TYPE);
        tagSearchPage = policyMap.get("tagSearchPage", StringUtils.EMPTY);
        tagSearchPage = ResourceUtils.getResolvedPath(resourceResolver, tagSearchPage);
    }

    /**
     * @return displayTag It is used to check if tags has to be displayed on page.
     */
    public String getDisplayTags() {
        return displayTags;
    }

    /**
     * @return tagSearchPage
     */
    public String getTagSearchPage() {
        return tagSearchPage;
    }

    /**
     * @return tagList It return the list of tag titles.
     */
    public List<Tag> getCurrentPageTagList() {
        return tagList;
    }

}
