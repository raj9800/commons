package com.nswbc.commons.core.models.utils;

import java.util.Objects;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.wcm.core.components.models.ListItem;

/**
 * @author yuvraj.bansal
 *
 */
public class ComponentSelectorResourceItems implements ListItem {

    /** private variable for title. */
    private String title;
    /** private variable for name. */
    private String name;
    /** private variable for isParExist. */
    private boolean isParExist;


    /**
     * @param request.
     * @param resource.
     */
    public ComponentSelectorResourceItems(SlingHttpServletRequest request, Resource resource) {
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        if (valueMap != null) {
            this.title = valueMap.get("cq:panelTitle", String.class);
        }
        this.name = resource.getName();
        if (Objects.nonNull(resource.getParent().getChild("par/par_" + resource.getName()))) {
            this.isParExist = true;
        }
    }

    /**
     * @return this.title.
     */
    @Override
    public String getTitle() {
        return this.title;
    }

    /**
     * @return this.name.
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * @return this.isParExist
     */
    public boolean isParExist() {
        return this.isParExist;
    }
}
