package com.nswbc.commons.core.configuration.login;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = LoginConfigurationInstance.class, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = ClientIDLoginServiceConfiguration.class, factory = true)
public class LoginConfigurationInstance {
    /** private configuration variable. */
    private ClientIDLoginServiceConfiguration config;

    /**
     * @param config
     */
    @Activate
    public void activate(ClientIDLoginServiceConfiguration config) {
        this.config = config;
    }

    /**
     * @return the config
     */
    public ClientIDLoginServiceConfiguration getConfig() {
        return config;
    }

}
