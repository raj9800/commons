package com.nswbc.commons.core.models.category;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.nswbc.commons.core.models.carousel.CarouselPanel;

/**
 * @author yuvraj.bansal
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/category/categoryItems",
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class CategoryItemsModel {
    /**
     *  private self variable of carouselPanel class.
     */
    @Self
    private CarouselPanel panelItem;

    /**
     * @return panelItems
     */
    public CarouselPanel getPanelItem() {
        return panelItem;
    }
}
