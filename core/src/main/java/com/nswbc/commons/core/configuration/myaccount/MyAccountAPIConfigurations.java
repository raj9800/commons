package com.nswbc.commons.core.configuration.myaccount;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author yuvraj.bansal Interface for configuration
 *
 */
@ObjectClassDefinition(name = "My Account API Configurations", description = "Service Configuration")
public @interface MyAccountAPIConfigurations {
    /**
     * @return tokenValidationAPI
     */
    @AttributeDefinition(name = "Token Validation API", description = "Provide the api url for token validation.")
    String tokenValidationAPI();
    /**
     * @return memberDetailAPI
     */
    @AttributeDefinition(name = "Get Member Details API", description = "Provide the api url for member details.")
    String memberDetailsAPI();
    /**
     * @return updateMemberDetails
     */
    @AttributeDefinition(name = "Update Member Details API", description = "Provide the api url to update member details.")
    String updateMemberDetailsAPI();
    /**
     * @return changePassword
     */
    @AttributeDefinition(name = "Change Password API", description = "Provide the change password api url.")
    String changePasswordAPI();
    /**
     * @return memberdeactivation
     */
    @AttributeDefinition(name = "Member Deactivation API", description = "Provide the api url to deactivate the member.")
    String memberdeactivationAPI();
    /**
     * @return subscriptionkey
     */
    @AttributeDefinition(name = "Subscription Key", description = "Provide the subscription key.")
    String subscriptionKey();


}
