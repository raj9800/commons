package com.nswbc.commons.core.models.carousel;

import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.adobe.cq.export.json.ComponentExporter;
import com.nswbc.commons.core.models.image.ImageModel;

/**
 * Carousel Panel Model class.
 * @author rajat.pachouri
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
adapters = {ComponentExporter.class }, resourceType = "nswbc-commons/components/content/carousel-panel")
public class HeroCarouselPanel implements ComponentExporter {

    /** private current resource object. */
    @Inject
    private Resource resource;

    /** private variable to show desktop image in carousel panel. */
    private ImageModel desktopimage;

    /** private variable to show mobile image in carousel panel. */
    private ImageModel mobileimage;
    /**
     * Getter method for desktopimage object.
     * @return desktopimage object
     */
    public ImageModel getDesktopimage() {
        return desktopimage;
    }
    /**
     * Getter method for mobileimage object.
     * @return mobileimage object
     */
    public ImageModel getMobileimage() {
        return mobileimage;
    }
    /**
     * Getter method for exported type.
     * @return title
     */
    @Override
    public String getExportedType() {
        return resource.getResourceType();
    }

    /**
     * Post construct method to inject images.
     */
    @PostConstruct
    public void init() {
        if (!Objects.isNull(resource)) {
            Resource desktopimage = resource.getChild("desktopimage");
            if (!Objects.isNull(desktopimage)) {
                this.desktopimage = desktopimage.adaptTo(ImageModel.class);
            }
            Resource mobileimage = resource.getChild("mobileimage");
            if (!Objects.isNull(mobileimage)) {
                this.mobileimage = mobileimage.adaptTo(ImageModel.class);
            }
        }
    }
}
