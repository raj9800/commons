package com.nswbc.commons.core.models.metatags;

import static com.nswbc.commons.core.constants.Constants.Application.EMPTY;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;

/**
 * @author yuvraj.bansal
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class MetaTagsModel {
    /**
     * private inject variable for nofollow.
     */
    @Inject
    @Via("resource")
    @Default(values = "false")
    private String isNoFollow;

    /**
     * current page object.
     */
    @ScriptVariable
    private Page currentPage;

    /**
     * private inject variable for subHeadingStrong.
     */
    @Inject
    @Via("resource")
    @Default(values = "false")
    private String isNoIndex;

    /** private variable for metatag. */
    @Inject
    @Via("resource")
    private Resource metatags;

    /** private variable for metatags items. */
    private List<MetaTagsItemModel> metaTagList;
    /**
     * dot constant.
     */
    protected static final String DOT = ".";
    /**
     * The File reference.
     */
    @Inject
    @Via("resource")
    @Named("root/responsivegrid/layoutcontainer/image/fileReference")
    private String fileReference;
    /**
     * private sling object for resource resolver.
     */
    @Inject
    private ResourceResolver resourceResolver;

    /**
     * @return metataglist
     */
    public List<MetaTagsItemModel> getMetaTagList() {
        metaTagList = new ArrayList<MetaTagsItemModel>();
        Iterator<Resource> children = null;
        if (Objects.nonNull(metatags)) {
            children = metatags.listChildren();
        }
        while (children.hasNext()) {
            Resource itemResource = children.next();
            MetaTagsItemModel itemObj = itemResource.adaptTo(MetaTagsItemModel.class);
            if (Objects.nonNull(itemObj)) {
                metaTagList.add(itemObj);
            }
        }
        return metaTagList;
    }

    /**
     * @return the isNoFollow
     */
    public String getIsNoFollow() {
        return isNoFollow;
    }

    /**
     * @return the isNoIndex
     */
    public String getIsNoIndex() {
        return isNoIndex;
    }

    /**
     * returns the image URL that is added on an article page to add into the meta
     * tag. resPath = ResourceUtils.getResolvedPath(resourceResolver, resPath);
     *
     * @return uri of image with a specific rendition.
     */
    public String getMetaDataForImage() {
        String finalImageURL = StringUtils.EMPTY;

        String imageCompPath = currentPage.getPath() + "/jcr:content/root/responsivegrid/layoutcontainer/image";
        String imageCompPathMapped = currentPage.getPath() + "/_jcr_content/root/responsivegrid/layoutcontainer/image";

        Resource imageCompRes = resourceResolver.getResource(imageCompPath);
        Resource imageRes = resourceResolver.getResource(fileReference);

        ValueMap imageResProperties = null != imageCompRes ? imageCompRes.getValueMap() : null;

        long lastModifiedDate = 0;
        String imageName = StringUtils.isNotEmpty(fileReference)
                ? getSeoFriendlyName(FilenameUtils.getBaseName(fileReference))
                : "";

        if (imageRes != null) {
            Asset asset = imageRes.adaptTo(Asset.class);
            if (asset != null) {

                // get extention.
                String mimeType = asset.getMimeType();
                String extension = EMPTY;
                String[] split = mimeType.split("/");
                if (split.length > 1) {
                    extension = split[1];
                }

                // getLastModified
                long assetLastModifiedDate = asset.getLastModified();

                Calendar lastModified = (null != imageResProperties)
                        ? imageResProperties.get(JcrConstants.JCR_LASTMODIFIED, Calendar.class)
                        : null;
                if (lastModified == null) {
                    lastModified = (null != imageResProperties)
                            ? imageResProperties.get(NameConstants.PN_PAGE_LAST_MOD, Calendar.class)
                            : null;
                }
                if (lastModified != null) {
                    lastModifiedDate = lastModified.getTimeInMillis();
                }

                if (assetLastModifiedDate > lastModifiedDate) {
                    lastModifiedDate = assetLastModifiedDate;
                }
                finalImageURL = imageCompPathMapped + DOT + "coreimg" + DOT + extension + "/"
                        + ((lastModifiedDate > 0) ? lastModifiedDate + "/" : "") + imageName + DOT + extension;
            }
        }
        return finalImageURL;
    }

    /**
     * Content editors can store DAM assets with white spaces in the name, this
     * method makes the asset name SEO friendly, Translates the string into
     * {@code application/x-www-form-urlencoded} format using {@code utf-8} encoding
     * scheme.
     *
     * @param imageName
     * @return the SEO friendly image name
     */
    protected String getSeoFriendlyName(String imageName) {

        // Google recommends using hyphens (-) instead of underscores (_) for SEO. See
        // https://support.google.com/webmasters/answer/76329?hl=en
        String seoFriendlyName = imageName.replaceAll("[\\ _]", "-").toLowerCase();
        try {
            seoFriendlyName = URLEncoder.encode(seoFriendlyName, "UTF-8");
        } catch (UnsupportedEncodingException e) {
//            LOGGER.error(String.format("The Character Encoding is not supported."));
        }
        return seoFriendlyName;
    }

}
