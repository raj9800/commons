/*
 * #%L
 * ACS AEM Commons Bundle
 * %%
 * Copyright (C) 2013 - 2014 Adobe
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.nswbc.commons.core.modelinjectors;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.models.annotations.Source;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.spi.injectorspecific.InjectAnnotation;

/**
 * Injects various parts of a resource based on the type of the property.<br>
 * Traverses upwards in the page hierarchy until the required type is found.<br>
 * Note: not supported by the javax.Inject annotation because of performance
 * reasons. Only direct annotation is supported. <br>
 * <br>
 * <b>Following things can be injected </b><br>
 * 1. <b>String</b> single valued string property <br>
 * 2. <b>String[]</b> Multi valued String property array<br>
 * 3. <b>Resource</b> Child resource<br>
 * 4. <b>List of Resource</b> list of nodes under a child resource as a list<br>
 */
@Target({ METHOD, FIELD, PARAMETER })
@Retention(RUNTIME)
@InjectAnnotation
@Source(HierarchyNodeInheritanceResource.SOURCE)
public @interface HierarchyNodeInheritanceResource {

    /**
     * Source value used for this annotation.
     *
     * @see Source
     */
    String SOURCE = "hierarchical-resource";

    /**
     * Specifies the name of the value from the value map to take. If empty, then
     * the name is derived from the method or field.
     *
     * @return defaultValue
     */
    String value() default StringUtils.EMPTY;

    /**
     * if set to REQUIRED injection is mandatory, if set to OPTIONAL injection is
     * optional, in case of DEFAULT the standard annotations
     * ({@link org.apache.sling.models.annotations.Optional},
     * {@link org.apache.sling.models.annotations.Required}) are used. If even those
     * are not available the default injection strategy defined on the
     * {@link org.apache.sling.models.annotations.Model} applies. Default value =
     * DEFAULT.
     *
     * @return injection strategy
     */
    InjectionStrategy injectionStrategy() default InjectionStrategy.DEFAULT;

}
