package com.nswbc.commons.core.models.header;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.adobe.cq.export.json.ComponentExporter;
import com.day.cq.wcm.api.Page;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.modelinjectors.HierarchyNodeInheritanceResource;
import com.nswbc.commons.core.models.image.ImageModel;
import com.nswbc.commons.core.models.link.LinkModel;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * Footer Model.
 *
 * @author rajat.pachouri
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
adapters = { ComponentExporter.class })
public class HeaderModel implements ComponentExporter {
    /** Resource object. */
    @Inject
    private Resource resource;
    /** private sling object for resource resolver. */
    @Inject
    private ResourceResolver resourceResolver;

    /** private variable to show logo in header. */
    @HierarchyNodeInheritanceResource("logo")
    private Resource logo;
    /** private variable to show myaccount link in header. */
    @HierarchyNodeInheritanceResource("myaccount")
    private Resource myaccountLink;
    /** private variable to show utilitylogo in header. */
    @HierarchyNodeInheritanceResource("utilitylogo")
    private Resource utilitylogo;
    /** variable to store home page path. */
    @HierarchyNodeInheritanceResource("homepagepath")
    private String homepagepath;
    /** variable to store home page path. */
    @HierarchyNodeInheritanceResource("extraLink")
    private String extraLink;
    /** private variable to store utilitylinks. */
    @HierarchyNodeInheritanceResource("utilityLinks")
    private List<Resource> utilityLinks;
    /** private variable to store search result page path. */
    @HierarchyNodeInheritanceResource("searchResultPath")
    private String searchResultPath;
    /** private variable to store second image used for hero mode. */
    @HierarchyNodeInheritanceResource("fileReference")
    private String fileReference;
    /** private variable to include registration in header. */
    @HierarchyNodeInheritanceResource("registrationLabel")
    private String registrationLabel;
    /** private variable to include registration link in header. */
    @HierarchyNodeInheritanceResource("registrationLink")
    private String registrationLink;
    /** private variable for samePageRegisterUri. */
    @HierarchyNodeInheritanceResource("samePageRegisterUri")
    private String samePageRegisterUri;
    /** private variable for differentPageRegisterUri. */
    @HierarchyNodeInheritanceResource("differentPageRegisterUri")
    private String differentPageRegisterUri;
    /** private variable to include login label in header. */
    @HierarchyNodeInheritanceResource("loginLabel")
    private String loginLabel;
    /** private variable to include login link in header. */
    @HierarchyNodeInheritanceResource("loginLink")
    private String loginLink;
    /** private variable for samePageLoginUri. */
    @HierarchyNodeInheritanceResource("samePageLoginUri")
    private String samePageLoginUri;
    /** private variable for differentPageLoginUri. */
    @HierarchyNodeInheritanceResource("differentPageLoginUri")
    private String differentPageLoginUri;
    /** private variable for showRegisterConfig. */
    @HierarchyNodeInheritanceResource("showRegisterConfig")
    private String showRegisterConfig;
    /** private variable for showLoginConfig. */
    @HierarchyNodeInheritanceResource("showLoginConfig")
    private String showLoginConfig;
    /** Script Variable for servlet request. */
    @Self
    private SlingHttpServletRequest request;
    /**
     * Current page object.
     */
    @Inject
    private Page currentPage;

    /**
     * Getter for logo.
     *
     * @return logo.
     */
    public ImageModel getLogo() {
        ImageModel logo = null;
        if (Objects.nonNull(this.logo)) {
            logo = this.logo.adaptTo(ImageModel.class);
        }
        return logo;
    }

    /**
     * Getter for myaccount link.
     *
     * @return myaccountLink.
     */
    public LinkModel getMyAccountLink() {
        LinkModel myaccountLink = null;
        if (Objects.nonNull(this.myaccountLink)) {
            myaccountLink = this.myaccountLink.adaptTo(LinkModel.class);
        }
        return myaccountLink;
    }

    /**
     * Getter for logo.
     *
     * @return logo.
     */
    public ImageModel getUtilitylogo() {
        ImageModel logo = null;
        if (Objects.nonNull(this.utilitylogo)) {
            logo = this.utilitylogo.adaptTo(ImageModel.class);
        }
        return logo;
    }

    /**
     * method to know if the current page is homepage.
     *
     * @return true is current page is true page
     */
    public boolean isHomePage() {
        boolean isHomePage = false;
        if (Objects.nonNull(currentPage) && StringUtils.isNotBlank(homepagepath)) {
            if (currentPage.getPath().equals(homepagepath)) {
                isHomePage = true;
            }
        }
        return isHomePage;
    }

    /**
     * This method adapts the resource to LinkModel and returns a list of
     * linkmodels.
     *
     * @param list List of resources to adapt to link model
     * @return adaptedList
     */
    public List<LinkModel> getAdaptedList(List<Resource> list) {
        ArrayList<LinkModel> adaptedList = null;
        if (Objects.nonNull(list) && list.size() > 0) {
            adaptedList = new ArrayList<LinkModel>();
            for (Resource res : list) {
                LinkModel link = res.adaptTo(LinkModel.class);
                adaptedList.add(link);
            }
        }
        return adaptedList;
    }

    /**
     * returns utilityLinks.
     *
     * @return List of LinkModel
     */
    public List<LinkModel> getUtilityLinks() {
        return getAdaptedList(utilityLinks);
    }

    /**
     * Getter for extraLink.
     *
     * @return string
     */
    public String getExtraLink() {
        return extraLink;
    }

    /**
     * Getter for fileReference.
     *
     * @return string
     */
    public String getFileReference() {
        return fileReference;
    }

    /**
     * Getter for search result path.
     *
     * @return string
     */
    public String getSearchResultPath() {
        Page searchPage = resourceResolver.getResource(searchResultPath).adaptTo(Page.class);
        return ResourceUtils.getURL(resourceResolver, searchPage.getPath() + Constants.Extensions.HTML_EXTENSION);
    }

    /**
     * @return List of LinkModel
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * @return List of LinkModel
     */
    public ResourceResolver getResourceResolver() {
        return resourceResolver;
    }

    /**
     * @return the registrationLabel
     */
    public String getRegistrationLabel() {
        return registrationLabel;
    }

    /**
     * @return the registrationLink
     */
    public String getRegistrationLink() {
        String differentPagePostRegisterUri = Constants.Application.EMPTY;
        if (StringUtils.isNotEmpty(differentPageRegisterUri) && StringUtils.equals(samePageRegisterUri, "false")) {
            Page differentPageUri = resourceResolver.getResource(differentPageRegisterUri).adaptTo(Page.class);
            differentPagePostRegisterUri = ResourceUtils.getResolvedPath(resourceResolver, differentPageUri.getPath());
        } else {
            differentPagePostRegisterUri = ResourceUtils.getResolvedPath(resourceResolver, currentPage.getPath());
        }
        Page registrationPage = resourceResolver.getResource(registrationLink).adaptTo(Page.class);
        return ResourceUtils.getResolvedPath(resourceResolver, registrationPage.getPath()) + Constants.Application.QUESTION_MARK
                + "post_login_redirect=" + differentPagePostRegisterUri;
    }

    /**
     * @return the loginLabel
     */
    public String getLoginLabel() {
        return loginLabel;
    }

    /**
     * @return the loginLink
     */
    public String getLoginLink() {
        String differentPagePostLoginUri = Constants.Application.EMPTY;
        if (StringUtils.isNotEmpty(differentPageLoginUri) && StringUtils.equals(samePageLoginUri, "false")) {
            Page differentPageUri = resourceResolver.getResource(differentPageLoginUri).adaptTo(Page.class);
            differentPagePostLoginUri = ResourceUtils.getResolvedPath(resourceResolver, differentPageUri.getPath());
        }
        Page loginPage = resourceResolver.getResource(loginLink).adaptTo(Page.class);
        return ResourceUtils.getResolvedPath(resourceResolver, loginPage.getPath()) + ((StringUtils.isNotEmpty(differentPageLoginUri) && StringUtils
                .equals(samePageLoginUri, "false")) ? Constants.Application.QUESTION_MARK
                        + "post_login_redirect=" + differentPagePostLoginUri : Constants.Application.EMPTY);

    }

    /**
     * Getter method for exported type.
     *
     * @return title
     */
    @Override
    public String getExportedType() {
        return resource.getResourceType();
    }

    /**
     * @return the showRegisterConfig
     */
    public String getShowRegisterConfig() {
        return showRegisterConfig;
    }

    /**
     * @return the showLoginConfig
     */
    public String getShowLoginConfig() {
        return showLoginConfig;
    }

}
