package com.nswbc.commons.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;

import com.day.cq.wcm.api.Page;
import com.google.gson.JsonObject;
import com.nswbc.commons.core.configuration.products.ProductGuidCheckConfiguration;

/**
 * ProductCheckGuidService Class.
 *
 * @author riccardo.teruzzi
 */
public interface ProductCheckGuidService {

    /**
     * Gets config.
     *
     * @return the config
     */
    ProductGuidCheckConfiguration getConfig();

    /**
     * @param resourceResolver
     * @return secure page
     */
    Page getSecurePage(ResourceResolver resourceResolver);

    /**
     * Check guid code int.
     *
     * @param request  the request
     * @param response the resource
     * @return the json response
     */
    JsonObject checkGuidCode(SlingHttpServletRequest request, SlingHttpServletResponse response);

    /**
     * Gets awards list.
     *
     * @param securePage
     * @return the awards json list
     */
    JsonObject getAwardsList(Page securePage);
}
