package com.nswbc.commons.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * UpdateModernAwardsListService Class.
 *
 * @author riccardo.teruzzi
 */
public interface WebinarVerificationService {
    /**
     * This method returns the update modern awards list.
     *
     * @param request  the request
     * @param response the response
     * @param bodyJson jsonobject from the body
     * @return the update modern awards list
     */
    JsonObject checkVerification(SlingHttpServletRequest request, SlingHttpServletResponse response,
            JsonObject bodyJson);

    /**
     * This method returns the update modern awards list.
     *
     * @param request  the request
     * @param response the response
     * @param bodyJson jsonobject from the body
     * @return the update modern awards list
     */
    JsonObject getEventCodeAndcheckVerification(SlingHttpServletRequest request, SlingHttpServletResponse response,
            JsonObject bodyJson);
}
