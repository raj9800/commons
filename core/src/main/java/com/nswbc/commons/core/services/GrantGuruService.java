package com.nswbc.commons.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * Grant guru service interface.
 *
 * @author rajat.pachouri
 *
 */
public interface GrantGuruService {
    /**
     * This method grant guru uri with necessary tokens for single sign in.
     *
     * @param request  the request
     * @param response the response
     * @return uri with tokens as parameters
     */
    JsonObject getGrantGuruURI(SlingHttpServletRequest request, SlingHttpServletResponse response);

}
