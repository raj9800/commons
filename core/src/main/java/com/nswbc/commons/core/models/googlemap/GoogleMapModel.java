package com.nswbc.commons.core.models.googlemap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nswbc.commons.core.configuration.googlemap.GoogleMapConfigurationsInstance;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * Google map model.
 *
 * @author rajat.pachouri
 *
 */
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class GoogleMapModel {
    /**
     * The constant PLACES.
     */
    public static final String PLACES = "places";

    /**
     * The self resource.
     */
    @SlingObject
    private Resource resource;

    /**
     * Resource resolver object.
     */
    @Inject
    private ResourceResolver resourceResolver;

    /**
     * Google map config.
     */
    @Inject
    private GoogleMapConfigurationsInstance googleMapConfig;

    /** Script Variable for current page. */
    @ScriptVariable
    @Optional
    private Page currentPage;

    /**
     * variable to store locations.
     */
    private String locations;

    /**
     * Gets current page path.
     *
     * @return currentPagePath current page
     */
    public String getCurrentPagePath() {
        return ResourceUtils.getResolvedPath(resourceResolver, currentPage.getPath());
    }

    /**
     * Getter for locations.
     *
     * @return adaptedList adapted list
     */
    public String getLocations() {
        return locations;
    }

    /**
     * To get api key.
     *
     * @return apiKey
     */
    public String getApiKey() {
        return googleMapConfig.getConfig().googleMapApiKey();
    }

    /**
     * populate locations.
     */
    @PostConstruct
    public void populateLocations() {
        Gson gson = new Gson();
        JsonArray places = new JsonArray();
        ArrayList<PlacesModel> adaptedList = new ArrayList<>();
        Resource linksList = resource.getChild(PLACES);
        if (Objects.nonNull(linksList)) {
            Iterator<Resource> resourceIterator = linksList.listChildren();
            while (resourceIterator.hasNext()) {
                Resource res = resourceIterator.next();
                PlacesModel place = res.adaptTo(PlacesModel.class);
                adaptedList.add(place);
            }
            for (PlacesModel place : adaptedList) {
                JsonObject obj = new JsonObject();
                obj.addProperty("name", place.getTitle());
                obj.addProperty("lat", place.getLatitude());
                obj.addProperty("long", place.getLongitude());
                obj.addProperty("address", place.getAddress());
                obj.addProperty("email", place.getEmail());
                obj.addProperty("phone", place.getPhone());
                places.add(obj);
            }
        }
        locations = places.size() > 0 ? gson.toJson(places) : StringUtils.EMPTY;
    }
}
