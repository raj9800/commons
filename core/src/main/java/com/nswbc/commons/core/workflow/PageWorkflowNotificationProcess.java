package com.nswbc.commons.core.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.email.EmailService;
import com.adobe.granite.security.user.UserProperties;
import com.adobe.granite.security.user.UserPropertiesManager;
import com.adobe.granite.security.user.util.AuthorizableUtil;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.HistoryItem;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.commons.Externalizer;
import com.day.cq.replication.ReplicationOptions;
import com.day.cq.replication.Replicator;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.workflow.utils.WorkflowUtils;

/**
 * This process contains all the business logic to send the email notifications
 * to users during the page activation and deactivation worklflow.
 *
 * @author rajat.pachouri
 *
 */
@Component(service = WorkflowProcess.class, immediate = true, enabled = true,
        property = {"process.label= Email notification process for "
                + "Business Australia Page Activation and Deactivation workflow"})

public class PageWorkflowNotificationProcess implements WorkflowProcess {

    /**
     * Template to use when assigning the workflow to a participant from higher
     * group.
     */
    private static final String TEMPLATE_FORWARD = "/apps/workflow/email-templates/forward.txt";
    /**
     * Template to use when the workflow ends successfully.
     */
    private static final String TEMPLATE_SUCCESS = "/apps/workflow/email-templates/success.txt";
    /**
     * Template to use when the workflow is terminated.
     */
    private static final String TEMPLATE_TERMINATED = "/apps/workflow/email-templates/terminated.txt";
    /**
     * Template to use when the workflow is reverted back to a participant from
     * lower group.
     */
    private static final String TEMPLATE_REVERETED = "/apps/workflow/email-templates/reverted.txt";

    /**
     * Replicator service to publish pages.
     */
    @Reference
    private Replicator replicator;

    /**
     * ACS commons email service to send emails.
     */
    @Reference
    private EmailService emailService;

    /**
     * Externalizer service.
     */
    @Reference
    private Externalizer externalizer;
    /**
     * LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PageWorkflowNotificationProcess.class);

    /**
     * The method which is called when the process runs.
     */
    @Override
    public final void execute(final WorkItem workitem, final WorkflowSession wfsession, final MetaDataMap metaDataMap) {
        List<String> args = null;
        String payload = workitem.getWorkflowData().getPayload().toString();
        ResourceResolver resolver = wfsession.adaptTo(ResourceResolver.class);
        UserPropertiesManager userPropertiesManager = resolver.adaptTo(UserPropertiesManager.class);
        UserManager userManager = resolver.adaptTo(UserManager.class);
        String effectiveDeactivationTime = StringUtils.EMPTY;
        String activationTimeAssignee = StringUtils.EMPTY;
        String effectiveActivationTime = StringUtils.EMPTY;
        String deactivationTimeAssignee = StringUtils.EMPTY;
        List<HistoryItem> list = null;
        Map<String, String> emailParams = new HashMap<String, String>();
        String workflowType = StringUtils.EMPTY;
        String assignTo = StringUtils.EMPTY;
        String from = StringUtils.EMPTY;
        String workflowInitiator = StringUtils.EMPTY;
        String timeheading = StringUtils.EMPTY;
        // get args
        args = WorkflowUtils.getArgsArray(metaDataMap);
        String[] emailRecipients = null;

        boolean isActivationWorkflow = false;
        ReplicationOptions opts = new ReplicationOptions();
        opts.setSynchronous(true);
        String emailTemplate = StringUtils.EMPTY;
        // determine if the workflow is for the activation or deactivation
        if (null != args && !args.isEmpty()) {
            if (args.contains("activation")) {
                isActivationWorkflow = true;
                workflowType = "Activation";
                LOGGER.error("Activation workflow");
            }
            if (args.contains("deactivation")) {
                workflowType = "Deactivation";
                LOGGER.error("Deativation workflow");
            }
        }
        // get workflow history list
        list = WorkflowUtils.getWorkflowHistoryList(wfsession, workitem);
        String value = StringUtils.EMPTY;
        try {
            // get effective activation time and assignee if the workflow is for the
            // activation
            if (isActivationWorkflow) {
                value = WorkflowUtils.geteffectiveUserAndDateFromHistory(list, "overrideactivation", "actdate");
                if (StringUtils.isNotBlank(value)) {
                    effectiveActivationTime = WorkflowUtils.splitDetails(value)[0];
                    activationTimeAssignee = WorkflowUtils.splitDetails(value)[1];
                    UserProperties profile;
                    profile = AuthorizableUtil.getProfile(userPropertiesManager, activationTimeAssignee);
                    activationTimeAssignee = profile.getDisplayName();
                }
            }
            // get effective deactivation time and assignee for both activation and
            // deactivation workflow
            value = WorkflowUtils.geteffectiveUserAndDateFromHistory(list, "overridedeactivation", "deactdate");
            if (StringUtils.isNotBlank(value)) {
                effectiveDeactivationTime = WorkflowUtils.splitDetails(value)[0];
                deactivationTimeAssignee = WorkflowUtils.splitDetails(value)[1];
                UserProperties profile;
                profile = AuthorizableUtil.getProfile(userPropertiesManager, deactivationTimeAssignee);
                deactivationTimeAssignee = profile.getDisplayName();
            }

            // Get the assignee of last participant step. This applies to every group hence
            // no if condition.
            from = WorkflowUtils.getLastAssigneeFromHistory(list);
            if (StringUtils.isNotBlank(from)) {
                UserProperties profile;
                profile = AuthorizableUtil.getProfile(userPropertiesManager, from);
                from = profile.getDisplayName();
            }
            // Get the initiator of the workflow if it is reverted to initiator or finishes
            // or rejected
            if (args.contains("revert-initiator")) {
                timeheading = "Effective time given for the page.";
                workflowInitiator = workitem.getWorkflow().getInitiator();
                emailRecipients = fillEmailRecipients(userManager, workflowInitiator, emailRecipients);
            }
            if (args.contains("reject") || args.contains("success")) {
                timeheading = "Page will be actioned upon based on this time.";
                HashSet<String> usersSet = WorkflowUtils.getAllAssigneeFromHistory(list);
                List<String> userEmails = new ArrayList<String>();
                if (null != usersSet && !usersSet.isEmpty()) {
                    for (String userId : usersSet) {
                        if (StringUtils.isNotBlank(userId)) {
                            User user = (User) userManager.getAuthorizable(userId);
                            String userEmail = getMailIdFrmUsr(user);
                            if (StringUtils.isNotBlank(userEmail)) {
                                userEmails.add(userEmail);
                            }
                        }
                    }
                }
                if (null != userEmails && !userEmails.isEmpty()) {
                    emailRecipients = userEmails.toArray(new String[0]);
                }
            }
            // Get the user the workflow will be assigned to if it forward
            if (args.contains("forward")) {
                timeheading = "Effective time given for the page.";
                assignTo = WorkflowUtils.getAssignToUserFromLastItems(list);
                emailRecipients = fillEmailRecipients(userManager, assignTo, emailRecipients);
            }
            // Get the user the workflow will be assigned to if it forwarded to group 2
            if (args.contains("revert-group2")) {
                timeheading = "Effective time given for the page.";
                assignTo = WorkflowUtils.getPropertyFromHistory(list, "user2");
                emailRecipients = fillEmailRecipients(userManager, assignTo, emailRecipients);
            }
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage(), e);
        }
        emailTemplate = getTemplate(args);
        emailParams.put("from", from);
        String time = StringUtils.EMPTY;
        // populating time from backend as the email template renders keys if the values
        // are not available to it.
        if (StringUtils.isNotBlank(effectiveActivationTime)) {
            time = "Activation time: " + effectiveActivationTime + "\nprovided by: " + activationTimeAssignee + "\n";
        }
        if (StringUtils.isNotBlank(effectiveDeactivationTime)) {
            if (isActivationWorkflow) {
                time = time + "\n";
            }
            time = time + "Deactivation time: " + effectiveDeactivationTime + "\nprovided by: "
                    + deactivationTimeAssignee;
        }
        if (StringUtils.isBlank(time)) {
            time = "No time has been given for this page hence it will be actioned immediately.";
        } else {
            time = timeheading + "\n" + time;
        }
        emailParams.put("time", time);
        payload = externalizer.externalLink(resolver, "author",
                "/editor.html" + payload + Constants.Extensions.HTML_EXTENSION);
        emailParams.put("payload", payload);
        emailParams.put("workflowType", workflowType);
        if (Objects.nonNull(emailRecipients)) {
            emailService.sendEmail(emailTemplate, emailParams, emailRecipients);
        }
    }

    /**
     *
     * This method returns a list with the email of the user whose userId has been
     * passed to it.
     *
     * @param userManager     usermanager to get user from userId
     * @param userId          userID
     * @param emailRecipients list to collect all the email recipients.
     * @return emailRecipients
     * @throws RepositoryException  RepositoryException
     * @throws ValueFormatException ValueFormatException
     */
    private String[] fillEmailRecipients(UserManager userManager, String userId, String[] emailRecipients)
            throws RepositoryException, ValueFormatException {
        String userEmail;
        if (StringUtils.isNotBlank(userId)) {
            User user = (User) userManager.getAuthorizable(userId);
            userEmail = getMailIdFrmUsr(user);
            if (StringUtils.isNotBlank(userEmail)) {
                emailRecipients = new String[] {userEmail};
            }
        }
        if (Objects.isNull(emailRecipients)) {
            LOGGER.debug("No emailID present for" + userId);
        }
        return emailRecipients;
    }

    /**
     * This method return the email Id of a user.
     *
     * @param user
     * @throws ValueFormatException  the value format exception
     * @throws IllegalStateException the illegalstate exception
     * @throws RepositoryException   the respository exception
     * @return <tt>emailId</tt> if found else <tt>null</tt> object
     */
    private String getMailIdFrmUsr(final User user)
            throws ValueFormatException, IllegalStateException, RepositoryException {
        return null != user.getProperty("./profile/email") ? user.getProperty("./profile/email")[0].getString() : null;
    }

    /**
     * This method sets the template based on the arguments provided in the workflow
     * steps.
     *
     * @param args The args object passed via workflow steps
     * @return template
     */
    private String getTemplate(final List<String> args) {
        String template = StringUtils.EMPTY;
        if (args.contains("success")) {
            template = TEMPLATE_SUCCESS;

        }
        if (args.contains("forward")) {
            template = TEMPLATE_FORWARD;
        }
        if (args.contains("revert-group2")) {
            template = TEMPLATE_REVERETED;
        }
        if (args.contains("revert-initiator")) {
            template = TEMPLATE_REVERETED;
        }
        if (args.contains("reject")) {
            template = TEMPLATE_TERMINATED;
        }
        return template;
    }
}
