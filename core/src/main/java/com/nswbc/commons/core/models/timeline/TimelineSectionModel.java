package com.nswbc.commons.core.models.timeline;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

/**
 * @author yuvraj.bansal. Model for Timeline Section.
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/timelineSection",
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class TimelineSectionModel {

    /** private variable for timeLineConfig. */
    @Inject
    @Via("resource")
    private Resource timeLineConfig;
    /** private variable for timelineSectionList. */
    private List<TimeLineSectionBean> timelineSectionList;

    /**
     * @return timelineSectionList.
     */
    public List<TimeLineSectionBean> getSectionList() {
        timelineSectionList = new ArrayList<TimeLineSectionBean>();
        Iterator<Resource> children = null;
        if (Objects.nonNull(timeLineConfig)) {
            children = timeLineConfig.listChildren();
        }
        while (children.hasNext()) {
            Resource itemResource = children.next();
            TimeLineSectionBean beanObj = itemResource.adaptTo(TimeLineSectionBean.class);
            if (Objects.nonNull(beanObj)) {
                timelineSectionList.add(beanObj);
            }
        }
        return timelineSectionList;
    }
}
