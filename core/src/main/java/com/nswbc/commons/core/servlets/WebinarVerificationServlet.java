package com.nswbc.commons.core.servlets;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Servlet;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.WebinarVerificationService;
import com.nswbc.commons.core.utils.RestUtil;
import com.nswbc.commons.core.utils.ServerSideValidationUtil;

/**
 * @author rajat.pachouri
 *
 */
@Component(service = Servlet.class, property = {"sling.servlet.paths=" + WebinarVerificationServlet.SERVLET_PATH,
        "sling.servlet.extensions=" + Constants.Text.JSON, "sling.servlet.methods=POST"})
public class WebinarVerificationServlet extends SlingAllMethodsServlet {

    /** generated serial version uid. */
    private static final long serialVersionUID = 8559142970097907293L;
    /** protected variable for servlet path. */
    protected static final String SERVLET_PATH = "/bin/nswbc/webinarverify";
    /** Constant for status. */
    private static final String STATUS = "status";
    /** Constant for Subscription key. */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    /** private constant for logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(WebinarVerificationServlet.class);
    /** private reference variable for my account configuration. */
    @Reference
    private WebinarVerificationService webinarVerificationService;

    /**
     * doPost method to send email and receive token.
     */
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        JsonObject result = new JsonObject();
        JsonObject bodyJson = RestUtil.getBodyJson(request);
        if (!bodyJson.isJsonNull() && (bodyJson.has("Email")
                // if request comes from webinar verification component.
                && ServerSideValidationUtil.checkEmail(bodyJson.get("Email").getAsString()))) {
            if (bodyJson.has("EventCode") && StringUtils.isNotBlank(bodyJson.get("EventCode").getAsString())) {
                result = webinarVerificationService.checkVerification(request, response, bodyJson);
            } else if (bodyJson.has("redirecturi")
                    // if request is coming from a webinar page.
                    && StringUtils.isNotBlank((bodyJson.get("redirecturi").getAsString()))) {
                result = webinarVerificationService.getEventCodeAndcheckVerification(request, response, bodyJson);
            }
        } else {
            result = RestUtil.getBadRequestResponse(response);
        }
        printResponseResult(response, result);
    }

    /**
     * @param response
     * @param result
     * @throws IOException This method checks the output result
     */
    private void printResponseResult(SlingHttpServletResponse response, JsonObject result) throws IOException {
        if (Objects.nonNull(result)) {
            response.getWriter().print(result);
        }
    }
}
