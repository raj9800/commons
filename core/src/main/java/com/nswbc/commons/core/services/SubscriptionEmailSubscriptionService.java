package com.nswbc.commons.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author yuvraj.bansal
 *
 */
public interface SubscriptionEmailSubscriptionService {

    /**
     * @param request
     * @param response
     * @return json as status
     */
    JsonObject getToken(SlingHttpServletRequest request, SlingHttpServletResponse response);
}
