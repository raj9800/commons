package com.nswbc.commons.core.services;

import com.google.gson.JsonObject;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

/**
 * ModernAwardsListService Class.
 *
 * @author riccardo.teruzzi
 */
public interface ModernAwardsListService {
    /**
     * This method returns the modern awards list.
     *
     * @param request  the request
     * @param response the response
     * @return the modern awards list
     */
    JsonObject getModernAwardsList(SlingHttpServletRequest request, SlingHttpServletResponse response);

}
