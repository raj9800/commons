package com.nswbc.commons.core.services.impl;

import java.util.Iterator;

import javax.jcr.RepositoryException;
import javax.jcr.query.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.json.JSONArray;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nswbc.commons.core.services.UsersService;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * This class fetches the list of all the users from a given group.
 *
 * @author rajat.pachouri
 *
 */
@Component(immediate = true, service = UsersService.class)
public class GetUsersImpl implements UsersService {

    /**
     * THe resource resolver factory.
     */
    @Reference
    private ResourceResolverFactory resourceFactory;
    /**
     * The resource resolver.
     */
    private ResourceResolver resolver = null;
    /**
     * The query to get the list of the resource path of all the users.
     */
    private static final String QUERY = "SELECT * FROM [rep:User] ORDER BY [rep:principalName]";
    /**
     * LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(GetUsersImpl.class);

    /**
     * Method that takes a group name and returns the list of all the declared users
     * from that group.
     *
     * @return json object which contains the users and their full names
     */
    @Override
    public String getUsers(String groupName) {
        JSONObject rootObj = new JSONObject();
        JSONArray usersList = new JSONArray();
        try {
            if (StringUtils.isNotBlank(groupName)) {
                resolver = ResourceUtils.getResourceResolver(resourceFactory);
                final Iterator<Resource> resources = resolver.findResources(QUERY, Query.JCR_SQL2);
                final UserManager userManager = resolver.adaptTo(UserManager.class);
                while (resources.hasNext()) {
                    Resource resource = resources.next();
                    Authorizable authorizable = userManager.getAuthorizableByPath(resource.getPath());
                    // if (getGroupIds(authorizable.memberOf(), groupName) ||
                    // getGroupIds(authorizable.declaredMemberOf(), groupName)) {
                    if (isUserFromTheGroup(authorizable.declaredMemberOf(), groupName)) {
                        LOGGER.debug("User Id: " + authorizable.getPrincipal().getName());
                        ValueMap properties = resource.getValueMap();
                        String firstName = properties.get("profile/givenName", "");
                        String lastName = properties.get("profile/familyName", "");
                        JSONObject user = new JSONObject();
                        user.put("value", authorizable.getPrincipal().getName());
                        user.put("content", new JSONObject().put("textContent", (firstName + " " + lastName)));
                        usersList.put(user);
                    }
                }
                rootObj.put("items", usersList);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return rootObj.toString();
    }

    /**
     * checking is a group belongs to a given group.
     *
     * @param groups    list of the all the group the user belongs to
     * @param groupName group name to check for
     * @return true if the user belongs to a given group else false
     * @throws RepositoryException the repository expection
     */
    private boolean isUserFromTheGroup(Iterator<Group> groups, String groupName) throws RepositoryException {
        while (groups.hasNext()) {
            Group group = groups.next();
            if (StringUtils.isNotBlank(group.getID()) && group.getID().equals(groupName)) {
                return true;
            }
        }
        return false;
    }
}
