package com.nswbc.commons.core.models.advocacy;

import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.nswbc.commons.core.constants.Constants;

/**
 * @author yuvraj.bansal
 *
 */
@Model(adaptables = SlingHttpServletRequest.class,
        resourceType = "nswbc-commons/components/content/emailsignup-advocacy",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class AdvocacyModel {
    /**
     * private inject variable for subscription type.
     */
    @Inject
    @Via("resource")
    private String subscriptionType;
    /**
     * private inject variable for alignment type.
     */
    @Inject
    @Via("resource")
    private String alignType;
    /**
     * private inject variable for background color.
     */
    @Inject
    @Via("resource")
    private String bgColor;
    /**
     * private inject variable for source.
     */
    @Inject
    @Via("resource")
    @Default(values = "AEM")
    private String source;
    /**
     * private inject variable for text color.
     */
    @Inject
    @Via("resource")
    private String textColor;
    /**
     * private inject variable for heading type.
     */
    @Inject
    @Via("resource")
    private String headingType;
    /**
     * private inject variable for image.
     */
    @Inject
    @Via("resource")
    private String fileReference;
    /**
     * private inject variable for image alt text.
     */
    @Inject
    @Via("resource")
    private String imgAltText;
    /**
     * private inject variable for heading text.
     */
    @Inject
    @Via("resource")
    private String headingText;
    /**
     * private inject variable for subheadingType type.
     */
    @Inject
    @Via("resource")
    private String subheadingType;
    /**
     * private inject variable for subheading text.
     */
    @Inject
    @Via("resource")
    private String subheadingText;
    /**
     * private inject variable for email field label.
     */
    @Inject
    @Via("resource")
    private String emailfieldLabel;
    /**
     * private inject variable for opt in text.
     */
    @Inject
    @Via("resource")
    private String optinText;
    /**
     * private inject variable for opt in subtext.
     */
    @Inject
    @Via("resource")
    private String optinSubText;
    /**
     * private inject variable for campaign id.
     */
    @Inject
    @Via("resource")
    private String campaignId;
    /**
     * private inject variable for thanks text.
     */
    @Inject
    @Via("resource")
    private String thanksText;
    /**
     * private inject variable for thanks subtext.
     */
    @Inject
    @Via("resource")
    private String thanksSubText;

    /**
     * current resource.
     */
    @Inject
    private Resource resource;

    /** private sling object for resource resolver. */
    @Inject
    private ResourceResolver resourceResolver;

    /** Constant for dc:title. */
    private static final String DC_TITLE = "dc:title";

    /**
     * Init method for this model class.
     */
    @PostConstruct
    private void initModel() {
        if (Objects.nonNull(fileReference)) {
            Resource resource = resourceResolver.getResource(fileReference);
            if (Objects.nonNull(resource)) {
                Asset asset = resource.adaptTo(Asset.class);
                if (StringUtils.isEmpty(imgAltText)) {
                    this.imgAltText = asset.getMetadataValue(DC_TITLE);
                }
            }
        }
    }

    /**
     * @return the subscriptionType
     */
    public String getSubscriptionType() {
        return subscriptionType;
    }

    /**
     * @return the alignType
     */
    public String getAlignType() {
        return alignType;
    }

    /**
     * @return the bgColor
     */
    public String getBgColor() {
        return bgColor;
    }

    /**
     * @return the textColor
     */
    public String getTextColor() {
        return textColor;
    }

    /**
     * @return the headingType
     */
    public String getHeadingType() {
        return headingType;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @return the fileReference
     */
    public String getFileReference() {
        return fileReference;
    }

    /**
     * This is to generate uniqueID to be used with a checkbox and its label.
     *
     * @return unique ID based in resource path.
     */
    public String getUniqueID() {
        String id = StringUtils.EMPTY;
        if (Objects.nonNull(resource)) {
            String resPath = resource.getPath();
            id = resPath.substring(resPath.indexOf(JcrConstants.JCR_CONTENT) + Constants.Number.ELEVEN);
        }
        return id;
    }

    /**
     * @return the imgAltText
     */
    public String getImgAltText() {
        return imgAltText;
    }

    /**
     * @return the headingText
     */
    public String getHeadingText() {
        return headingText;
    }

    /**
     * @return the subheadingType
     */
    public String getSubheadingType() {
        return subheadingType;
    }

    /**
     * @return the subheadingText
     */
    public String getSubheadingText() {
        return subheadingText;
    }

    /**
     * @return the emailfieldLabel
     */
    public String getEmailfieldLabel() {
        return emailfieldLabel;
    }

    /**
     * @return the optinText
     */
    public String getOptinText() {
        return optinText;
    }

    /**
     * @return the opt in subtext
     */
    public String getOptinSubText() {
        return optinSubText;
    }

    /**
     * @return the campaignId
     */
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * @return the thanksText
     */
    public String getThanksText() {
        return thanksText;
    }

    /**
     * @return the thanksSubText
     */
    public String getThanksSubText() {
        return thanksSubText;
    }
}
