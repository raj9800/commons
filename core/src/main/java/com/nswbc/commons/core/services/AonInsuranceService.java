package com.nswbc.commons.core.services;

import com.google.gson.JsonObject;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

/**
 * Aon Insurance service interface.
 */

public interface AonInsuranceService {

    /**
     * This method provides the AonInsurance URL where the customer will be redirected
     * after CRM successfully registers the aon insurance interest of the logged in customer.
     * @param request
     * @param response
     * @return
     * JsonObject
     */
    JsonObject getAonInsuranceURI(SlingHttpServletRequest request, SlingHttpServletResponse response);
}
