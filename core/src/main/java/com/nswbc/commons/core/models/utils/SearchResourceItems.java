package com.nswbc.commons.core.models.utils;

import java.util.Calendar;
import java.util.Collection;
import java.util.Objects;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.wcm.core.components.models.ListItem;
import com.day.cq.wcm.api.Page;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.models.breadcrumb.BreadcrumbModel;
import com.nswbc.commons.core.models.breadcrumb.BreadcrumbNavigationItem;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * @author yuvraj.bansal
 *
 */
public class SearchResourceItems implements ListItem {

    /** private variable servlet request. */
    private SlingHttpServletRequest request;
    /** private constant for page. */
    private Page page;
    /** private constant for breadcrumb class. */
    private BreadcrumbModel brModel;
    /** private variable to track if an article is premium or not. */
    private boolean isArticlePremium;

    /**
     * @param request
     * @param page
     */
    public SearchResourceItems(SlingHttpServletRequest request, Page page) {
        this.request = request;
        this.page = page;
        // only for article pages
        isArticlePremium = page.getProperties().containsKey("isPremium");
        brModel = request.adaptTo(BreadcrumbModel.class);
        if (Objects.nonNull(brModel)) {
            setBreadcrumbDetails(brModel, request, page);
        }
    }

    /**
     * @param brModel
     * @param request
     * @param page    This method is used to set the breadcrumb policy details.
     */
    private void setBreadcrumbDetails(BreadcrumbModel brModel, SlingHttpServletRequest request, Page page) {
        ValueMap breadCrumbPolicy = ResourceUtils.getPolicyDetails(request,
                Constants.Application.BREADCRUMB_RESOURCE_TYPE);
        brModel.setCurrentPage(page);
        brModel.setHideCurrent(breadCrumbPolicy.get("hideCurrent", Boolean.valueOf(false)).booleanValue());
        brModel.setShowHidden(breadCrumbPolicy.get("showHidden", Boolean.valueOf(false)).booleanValue());
        brModel.setStartLevel(breadCrumbPolicy.get("startLevel", Integer.valueOf(Constants.Number.THREE)).intValue());

    }

    /**
     * This method is use to get URL of a page.
     */
    @Override
    public String getURL() {
        return ResourceUtils.getResolvedPath(request.getResource().getResourceResolver(), page.getPath());
    }

    /**
     * This method is use to get title of a page.
     */
    @Override
    public String getTitle() {
        String title = page.getNavigationTitle();
        if (title == null) {
            title = page.getPageTitle();
        }
        if (title == null) {
            title = page.getTitle();
        }
        if (title == null) {
            title = page.getName();
        }
        return title;
    }

    /**
     * This method is use to get description of a page.
     */
    @Override
    public String getDescription() {
        return page.getDescription();
    }

    /**
     * This method is use to get last modified date of a page.
     */
    @Override
    public Calendar getLastModified() {
        return page.getLastModified();
    }

    /**
     * This method is use to get page path.
     */
    @Override
    public String getPath() {
        return page.getPath();
    }

    /**
     * This method is use to get name of the page node.
     */
    @Override
    @JsonIgnore
    public String getName() {
        return page.getName();
    }

    /**
     * @return This method is used to get the breadcrumb details associated with a
     *         page.
     */
    public Collection<BreadcrumbNavigationItem> getBreadcrumbDetails() {
        return brModel.getBreadcrumbItems();
    }

    /**
     * Getter to check if an article is premium.
     *
     * @return isArticlePremium;
     */
    public boolean getIsArticlePremium() {
        return isArticlePremium;
    }
}
