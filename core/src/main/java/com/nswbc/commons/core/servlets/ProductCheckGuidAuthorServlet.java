package com.nswbc.commons.core.servlets;

import static com.nswbc.commons.core.servlets.ProductCheckGuidAuthorServlet.SERVLET_PATH;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.ProductCheckGuidService;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * ProductCheckGuidAuthorServlet Class.
 *
 * @author riccardo.teruzzi
 */
@Component(service = Servlet.class,
        property = {"sling.servlet.selectors=" + ProductCheckGuidAuthorServlet.GUID_CHECK_SELECTOR,
                "sling.servlet.paths=" + SERVLET_PATH, "sling.servlet.extensions=" + Constants.Text.JSON,
                "sling.servlet.methods=GET"})
public class ProductCheckGuidAuthorServlet extends SlingAllMethodsServlet {

    /**
     * private variable for log.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ProductCheckGuidAuthorServlet.class);

    /**
     * protected variable for servlet path.
     */
    protected static final String SERVLET_PATH = "/bin/nswbc/ba/productauthor";

    /**
     * protected selector variable for guid check validation.
     */
    protected static final String GUID_CHECK_SELECTOR = "guidcheck";

    /**
     * Constant for status.
     */
    private static final String STATUS = "status";

    /**
     * The constant PUBLISH.
     */
    private static final String PUBLISH = "publish";

    /**
     * private reference for product configuration.
     */
    @Reference
    private ProductCheckGuidService productCheckGuidService;

    /**
     * private reference for slingSettingsService.
     */
    @Reference
    private SlingSettingsService slingSettingsService;

    /**
     * doGetMethod to call various services.
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        response.setCharacterEncoding(Constants.Application.UTF8);
        if (!slingSettingsService.getRunModes().contains(PUBLISH)) {
            String[] selectors = request.getRequestPathInfo().getSelectors();
            String selector = selectors[0];
            if (StringUtils.equals(GUID_CHECK_SELECTOR, selector)) {
                JsonObject result = productCheckGuidService.checkGuidCode(request, response);
                printResponseResult(response, result);
            }
        } else {
            RestUtil.getBadRequestResponse(response);
            JsonObject resultJson = new JsonObject();
            resultJson.addProperty(STATUS, Constants.Number.ZERO);
            printResponseResult(response, resultJson);
        }
    }

    /**
     * @param response
     * @param result
     * @throws IOException This method checks the output result
     */
    private void printResponseResult(SlingHttpServletResponse response, JsonObject result) throws IOException {
        if (Objects.nonNull(result)) {
            response.getWriter().print(result);
        }
    }
}
