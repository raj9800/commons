package com.nswbc.commons.core.models.timeline;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.day.cq.wcm.api.Page;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * @author yuvraj.bansal
 *
 */
@Model(adaptables = Resource.class,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class TimeLineSectionBean {

    /** private variable for milestoneDescription. */
    @Inject
    private String milestoneDescription;
    /** private variable for milestoneCategory. */
    @Inject
    private String milestoneCategory;
    /** private variable for month. */
    @Inject
    private String month;
    /** private variable for linkPath. */
    @Inject
    private String linkTo;
    /** private variable for linkTarget. */
    @Inject
    private String linkTarget;
    /** private variable for status. */
    @Inject
    private String status;
    /** private variable for resource. */
    @Inject
    private Resource resource;
    /**
     * Resource resolver object.
     */
    @Inject
    private ResourceResolver resourceResolver;

    /**
     * @return the year
     */
    public String getYear() {
        return resource.getParent().getParent().getValueMap().get("cq:panelTitle").toString();
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @return the milestoneDescription
     */
    public String getMilestoneDescription() {
        return milestoneDescription;
    }

    /**
     * @return the milestoneCategory
     */
    public String getMilestoneCategory() {
        return milestoneCategory;
    }

    /**
     * @return the month
     */
    public String getMonth() {
        return month;
    }

    /**
     * @return the linkPath
     */
    public String getLinkTo() {
        return ResourceUtils.getResolvedPath(resourceResolver, linkTo);
    }

    /**
     * @return the linkTarget
     */
    public String getLinkTarget() {
        return linkTarget;
    }

    /**
     * @return title
     * This method is use to get title of a page.
     */
    public String getTitle() {
        Page page = resourceResolver.getResource(linkTo).adaptTo(Page.class);
        String title = page.getNavigationTitle();
        if (title == null) {
            title = page.getPageTitle();
        }
        if (title == null) {
            title = page.getTitle();
        }
        if (title == null) {
            title = page.getName();
        }
        return title;
    }

}
