package com.nswbc.commons.core.models.breadcrumb;

/**
 * @author Yuvraj.Bansal. BreadcrumbNavigationItem Class
 */
public final class BreadcrumbNavigationItem {

    /** private variable for breadcrumb item url. */
    private String url;
    /** private variable for breadcrumb item title. */
    private String title;
    /** private variable for breadcrumb item is current page. */
    private boolean isCurrentPage;
    /** private variable for islinkRemoved. */
    private boolean isLinkRemoved;

    /**
     * @return url.
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url.
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return isCurrentPage.
     */
    public boolean isCurrentPage() {
        return isCurrentPage;
    }

    /**
     * @param isCurrentPage
     */
    public void setCurrentPage(boolean isCurrentPage) {
        this.isCurrentPage = isCurrentPage;
    }

    /**
     * @param isLinkRemoved
     */
    public void setLinkRemoved(boolean isLinkRemoved) {
        this.isLinkRemoved = isLinkRemoved;
    }

    /**
     * @return isLinkRemoved
     */
    public boolean isLinkRemoved() {
        return isLinkRemoved;
    }
}
