package com.nswbc.commons.core.models.carousel;

import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.adobe.cq.export.json.ComponentExporter;
import com.day.cq.dam.api.Asset;
import com.nswbc.commons.core.models.link.LinkModel;

/**
 * Carousel Panel Model class.
 *
 * @author rajat.pachouri
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        adapters = {ComponentExporter.class}, resourceType = "nswbc-commons/components/content/carousel-panel")
public class CarouselPanel implements ComponentExporter {

    /** Constant for dc:title. */
    private static final String DC_TITLE = "dc:title";
    /**
     * constant for jcr content.
     */
    private static final String JCR_CONTENT = "/jcr:content";

    /** private variable to show title in carousel. */
    @Inject
    @Via("resource")
    private String title;

    /** private variable to show category in carousel. */
    @Inject
    @Via("resource")
    private String category;

    /** private current resource object. */
    @Inject
    private Resource resource;

    /** private sling object for resource resolver. */
    @Inject
    private ResourceResolver resourceResolver;

    /** private variable to show link in carousel. */
    @Self
    private LinkModel link;

    /** private variable to show image in carousel. */
    @Inject
    @Via("resource")
    private String fileReference;

    /** private variable to show alt text for image. */
    @Inject
    @Via("resource")
    private String imgAltText;

    /** private variable for linkTo. */
    @Inject
    @Via("resource")
    private String linkTo;

    /**
     * Is article premium.
     */
    private String isArticlePremium;

    /**
     * getter to check if an article is a premium article.
     *
     * @return isArticlePremium
     */
    public String getIsArticlePremium() {
        return isArticlePremium;
    }

    /**
     * Getter method for title.
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Getter method for category.
     *
     * @return category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Getter method for file reference.
     *
     * @return fileReference
     */
    public String getFileReference() {
        return fileReference;
    }

    /**
     * Getter method for image alt text.
     *
     * @return alt text given via author, if not given, returns title set in the dam
     *         for image
     */
    public String getImgAltText() {
        String title = StringUtils.EMPTY;
        if (Objects.nonNull(fileReference)) {
            Resource resource = resourceResolver.getResource(fileReference);
            Asset asset = resource.adaptTo(Asset.class);
            title = asset.getMetadataValue(DC_TITLE);
        }
        if (Objects.nonNull(imgAltText)) {
            return imgAltText;
        } else {
            return title;
        }
    }

    /**
     * Getter method for link object.
     *
     * @return link object
     */
    public LinkModel getLink() {
        return link;
    }

    /**
     * Getter method for exported type.
     *
     * @return title
     */
    @Override
    public String getExportedType() {
        return resource.getResourceType();
    }

    /**
     * To check if a page is premium or not.
     */
    @PostConstruct
    public void init() {
        if (null != linkTo && null != resourceResolver) {
            Resource linkToRes = resourceResolver.getResource(linkTo + JCR_CONTENT);
            if (Objects.nonNull(linkToRes)) {
                if (linkToRes.getValueMap().containsKey("isPremium")) {
                    isArticlePremium = "true";
                }
            }
        }

    }
}
