package com.nswbc.commons.core.listeners;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.observation.ResourceChange;
import org.apache.sling.api.resource.observation.ResourceChangeListener;
import org.apache.sling.settings.SlingSettingsService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.replication.dispatcher.DispatcherFlusher;
import com.day.cq.replication.Agent;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationResult;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.TagUtils;

/**
 * The type Page activation listener.
 */
@Component(
        service = ResourceChangeListener.class,
        property = {
                ResourceChangeListener.PATHS + "=" + "/content/nswbc",
                ResourceChangeListener.CHANGES + "=" + "ADDED",
                ResourceChangeListener.CHANGES + "=" + "CHANGED"
        })
public class PageActivationListener implements ResourceChangeListener {

    /**
     * Constant for Logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(PageActivationListener.class);

    /**
     * The constant PUBLISH.
     */
    private static final String PUBLISH = "publish";

    /**
     * AUTH_INFO.
     */
    private static final Map<String, Object> AUTH_INFO = Collections.singletonMap("sling.service.subservice", "acs-commons-dispatcher-flush-service");

    /**
     * dispatcherFlusher.
     */
    @Reference
    private DispatcherFlusher dispatcherFlusher;

    /**
     * resourceResolverFactory.
     */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /**
     * Query Builder.
     */
    @Reference
    private QueryBuilder queryBuilder;

    /**
     * private reference for slingSettingsService.
     */
    @Reference
    private SlingSettingsService slingSettingsService;

    /**
     * on change method. The method handles the flushing of pages with tags in
     * common and dynamic carousel
     */
    @Override
    public void onChange(List<ResourceChange> list) {
        if (slingSettingsService.getRunModes().contains(PUBLISH)) {
            // changes are all related to the nodes of the same page. I just need to get one
            // path and obtain the page path
            ResourceChange resourceChange = list.get(0);
            String resourcePath = resourceChange.getPath();
            Page currentPage = null;
            try (ResourceResolver flushingResourceResolver = resourceResolverFactory.getServiceResourceResolver(AUTH_INFO)) {
                PageManager pageManager = flushingResourceResolver.adaptTo(PageManager.class);
                if (pageManager != null) {
                    currentPage = pageManager.getContainingPage(resourcePath);
                }
                if (currentPage != null) {
                    Page homepage = currentPage.getAbsoluteParent(com.nswbc.commons.core.constants.Constants.Number.FOUR);
                    List<String> pathsWithSimilarTags = findPagePathWithSimilarTags(currentPage, homepage, flushingResourceResolver);
                    List<String> pathsWithSimilarCategory = findPageWithSimilarCategory(currentPage, homepage, flushingResourceResolver);
                    if (Objects.nonNull(pathsWithSimilarTags) && Objects.nonNull(pathsWithSimilarCategory)) {
                        flushPages(flushingResourceResolver, pathsWithSimilarTags);
                        flushPages(flushingResourceResolver, pathsWithSimilarCategory);
                    }
                }
            } catch (ReplicationException e) {
                LOG.error("Replication exception occurred during Dispatcher Flush request.", e);
            } catch (LoginException e) {
                LOG.error("Failed to get Resource resolver", e);
            }
        }
    }

    /**
     * @param flushingResourceResolver
     * @param pagePaths
     * @throws ReplicationException This method flush the pages from dispatcher
     *                              based on pagePaths
     */
    private void flushPages(ResourceResolver flushingResourceResolver, List<String> pagePaths) throws ReplicationException {
        for (String pathToFlush : pagePaths) {
            LOG.info("Page path: " + pathToFlush);
            final Map<Agent, ReplicationResult> results = dispatcherFlusher.flush(flushingResourceResolver,
                    ReplicationActionType.ACTIVATE, true, pathToFlush);
            for (final Map.Entry<Agent, ReplicationResult> entry : results.entrySet()) {
                final Agent agent = entry.getKey();
                final ReplicationResult result = entry.getValue();
                LOG.info("Agent: " + agent.getId());
                LOG.info("Page '" + result.getMessage() + "'" + result.isSuccess());
            }
        }
    }

    /**
     * @param currentPage
     * @param homepage
     * @param resourceResolver
     * @return pageList This method is use to find the all the pages with dynamic
     *         category and same category
     */
    private List<String> findPageWithSimilarCategory(Page currentPage, Page homepage, ResourceResolver resourceResolver) {
        List<String> pageList = new ArrayList<>();
        if (homepage != null) {
            String pageCategory = currentPage.getProperties().get(Constants.Application.PAGE_CATEGORY, String.class);
            if (Objects.nonNull(pageCategory)) {
                Session session = resourceResolver.adaptTo(Session.class);
                Map<String, String> queryParameters = new HashMap<>();
                queryParameters.put("path", homepage.getPath());
                queryParameters.put("type", "cq:page");
                queryParameters.put("property.and", "true");
                queryParameters.put("1_property", "categoryType");
                queryParameters.put("1_property.value", "dynamic");
                queryParameters.put("1_property.depth", "6");
                queryParameters.put("2_property", Constants.Application.PAGE_CATEGORY);
                queryParameters.put("2_property.value", pageCategory);
                Query query = queryBuilder.createQuery(PredicateGroup.create(queryParameters), session);
                SearchResult result = query.getResult();
                if (result.getTotalMatches() > 0) {
                    pageList = iterateOverResults(currentPage, pageList, result);
                }
            }
        }
        return pageList;

    }

    /**
     * Find page paths with similar tags list and with carouselType dynamic.
     *
     * @param currentPage      the current page
     * @param homepage         the home page
     * @param resourceResolver the resource resolver
     * @return the list
     */
    private List<String> findPagePathWithSimilarTags(Page currentPage, Page homepage, ResourceResolver resourceResolver) {
        List<String> pageList = new ArrayList<>();
        if (homepage != null) {
            Tag[] currentPageTags = TagUtils.getPageTagsFilterByNamespace(currentPage);
            Session session = resourceResolver.adaptTo(Session.class);
            Map<String, String> queryParameters = new HashMap<>();
            queryParameters.put("path", homepage.getPath());
            queryParameters.put("type", "cq:page");
            queryParameters.put("property", "carouselType");
            queryParameters.put("property.value", "dynamic");
            queryParameters.put("property.depth", "6");
            if (currentPageTags != null && currentPageTags.length > 0) {
                queryParameters.put("group.p.or", "true");
                for (int i = 0; i < currentPageTags.length; i++) {
                    queryParameters.put("group." + i + "_tagid", currentPageTags[i].getTagID());
                    queryParameters.put("group." + i + "_tagid.property", "jcr:content" + "/" + "cq:tags");
                }
            }
            Query query = queryBuilder.createQuery(PredicateGroup.create(queryParameters), session);
            SearchResult result = query.getResult();
            if (result.getTotalMatches() > 0) {
                pageList = iterateOverResults(currentPage, pageList, result);
            }
        }
        return pageList;
    }

    /**
     * @param currentPage
     * @param pageList
     * @param result
     * @return pageList
     *   This method is use to iterate over query results
     */
    private List<String> iterateOverResults(Page currentPage, final List<String> pageList, SearchResult result) {
        // iterating over the results
        for (Hit hit : result.getHits()) {
            try {
                String path = hit.getPath();
                if (!StringUtils.equals(path, currentPage.getPath())) { // don't add in list if result is currentPage
                    pageList.add(path);
                }
            } catch (RepositoryException e) {
                LOG.error("RepositoryException while retrieving results", e);
            }
        }
        return pageList;
    }
}
