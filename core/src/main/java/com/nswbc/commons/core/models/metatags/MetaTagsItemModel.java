package com.nswbc.commons.core.models.metatags;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

/**
 * @author yuvraj.bansal
 *
 */
@Model(adaptables = Resource.class,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class MetaTagsItemModel {
    /** private variable for metatag type. */
    @Inject
    private String type;
    /** private variable for typeValue. */
    @Inject
    private String typeValue;
    /** private variable for meta tag content value. */
    @Inject
    private String contentValue;

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @return the typeValue
     */
    public String getTypeValue() {
        return typeValue;
    }

    /**
     * @return the contentValue
     */
    public String getContentValue() {
        return contentValue;
    }

}
