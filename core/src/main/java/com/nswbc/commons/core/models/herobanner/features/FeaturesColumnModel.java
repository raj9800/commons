package com.nswbc.commons.core.models.herobanner.features;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

/**
 * Features column sling model.
 *
 * @author rajat.pachouri
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class FeaturesColumnModel {

    /**
     * feature.
     */
    @Inject
    private String feature;

    /**
     * getter for feature.
     *
     * @return heading
     */
    public String getFeature() {
        return feature;
    }

}
