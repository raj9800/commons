
/*
 * #%L
 * ACS AEM Commons Bundle
 * %%
 * Copyright (C) 2018 Adobe
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.nswbc.commons.core.modelinjectors.utils;

import javax.jcr.Session;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.components.ComponentContext;
import com.day.cq.wcm.commons.WCMUtils;

/**
 * Common methods for the injectors.
 */
public final class InjectorUtils {

    /**
     * Constructor.
     */
    private InjectorUtils() {
        // static class
    }

    // --- public static stuff --
    /**
     * Get Resource object from adaptable.
     *
     * @param adaptable the adaptable object
     * @return resource
     */
    public static Resource getResource(Object adaptable) {
        if (adaptable instanceof SlingHttpServletRequest) {
            return ((SlingHttpServletRequest) adaptable).getResource();
        }
        if (adaptable instanceof Resource) {
            return (Resource) adaptable;
        }

        return null;
    }

    /**
     * Get ResourceResolver Object.
     *
     * @param adaptable the adaptable object
     * @return resolver
     */
    public static ResourceResolver getResourceResolver(Object adaptable) {
        if (adaptable instanceof SlingHttpServletRequest) {
            return ((SlingHttpServletRequest) adaptable).getResourceResolver();
        }
        if (adaptable instanceof Resource) {
            return ((Resource) adaptable).getResourceResolver();
        }

        return null;
    }

    /**
     * Get the PageManager.
     *
     * @param adaptable the adaptable objectg
     * @return PageManager object
     */
    public static PageManager getPageManager(Object adaptable) {
        ResourceResolver resolver = getResourceResolver(adaptable);

        if (resolver != null) {
            return resolver.adaptTo(PageManager.class);
        }

        return null;
    }

    /**
     * Get the current component context.
     *
     * @param adaptable a SlingHttpServletRequest
     * @return the ComponentContext if the adaptable was a SlingHttpServletRequest,
     *         or null otherwise
     */
    public static ComponentContext getComponentContext(Object adaptable) {
        if (adaptable instanceof SlingHttpServletRequest) {
            SlingHttpServletRequest request = ((SlingHttpServletRequest) adaptable);

            return WCMUtils.getComponentContext(request);
        }
        // ComponentContext is not reachable from Resource

        return null;
    }

    /**
     * Get the current page.
     *
     * @param adaptable a Resource
     * @return the current Page if the adaptable was a Resource, null otherwise
     */
    public static Page getResourcePage(Object adaptable) {
        PageManager pageManager = getPageManager(adaptable);
        Resource resource = getResource(adaptable);

        if (pageManager != null && resource != null) {
            return pageManager.getContainingPage(resource);
        }

        return null;
    }

    /**
     * Get the current page.
     *
     * @param adaptable a SlingHttpServletRequest
     * @return the current Page if the adaptable was a SlingHttpServletRequest, null
     *         otherwise
     */
    public static Page getCurrentPage(Object adaptable) {
        ComponentContext context = getComponentContext(adaptable);

        return (context != null) ? context.getPage() : null;
    }

    /**
     * Get the session.
     *
     * @param adaptable Either a SlingHttpServletRequest or a Resource
     * @return the current Session
     */
    public static Session getSession(Object adaptable) {
        ResourceResolver resolver = getResourceResolver(adaptable);

        return resolver != null ? resolver.adaptTo(Session.class) : null;
    }

}
