package com.nswbc.commons.core.workflow;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.HistoryItem;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationOptions;
import com.day.cq.replication.Replicator;
import com.day.cq.wcm.api.reference.ReferenceProvider;
import com.nswbc.commons.core.workflow.utils.WorkflowUtils;

/**
 * This workflow process is used to activate and deactivate pages and the assets referenced to them.
 * Note: Assets are only activated when the page is activated but not deactivated when a page is deactivated.
 * @author rajat.pachouri
 *
 */
@Component(service = WorkflowProcess.class, immediate = true, enabled = true, property = {"process.label= Resource "
        + "Activation Process for Business Australia"})
public class PageWorkflowResourceActivation implements WorkflowProcess {
    /**
     * referenceproviders to collect all types of reference provider which are used to collect the referenced assets to a page and publish them.
     */
    @Reference(service = ReferenceProvider.class, cardinality = ReferenceCardinality.MULTIPLE, policy = ReferencePolicy.DYNAMIC)
    private final List<ReferenceProvider> referenceProviders = new CopyOnWriteArrayList<ReferenceProvider>();
    /**
     * Workflow model to use to start schedule activation workflow.
     */
    private static final String SCHEDULED_ACTIVATION_WORKFLOW_MODEL = "/etc/workflow/models/scheduled_activation/jcr:content/model";
    /**
     * Workflow model to use to start schedule deactivation workflow.
     */
    private static final String SCHEDULED_DEACTIVATION_WORKFLOW_MODEL = "/etc/workflow/models/scheduled_deactivation/jcr:content/model";
    /**
     * Replicator service to publish pages.
     */
    @Reference
    private Replicator replicator;

    /**
     * LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PageWorkflowResourceActivation.class);

    /**
     * The method which is called when the process runs.
     */
    public void execute(WorkItem workitem, WorkflowSession wfsession, MetaDataMap metaDataMap) {
        Session session = wfsession.adaptTo(Session.class);
        String deactivationTime = StringUtils.EMPTY;
        String deactivationTimeAbsolute = StringUtils.EMPTY;
        //String activationTimeAssignee = StringUtils.EMPTY;
        String activationTime  = StringUtils.EMPTY;
        String activationTimeAbsolute  = StringUtils.EMPTY;
        //String deactivationTimeAssignee = StringUtils.EMPTY;
        List<com.day.cq.wcm.api.reference.Reference> allReferences = new ArrayList<com.day.cq.wcm.api.reference.Reference>();
        List<HistoryItem> list = null;
        List<String> args = null;
        boolean isActivationWorkflow = false; // temporary condition
        boolean isDeactivationWorkflow = false; // temporary condition
        ReplicationOptions opts = new ReplicationOptions();
        opts.setSynchronous(true);
        String payload = workitem.getWorkflowData().getPayload().toString();
        ResourceResolver resolver = wfsession.adaptTo(ResourceResolver.class);

        // get args
        args = WorkflowUtils.getArgsArray(metaDataMap);

        // determine if the workflow is for the activation or deactivation
        if (null != args && args.size() > 0) {
            if (args.contains("activation")) {
                isActivationWorkflow = true;
                LOGGER.error("ACtivation workflow");
            }
            if (args.contains("deactivation")) {
                isDeactivationWorkflow = true;
                LOGGER.error("Deativation workflow");
            }
        }
        // get workflow history list
        list = WorkflowUtils.getWorkflowHistoryList(wfsession, workitem);

        String value = StringUtils.EMPTY;
        // get activation time if the workflow is for the activation
        if (isActivationWorkflow) {
            value = WorkflowUtils.geteffectiveUserAndDateFromHistory(list, "overrideactivation", "actdate");
            if (StringUtils.isNotBlank(value)) {
                activationTime = WorkflowUtils.splitDetails(value)[0];
                //activationTimeAssignee = WorkflowUtils.splitDetails(value)[1];
                if (StringUtils.isNotBlank(activationTime)) {
                    activationTimeAbsolute = WorkflowUtils.getAbsoluteTime(activationTime);
                }
            }
        }
        value = WorkflowUtils.geteffectiveUserAndDateFromHistory(list, "overridedeactivation", "deactdate");
        if (StringUtils.isNotBlank(value)) {
            deactivationTime = WorkflowUtils.splitDetails(value)[0];
            //deactivationTimeAssignee = WorkflowUtils.splitDetails(value)[1];
            if (StringUtils.isNotBlank(deactivationTime)) {
                deactivationTimeAbsolute = WorkflowUtils.getAbsoluteTime(deactivationTime);
            }
        }

        //Handle activation
        if (isActivationWorkflow) {
            // Collecting the list of resources to be published
            List<String> publishList = new ArrayList<String>();
            publishList.add(payload);
            // collecting references i.e assets, content fragments, dynamic template, tags etc.
            allReferences = WorkflowUtils.getOutdatedAndUnpublishedReferences(resolver, payload, referenceProviders);
            // storing references in publishList
            if (null != allReferences && allReferences.size() > 0) {
                for (com.day.cq.wcm.api.reference.Reference ref : allReferences) {
                    if (null != ref) {
                        Resource res = ref.getResource();
                        if (null != res) {
                            publishList.add(res.getPath());
                        }
                    }
                }
            }
            // Queuing them up based on the workflow data
            if (publishList.size() > 0) {
                for (String pl : publishList) {
                    if (StringUtils.isNotBlank(activationTime)) {
                        WorkflowUtils.scheduleActivationAndDeactivation(wfsession, SCHEDULED_ACTIVATION_WORKFLOW_MODEL, activationTimeAbsolute, pl);
                    } else {
                        WorkflowUtils.scheduleImmediately(session, replicator, ReplicationActionType.ACTIVATE, pl, opts);
                    }
                }
            }
            // Schedule deactivation if deactivation time is available as well.
            if (StringUtils.isNotBlank(deactivationTime)) {
                WorkflowUtils.scheduleActivationAndDeactivation(wfsession, SCHEDULED_DEACTIVATION_WORKFLOW_MODEL, deactivationTimeAbsolute,
                        payload);
            }
        }
        // Handle deactivation
        if (isDeactivationWorkflow) {
            if (StringUtils.isNotBlank(deactivationTime)) {
                WorkflowUtils.scheduleActivationAndDeactivation(wfsession, SCHEDULED_DEACTIVATION_WORKFLOW_MODEL, deactivationTimeAbsolute,
                        payload);
            } else {
                WorkflowUtils.scheduleImmediately(session, replicator, ReplicationActionType.DEACTIVATE,
                        payload, opts);

            }
        }
    }
//    boolean canReplicate(String path, Session session) throws RepositoryException {
//        AccessControlManager acMgr = session.getAccessControlManager();
//        return session.getAccessControlManager().hasPrivileges(path,
//    new Privilege[]{acMgr.privilegeFromName("{http://www.day.com/crx/1.0}replicate")});
//    }
}
