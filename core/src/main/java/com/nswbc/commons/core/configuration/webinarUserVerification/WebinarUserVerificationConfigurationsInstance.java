package com.nswbc.commons.core.configuration.webinarUserVerification;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;

/**
 * Component of the Webinar user verification configuration Instance.
 *
 * @author rajat.pachouri
 */

@Component(immediate = true, service = WebinarUserVerificationConfigurationsInstance.class,
        configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = WebinarUserVerificationConfigurations.class)
public class WebinarUserVerificationConfigurationsInstance {

    /**
     * private configurations.
     */
    private WebinarUserVerificationConfigurations config;

    /**
     * This method will run when the component is activated.
     *
     * @param config
     */
    @Activate
    public void activate(WebinarUserVerificationConfigurations config) {
        this.config = config;
    }

    /**
     * This method returns the configuration.
     *
     * @return config
     */
    public WebinarUserVerificationConfigurations getConfig() {
        return config;
    }
}
