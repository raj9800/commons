package com.nswbc.commons.core.configuration.myaccount;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = MyAccountAPIConfigurationsInstance.class,
        configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = MyAccountAPIConfigurations.class)
public class MyAccountAPIConfigurationsInstance {
    /** private configuration variable. */
    private MyAccountAPIConfigurations config;

    /**
     * @param config
     */
    @Activate
    public void activate(MyAccountAPIConfigurations config) {
        this.config = config;
    }

    /**
     * @return the config
     */
    public MyAccountAPIConfigurations getConfig() {
        return config;
    }
}
