package com.nswbc.commons.core.utils;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.day.cq.wcm.api.Page;

/**
 * This class is used to get brand name from aem.
 *
 * @author yuvraj.bansal
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class BrandName {
    /**
     * Current page object.
     */
    @Inject
    private Page currentPage;

    /**
     * This section populates brand ID for page analytics.
     *
     * @return brand
     */
    public String getBrandName() {
        return ResourceUtils.populateBrand(currentPage);
    }
}
