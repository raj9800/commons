package com.nswbc.commons.core.configuration.emailsubscribe;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author yuvraj.bansal Interface for configuration
 *
 */
@ObjectClassDefinition(name = "Gated Content API Configurations", description = "Service Configuration")
public @interface SubscriptionAPIServiceConfiguration {
    /**
     * @return subscriptionAPI
     */
    @AttributeDefinition(name = "Subscription Registration API", description = "Provide the api url for email subscription.")
    String subscriptionAPI();
    /**
     * @return tokenValidationAPI
     */
    @AttributeDefinition(name = "Token Validation API", description = "Provide the api url for token validation.")
    String tokenValidationAPI();

    /**
     * @return subscriptionKey
     */
    @AttributeDefinition(name = "Subscription Key", description = "Provide the subscription key.")
    String subscriptionKey();

    /**
     * @return decryptionSecretKey
     */
    @AttributeDefinition(name = "Decryption Secret Key", description = "Provide the decryption secret key.")
    String decryptionSecretKey();

    /**
     * @return decryptionInitializationVectorKey
     */
    @AttributeDefinition(name = "Decryption Initializing Vector Key", description = "Provide the decryption initialization vector key.")
    String decryptionInitializationVectorKey();


}
