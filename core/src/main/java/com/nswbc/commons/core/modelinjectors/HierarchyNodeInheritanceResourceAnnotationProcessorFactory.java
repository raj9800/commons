/*
 * #%L
 * ACS AEM Commons Bundle
 * %%
 * Copyright (C) 2013 Adobe
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.nswbc.commons.core.modelinjectors;

import static org.apache.commons.lang3.StringUtils.isBlank;

import java.lang.reflect.AnnotatedElement;

import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.spi.injectorspecific.AbstractInjectAnnotationProcessor2;
import org.apache.sling.models.spi.injectorspecific.InjectAnnotationProcessor2;
import org.apache.sling.models.spi.injectorspecific.StaticInjectAnnotationProcessorFactory;
import org.osgi.service.component.annotations.Component;

/**
 * The annotation processor for the {@link HierarchyNodeInheritanceResource}
 * annotation.
 * <p>
 * Note: This can only be used together with Sling Models API bundle in version
 * 1.2.0 (due to the dependency on InjectionStrategy)
 */
@Component(service = StaticInjectAnnotationProcessorFactory.class)
public class HierarchyNodeInheritanceResourceAnnotationProcessorFactory
        implements StaticInjectAnnotationProcessorFactory {

    /**
     * Returns HierarchyNodeInheritanceResource annotation.
     */
    @Override
    public InjectAnnotationProcessor2 createAnnotationProcessor(AnnotatedElement element) {
        HierarchyNodeInheritanceResource annotation = element.getAnnotation(HierarchyNodeInheritanceResource.class);
        if (annotation != null) {
            return new HierarchyNodeResource(annotation);
        }
        return null;
    }

    /**
     * Class for injector.
     *
     * @author rajat.pachouri
     *
     */
    private class HierarchyNodeResource extends AbstractInjectAnnotationProcessor2 {
        /**
         * HierarchyNodeInheritanceResource annotation.
         */
        private final HierarchyNodeInheritanceResource annotation;

        /**
         * Constructor.
         *
         * @param annotation
         */
        HierarchyNodeResource(HierarchyNodeInheritanceResource annotation) {
            this.annotation = annotation;
        }

        /**
         * Returns injection strategy.
         */
        @Override
        public InjectionStrategy getInjectionStrategy() {
            return annotation.injectionStrategy();
        }

        /**
         * returns injector name.
         */
        @Override
        public String getName() {
            if (isBlank(annotation.value())) {
                return null;
            }
            return annotation.value();
        }

    }

}
