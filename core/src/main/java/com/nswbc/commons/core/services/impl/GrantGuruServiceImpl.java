package com.nswbc.commons.core.services.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.commons.core.configuration.grantguru.GrantGuruConfigurationsInstance;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.GrantGuruService;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * @author rajat.pachouri
 *
 */
@Component(immediate = true, service = GrantGuruService.class)
public class GrantGuruServiceImpl implements GrantGuruService {

    /**
     * Constant for Logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(GrantGuruServiceImpl.class);
    /**
     * Constant for Subscription key.
     */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    /**
     * Constant for Authorization.
     */
    private static final String AUTHORIZATION = "Authorization";

    /**
     * private reference variable for my account configuration.
     */
    @Reference
    private GrantGuruConfigurationsInstance grantGuruConfig;

    /**
     * This method fetches the tokens and create the grant guru URI with tokens as
     * parameters.
     */
    @Override
    public JsonObject getGrantGuruURI(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        String token = RestUtil.getTokenFromHeader(request, "token");
        if (Objects.nonNull(token)) {
            Map<String, String> headerMap = setHeadersMap(token);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            StringBuilder builder = new StringBuilder();
            HttpResponse restResponse = RestUtil.processRestGetRequest(client,
                    grantGuruConfig.getConfig().grantGuruTokensAPI(), builder.append(Constants.Application.EMPTY),
                    headerMap);
            if (Objects.nonNull(restResponse)
                    && !(restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED)) {
                if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED) {
                    RestUtil.closeHttpClient(client);
                    return RestUtil.getServerErrorResponse(response);
                } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED_ONE) {
                    RestUtil.closeHttpClient(client);
                    return RestUtil.getUnAuthorizedResponse(response);
                } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.TWO_HUNDRED) {
                    String ggURL = StringUtils.EMPTY;
                    try {
                        String responseEntity = EntityUtils.toString(restResponse.getEntity());
                        JsonObject tokens = new JsonParser().parse(responseEntity).getAsJsonObject();
                        ggURL = generateGrantGuruRedirectURL(tokens);
                    } catch (ParseException | IOException | URISyntaxException e) {
                        LOG.error("Unable to return Grant Guru redirect URL : ", e.getMessage());
                    }
                    RestUtil.closeHttpClient(client);
                    JsonObject resultJson = new JsonObject();
                    resultJson.addProperty("url", ggURL);
                    return resultJson;
                }
            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

    /**
     * This method takes tokens and generate the grant guru api uri to that is used
     * to redirect the user.
     *
     * @param tokens the tokens.
     * @return grant guru url
     * @throws URISyntaxException URI syntax exception.
     */
    private String generateGrantGuruRedirectURL(JsonObject tokens) throws URISyntaxException {
        String loginToken = StringUtils.EMPTY;
        String randomToken = StringUtils.EMPTY;
        String redirectAPI = grantGuruConfig.getConfig().grantGuruRedirectAPI();
        String ggURL = StringUtils.EMPTY;
        if (Objects.nonNull(tokens)) {
            if (tokens.has("LoginToken")) {
                loginToken = tokens.get("LoginToken").toString();
            }
            if (tokens.has("RandomToken")) {
                randomToken = tokens.get("RandomToken").toString();
            }
        }
        if (StringUtils.isNotBlank(loginToken) && StringUtils.isNotBlank(randomToken)
                && StringUtils.isNotBlank(redirectAPI)) {
            URIBuilder uri = new URIBuilder(redirectAPI);
            uri.addParameter("login", loginToken);
            uri.addParameter("token", randomToken);
            ggURL = uri.toString();
        }
        return ggURL;
    }

    /**
     * @return header map in request.
     */
    private Map<String, String> setHeadersMap(String token) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        headerMap.put(SUBSCRIPTION_KEY, grantGuruConfig.getConfig().subscriptionKey());
        headerMap.put(AUTHORIZATION, "Bearer " + token);
        return headerMap;
    }
}
