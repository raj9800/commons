package com.nswbc.commons.core.models.image;

import java.util.Objects;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.day.cq.dam.api.Asset;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * @author rajat.pachouri. Image Model Class.
 */
@Model(adaptables = Resource.class,
       defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class ImageModel {

    /** Constant for dc:title. */
    private static final String DC_TITLE = "dc:title";

    /** private sling object for resource resolver. */
    @Inject
    private ResourceResolver resourceResolver;

    /**
     * private inject variable for fileReference.
     */
    @Inject
    @Default(values = "")
    private String fileReference;

    /**
     * private inject variable for imgAltText.
     */
    @Inject
    @Default(values = "")
    private String imgAltText;

    /**
     * private inject variable for linkTo.
     */
    @Inject
    @Default(values = "")
    private String linkTo;

    /**
     * private inject variable for linkTo.
     */
    @Inject
    @Default(values = "")
    private String linkTarget;

    /**
     * @return fileReference
     */
    public String getFileReference() {
        return fileReference;
    }

    /**
     * @return linkTo
     */
    public String getLinkTo() {
        return ResourceUtils.getResolvedPath(resourceResolver, linkTo);
        //return linkTo;
    }
    /**
     * @return linkTarget
     */
    public String getLinkTarget() {
        return linkTarget;
    }
    /**
     * Getter method for image alt text.
     * @return alt text given via author, if not given, returns title set in the dam for image
     */
    public String getImgAltText() {
        String title = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(imgAltText)) {
            return imgAltText;
        } else {
            if (Objects.nonNull(fileReference)) {
                Resource resource = resourceResolver.getResource(fileReference);
                Asset asset = resource.adaptTo(Asset.class);
                title = asset.getMetadataValue(DC_TITLE);
            }
            return title;
        }
    }
}
