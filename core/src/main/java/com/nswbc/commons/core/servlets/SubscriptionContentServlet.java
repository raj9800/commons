package com.nswbc.commons.core.servlets;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.JsonObject;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.SubscriptionCampaignEmailCheckService;
import com.nswbc.commons.core.services.SubscriptionEmailSubscriptionService;
import com.nswbc.commons.core.services.SubscriptionValidateTokenService;

/**
 * @author yuvraj.bansal
 *
 */
@Component(
        service = Servlet.class,
        property = {
                "sling.servlet.selectors=" + SubscriptionContentServlet.EMAIL_SUBSCRIBE,
                "sling.servlet.selectors=" + SubscriptionContentServlet.VALIDATE,
                "sling.servlet.selectors=" + SubscriptionContentServlet.CAMPAIGN_EMAIL_CHECK,
                "sling.servlet.paths=" + SubscriptionContentServlet.SERVLET_PATH,
                "sling.servlet.extensions=" + Constants.Text.JSON,
                "sling.servlet.methods=GET",
                "sling.servlet.methods=POST"
        })
public class SubscriptionContentServlet extends SlingAllMethodsServlet {

    /** generated serial version uid. */
    private static final long serialVersionUID = 8559142970097907292L;
    /** protected variable for servlet path. */
    protected static final String SERVLET_PATH = "/bin/nswbc/subscription";
    /** protected selector variable for email submit. */
    protected static final String EMAIL_SUBSCRIBE = "emailSubscribe";
    /** protected selector variable for token validation. */
    protected static final String VALIDATE = "validateSubscribeToken";
    /** protected selector variable for campaign email check. */
    protected static final String CAMPAIGN_EMAIL_CHECK = "campaignEmailCheck";

    /** private reference variable for email verification. */
    @Reference
    private SubscriptionEmailSubscriptionService emailToken;
    /** private reference variable for abn crm verification. */
    @Reference
    private SubscriptionValidateTokenService validateTokenService;
    /** private reference variable for registration. */
    @Reference
    private SubscriptionCampaignEmailCheckService campaignEmailCheck;

    /**
     * doGet method to provide check between email and registration flow.
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        String selector = request.getRequestPathInfo().getSelectorString();
        if (selector.equals(VALIDATE)) {
            JsonObject result = validateTokenService.validateToken(request, response);
            printResponseResult(response, result);
        } else if (selector.equals(CAMPAIGN_EMAIL_CHECK)) {
            JsonObject result = campaignEmailCheck.verifyEmail(request, response);
            printResponseResult(response, result);
        }
    }

    /**
     * doPost method to send email and receive token.
     */
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws IOException {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        String selector = request.getRequestPathInfo().getSelectorString();
        if (selector.equals(EMAIL_SUBSCRIBE)) {
            JsonObject result = emailToken.getToken(request, response);
            printResponseResult(response, result);
        }
    }

    /**
     * @param response
     * @param result
     * @throws IOException This method checks the output result
     */
    private void printResponseResult(SlingHttpServletResponse response, JsonObject result) throws IOException {
        if (Objects.nonNull(result)) {
            response.getWriter().print(result);
        }
    }

}
