package com.nswbc.commons.core.workflow.configuration;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;

/**
 * @author rajat.pachouri
 *
 */
@Component(immediate = true, service = BrandGroupMappingServiceInstance.class,
        configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = WorkflowBrandGroupMapping.class, factory = true)
public class BrandGroupMappingServiceInstance {
    /** private configuration variable. */
    private WorkflowBrandGroupMapping config;

    /**
     * @param config
     */
    @Activate
    public void activate(WorkflowBrandGroupMapping config) {
        this.config = config;
    }

    /**
     * @return the config
     */
    public WorkflowBrandGroupMapping getConfig() {
        return config;
    }

}
