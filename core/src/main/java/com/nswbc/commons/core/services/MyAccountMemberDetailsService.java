package com.nswbc.commons.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author yuvraj.bansal
 *
 */
public interface MyAccountMemberDetailsService {
    /**
     * @param request
     * @param response
     * @return member details
     */
    JsonObject getMemberDetails(SlingHttpServletRequest request, SlingHttpServletResponse response);
}
