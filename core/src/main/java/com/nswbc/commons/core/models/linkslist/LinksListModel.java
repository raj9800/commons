package com.nswbc.commons.core.models.linkslist;

import com.day.cq.wcm.api.Page;
import com.nswbc.commons.core.models.link.LinkModel;
import com.nswbc.commons.core.utils.ResourceUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * LinksListComponentModel Class.
 *
 * @author sandeep.rawat
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/linksList",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class LinksListModel {

    /**
     * The constant LINKS_LIST.
     */
    public static final String LINKS_LIST = "linksList";

    /**
     * The Mobile view display label.
     */
    @Inject
    @Via("resource")
    private String mobileViewDisplayLabel;


    /**
     * Gets mobile view display label.
     *
     * @return the mobile view display label
     */
    public String getMobileViewDisplayLabel() {
        return mobileViewDisplayLabel;
    }

    /**
     * The self resource.
     */
    @SlingObject
    private Resource resource;

    /**
     * Resource resolver object.
     */
    @Inject
    private ResourceResolver resourceResolver;

    /** Script Variable for current page. */
    @ScriptVariable
    @Optional
    private Page currentPage;

    /**
     * Gets current page path.
     *
     * @return currentPagePath current page
     */
    public String getCurrentPagePath() {
        return ResourceUtils.getResolvedPath(resourceResolver, currentPage.getPath());
    }

    /**
     * Gets links list.
     *
     * @return linksList links list
     */
    public List<LinkModel> getLinksList() {
        ArrayList<LinkModel> adaptedList = new ArrayList<>();
        Resource linksList = resource.getChild(LINKS_LIST);
        if (Objects.nonNull(linksList)) {
            Iterator<Resource> resourceIterator = linksList.listChildren();
            while (resourceIterator.hasNext()) {
                Resource res = resourceIterator.next();
                LinkModel link = res.adaptTo(LinkModel.class);
                if (link != null) {
                    adaptedList.add(link);
                }
            }
        }
        return adaptedList;
    }

}
