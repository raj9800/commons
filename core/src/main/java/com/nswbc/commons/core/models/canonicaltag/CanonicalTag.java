package com.nswbc.commons.core.models.canonicaltag;

import java.util.Objects;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import com.nswbc.commons.core.modelinjectors.HierarchyNodeInheritanceResource;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * @author yuvraj.bansal
 *
 */
@Model(adaptables = SlingHttpServletRequest.class,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class CanonicalTag {
    /** private variable for brand name. */
    @HierarchyNodeInheritanceResource("brandName")
    private String brandName;
    /** Script Variable for current page. */
    @ScriptVariable
    @Optional
    private Page currentPage;

    /** private sling object for resource resolver. */
    @Inject
    private ResourceResolver resourceResolver;

    /**
     * @return current page path with domain
     */
    public String getCurrentPagePath() {
        String currentPagePath = ResourceUtils.getResolvedPath(resourceResolver, currentPage.getPath());
        if (!Objects.isNull(currentPagePath) && StringUtils.isNotBlank(brandName)) {
            Externalizer externalizer = resourceResolver.adaptTo(Externalizer.class);
            String fullPath = externalizer.externalLink(resourceResolver, brandName, currentPagePath);
            if (Objects.isNull(fullPath)) {
                return currentPagePath;
            }
            return fullPath;
        }
        return null;
    }
}
