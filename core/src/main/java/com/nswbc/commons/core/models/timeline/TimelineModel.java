package com.nswbc.commons.core.models.timeline;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.nswbc.commons.core.models.utils.ComponentSelectorUtils;

/**
 * @author yuvraj.bansal. Model for Sticky Menu.
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/timeline",
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class TimelineModel extends ComponentSelectorUtils {


    /** private variable for headerText. */
    @Inject
    @Via("resource")
    private String headerText;

    /**
     * @return the headerText
     */
    public String getHeaderText() {
        return headerText;
    }
}
