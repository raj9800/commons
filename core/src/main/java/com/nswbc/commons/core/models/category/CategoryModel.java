package com.nswbc.commons.core.models.category;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.nswbc.commons.core.models.utils.ComponentSelectorUtils;

/**
 * @author yuvraj.bansal
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/category",
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class CategoryModel extends ComponentSelectorUtils {
    /** private variable for headerText. */
    @Inject
    @Via("resource")
    private String headerText;
    /** private variable for showAllCategories. */
    @Inject
    @Via("resource")
    private String showAllCategories;

    /**
     * @return the showAllCategories
     */
    public String getShowAllCategories() {
        return showAllCategories;
    }

    /**
     * @return the headerText
     */
    public String getHeaderText() {
        return headerText;
    }
}

