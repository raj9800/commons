package com.nswbc.commons.core.configuration.aoninsurance;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Interface for Aon Insurance configurations. author ranjeet.singh
 */

@ObjectClassDefinition(name = "AON Insurance API Configurations",
        description = "This config has all the configuration for the AON Insurance API ")
public @interface AonInsuranceConfigurations {

    /**
     * @return subscriptionKey.
     */
    @AttributeDefinition(name = "Subscription Key", description = "Provide the subscription key.")
    String subscriptionKey();

    /**
     * @return aonInsuranceAPI.
     */
    @AttributeDefinition(name = "AON Insurance API", description = "Provide AON Insurance API")
    String aonInsuranceAPI();

    /**
     * @return aonInsuranceURL.
     */
    @AttributeDefinition(name = "AON Insurance Redirect URL", description = "Provide AON Insurance Redirect URL")
    String aonInsuranceRedirectURL();

}
