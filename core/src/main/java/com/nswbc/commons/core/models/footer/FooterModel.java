package com.nswbc.commons.core.models.footer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import com.adobe.aemds.guide.utils.JcrResourceConstants;
import com.adobe.cq.export.json.ComponentExporter;
import com.nswbc.commons.core.modelinjectors.HierarchyNodeInheritanceResource;
import com.nswbc.commons.core.models.image.ImageModel;
import com.nswbc.commons.core.models.link.LinkModel;

/**
 * Footer Model.
 *
 * @author rajat.pachouri
 *
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, adapters = {
        ComponentExporter.class})
public class FooterModel implements ComponentExporter {

    /** path of the section componennt. */
    private static final String FOOTER_SECTION_PATH = "nswbc-commons/components/content/footer-section";
    /** cq:panelTitle property. */
    private static final String CQ_PANEL_TITLE_PROPERTY = "cq:panelTitle";
    /** resource for parent node of the links for each section. */
    private static final String RESOURCE_LINKS = "links";
    /** private current resource object. */
    @Inject
    private Resource resource;
    /** private sling object for resource resolver. */
    @Inject
    private ResourceResolver resourceResolver;
    /** private variable to show logo in Footer. */
    @HierarchyNodeInheritanceResource("logo")
    private Resource logo;
    /** private variable to show logo in Footer. */
    @HierarchyNodeInheritanceResource("partof")
    private Resource partof;
    /** private variable to show copyrightText in Footer. */
    @HierarchyNodeInheritanceResource("copyrightText")
    private String copyrightText;
    /** private variable to show followUsLinks in Footer. */
    @HierarchyNodeInheritanceResource("followUsLinks")
    private List<Resource> followUsLinks;
    /** private variable to show category in Footer. */
    @HierarchyNodeInheritanceResource("quickLinks")
    private List<Resource> quickLinks;
    /** private variable to show category in Footer. */
    @HierarchyNodeInheritanceResource("serviceLinks")
    private List<Resource> serviceLinks;
    /** private variable to show category in Footer. */
    @HierarchyNodeInheritanceResource("sectionOneLinks")
    private List<Resource> sectionOneLinks;
    /** private variable to show category in Footer. */
    @HierarchyNodeInheritanceResource("sectionTwoLinks")
    private List<Resource> sectionTwoLinks;
    /** private variable to show category in Footer. */
    @HierarchyNodeInheritanceResource("sectionThreeLinks")
    private List<Resource> sectionThreeLinks;
    /** private variable to store all the direct child resources. */
    @HierarchyNodeInheritanceResource("all-childrens")
    private List<Resource> items;
    /** private variable to show category in Footer. */
    /**
     * Section one heading.
     */
    @HierarchyNodeInheritanceResource("sectionOneHeading")
    private String sectionOneHeading;
    /**
     * Section two heading.
     */
    @HierarchyNodeInheritanceResource("sectionTwoHeading")
    private String sectionTwoHeading;
    /**
     * Section three heading.
     */
    @HierarchyNodeInheritanceResource("sectionThreeHeading")
    private String sectionThreeHeading;

    /**
     * Getter for logo.
     *
     * @return imagemodel.
     */
    public ImageModel getLogo() {
        return getImageModelFromResource(logo);
    }

    /**
     * Getter for get part of.
     *
     * @return imagemodel
     */
    public ImageModel getPartof() {
        return getImageModelFromResource(partof);
    }

    /**
     * returns image model from a resource.
     *
     * @return logo
     */
    private ImageModel getImageModelFromResource(Resource res) {
        ImageModel logo = null;
        if (Objects.nonNull(res)) {
            logo = res.adaptTo(ImageModel.class);
        }
        return logo;
    }

    /**
     * Getter for copyrightText.
     *
     * @return copyrightText.
     */
    public String getCopyrightText() {
        return copyrightText;
    }

    /**
     * Splits the value for follow us links into id and URL and returns a list.
     *
     * @return list
     */
    public List<LinkModel> getFollowUsLinks() {
        return getAdaptedList(followUsLinks);
    }

    /**
     * @return List of LinkModel
     */
    public List<LinkModel> getQuickLinks() {
        return getAdaptedList(quickLinks);
    }

    /**
     * @return List of LinkModel
     */
    public List<LinkModel> getServiceLinks() {
        return getAdaptedList(serviceLinks);
    }

    /**
     * @return List of LinkModel
     */
    public List<LinkModel> getSectionOneLinks() {
        return getAdaptedList(sectionOneLinks);
    }

    /**
     * @return List of LinkModel
     */
    public List<LinkModel> getSectionTwoLinks() {
        return getAdaptedList(sectionTwoLinks);
    }

    /**
     * @return List of LinkModel
     */
    public List<LinkModel> getSectionThreeLinks() {
        return getAdaptedList(sectionThreeLinks);
    }

    /**
     * @return List of LinkModel
     */
    public String getSectionOneHeading() {
        return sectionOneHeading;
    }

    /**
     * @return List of LinkModel
     */
    public String getSectionTwoHeading() {
        return sectionTwoHeading;
    }

    /**
     * @return List of LinkModel
     */
    public String getSectionThreeHeading() {
        return sectionThreeHeading;
    }

    /**
     * @return List of LinkModel
     */
    public Resource getResource() {
        return resource;
    }

    /**
     * @return List of LinkModel
     */
    public ResourceResolver getResourceResolver() {
        return resourceResolver;
    }

    /**
     * Getter method for exported type.
     *
     * @return title
     */
    @Override
    public String getExportedType() {
        return resource.getResourceType();
    }

    /**
     * This method adapts the resource to LinkModel and returns a list of
     * linkmodels.
     *
     * @param list List of resources to adapt to link model
     * @return adaptedList
     */
    public List<LinkModel> getAdaptedList(List<Resource> list) {
        ArrayList<LinkModel> adaptedList = null;
        if (Objects.nonNull(list) && list.size() > 0) {
            adaptedList = new ArrayList<LinkModel>();
            for (Resource res : list) {
                LinkModel link = res.adaptTo(LinkModel.class);
                adaptedList.add(link);
            }
        }
        return adaptedList;
    }

    /**
     * Return all the direct child resources under the component resource.
     *
     * @return list containing all the direct child resources
     */
    public List<Resource> getItems() {
        return items;
    }

    /**
     * Return all the section child resources.
     *
     * @return list containing section child resources
     */
    public List<Section> getSections() {
        List<Section> sections = new ArrayList<Section>();
        for (Resource res : items) {
            if (res.getValueMap().containsKey(JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY) && res.getValueMap()
                    .get(JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY).equals(FOOTER_SECTION_PATH)) {
                List<LinkModel> linkList = new ArrayList<LinkModel>();
                Section section = new Section(linkList);
                section.heading = res.getValueMap().get(CQ_PANEL_TITLE_PROPERTY, StringUtils.EMPTY);
                section.path = res.getPath();
                if (res.hasChildren()) {
                    Resource linksWrapper = res.getChild(RESOURCE_LINKS);
                    if (linksWrapper.hasChildren()) {
                        for (Resource link : linksWrapper.getChildren()) {
                            linkList.add(link.adaptTo(LinkModel.class));
                        }
                    }
                }
                sections.add(section);
            }
        }
        return sections;
    }

    /**
     * class for section.
     *
     * @author rajat.pachouri
     *
     */
    public class Section {
        /** Path of the resource. */
        private String path = StringUtils.EMPTY;
        /** variable to store links. */
        private List<LinkModel> links;
        /** heading of the section. */
        private String heading = StringUtils.EMPTY;

        /**
         * getter for links.
         *
         * @return links
         */
        public List<LinkModel> getLinks() {
            return links;
        }

        /**
         * getter for path of the resource.
         *
         * @return path of the resource node.
         */
        public String getPath() {
            return path;
        }

        /**
         * getter for heading.
         *
         * @return heading for the section.
         */
        public String getHeading() {
            return heading;
        }

        /**
         * Constructor.
         *
         * @param links
         */
        Section(List<LinkModel> links) {
            this.links = links;
        }
    }
}
