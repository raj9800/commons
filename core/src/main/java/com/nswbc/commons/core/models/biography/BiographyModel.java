package com.nswbc.commons.core.models.biography;

import java.util.Objects;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.dam.api.Asset;

/**
 * @author yuvraj.bansal. Biography Model Class.
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/biography",
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class BiographyModel {

    /** Constant for dc:title. */
    private static final String DC_TITLE = "dc:title";

    /** Script Variable for value map. */
    @ScriptVariable
    private ValueMap properties;

    /** private sling object for resource resolver. */
    @SlingObject
    private ResourceResolver resourceResolver;

    /** private variable to show biotype in biography. */
    private String bioType;
    /** private variable to show authorname in biography. */
    private String authorName;
    /** private variable to show authortitle in biography. */
    private String authorTitle;
    /** private variable to show image path in biography. */
    private String fileReference;
    /** private variable to show author description in biography. */
    private String authorDescription;
    /** private variable to show title in biography. */
    private String title;
    /** private variable to show alt text for image. */
    private String imgAltText;

    /**
     * Init method for this model class.
     */
    @PostConstruct
    private void initModel() {
        this.bioType = this.properties.get("bioType", String.class);
        this.authorName = this.properties.get("authorName", String.class);
        this.authorTitle = this.properties.get("authorTitle", String.class);
        this.fileReference = this.properties.get("fileReference", String.class);
        this.authorDescription = this.properties.get("authorDescription", String.class);
        if (Objects.nonNull(fileReference)) {
            Resource resource = resourceResolver.getResource(fileReference);
            if (Objects.nonNull(resource)) {
                Asset asset = resource.adaptTo(Asset.class);
                this.title = asset.getMetadataValue(DC_TITLE);
                this.imgAltText = this.properties.get("imgAltText", this.title);
            }
        }
    }

    /**
     * @return bioType. This method returns the type of bio for author.
     */
    public String getBioType() {
        return bioType;
    }

    /**
     * @return aythorName. This method returns the name of author.
     */
    public String getAuthorName() {
        return authorName;
    }

    /**
     * @return authorTitle. This method returns the title of author.
     */
    public String getAuthorTitle() {
        return authorTitle;
    }

    /**
     * @return fileReference. This method returns the path of image asset.
     */
    public String getFileReference() {
        return fileReference;
    }

    /**
     * @return authorDescription. This method returns the description of author.
     */
    public String getAuthorDescription() {
        return authorDescription;
    }

    /**
     * @return title. This method returns the asset title for alt text.
     */
    public String getAssetMetaDataTitle() {
        return imgAltText;
    }

}
