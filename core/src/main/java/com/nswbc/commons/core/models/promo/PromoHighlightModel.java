package com.nswbc.commons.core.models.promo;

import java.util.Objects;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.day.cq.dam.api.Asset;

/**
 * @author yuvraj.bansal. PromoHighlightModel Class.
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/promohighlight",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class PromoHighlightModel {

    /** Constant for dc:title. */
    private static final String DC_TITLE = "dc:title";
    /** private sling object for resource resolver. */
    @Inject
    private ResourceResolver resourceResolver;
    /**
     * private inject variable for promoBgColor.
     */
    @Inject
    @Via("resource")
    @Default(values = "g-bg-color-snow")
    private String promoBgColor;

    /**
     * private inject variable for promoHeadingType.
     */
    @Inject
    @Via("resource")
    @Default(values = "image")
    private String promoHeadingType;
    /**
     * private inject variable for promoSubheadingType.
     */
    @Inject
    @Via("resource")
    @Default(values = "ptext")
    private String promoSubheadingType;

    /**
     * private inject variable for alignment.
     */
    @Inject
    @Via("resource")
    private String alignment;

    /**
     * private inject variable for image.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String fileReference;

    /**
     * private inject variable for imageType.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String imageType;

    /**
     * private inject variable for promoHeading.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String promoHeading;
    /**
     * private inject variable for richPromoSubHeading.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String richPromoSubHeading;

    /**
     * private inject variable for promoHeadingFontColor.
     */
    @Inject
    @Via("resource")
    @Default(values = "g-color-white")
    private String promoHeadingFontColor;

    /**
     * private inject variable for h1 heading tag.
     */
    @Inject
    @Via("resource")
    @Default(values = "false")
    private String useh1Tag;

    /**
     * private inject variable for promoSubHeading.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String promoSubHeading;

    /**
     * private inject variable for subHeadingFontColor.
     */
    @Inject
    @Via("resource")
    @Default(values = "jet")
    private String subHeadingFontColor;

    /**
     * private inject variable for subHeadingStrong.
     */
    @Inject
    @Via("resource")
    @Default(values = "false")
    private String subHeadingStrong;

    /**
     * private inject variable for includeBusiness.
     */
    @Inject
    @Via("resource")
    @Default(values = "false")
    private String includeBusiness;

    /**
     * private inject variable for businessFontColor.
     */
    @Inject
    @Via("resource")
    @Default(values = "black")
    private String businessFontColor;

    /**
     * private inject variable for imgAltText.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String logoAltText;
    /**
     * private inject variable for bottomRichText.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String bottomRichText;
    /**
     * private inject variable for bottomRichText.
     */
    @Inject
    @Via("resource")
    @Default(values = "jet")
    private String bottomRichTextColor;

    /**
     * private inject variable for logoReference.
     */
    @Inject
    @Via("resource")
    private String logoReference;

    /**
     * @return heading in h1.
     */
    public String getUseh1Tag() {
        return useh1Tag;
    }

    /**
     * @return subtitle heading in strong.
     */
    public String getSubHeadingStrong() {
        return subHeadingStrong;
    }

    /**
     * @return business text font color.
     */
    public String getBusinessFontColor() {
        return businessFontColor;
    }

    /**
     * @return promo heading font color.
     */
    public String getPromoHeadingFontColor() {
        return promoHeadingFontColor;
    }

    /**
     * @return sub title font color.
     */
    public String getSubHeadingFontColor() {
        return subHeadingFontColor;
    }

    /**
     * @return promo background color.
     */
    public String getPromoBgColor() {
        return promoBgColor;
    }

    /**
     * @return promo heading.
     */
    public String getPromoHeading() {
        return promoHeading;
    }

    /**
     * @return promo subtitle heading.
     */
    public String getPromoSubHeading() {
        return promoSubHeading;
    }

    /**
     * @return richPromoSubHeading rich promo subheading.
     */
    public String getRichPromoSubHeading() {
        return richPromoSubHeading;
    }

    /**
     * @return promoSubheadingType promoSubheadingType
     */
    public String getPromoSubheadingType() {
        return promoSubheadingType;
    }

    /**
     * @return include business text.
     */
    public String getIncludeBusiness() {
        return includeBusiness;
    }

    /**
     * @return include business text.
     */
    public String getPromoHeadingType() {
        return promoHeadingType;
    }

    /**
     * @return include business text.
     */
    public String getFileReference() {
        return fileReference;
    }

    /**
     * @return bottomRichText bottomRichText.
     */
    public String getBottomRichText() {
        return bottomRichText;
    }

    /**
     * @return bottomRichTextColor bottomRichTextColor.
     */
    public String getBottomRichTextColor() {
        return bottomRichTextColor;
    }

    /**
     * @return image Type
     */
    public String getImageType() {
        return imageType;
    }

    /**
     * @return logoReference
     */
    public String getLogoReference() {
        return logoReference;
    }

    /**
     * @return alignment
     */
    public String getAlignment() {
        return alignment;
    }

    /**
     * Getter method for image alt text.
     *
     * @return alt text given via author, if not given, returns title set in the dam
     *         for image
     */
    public String getLogoAltText() {
        String title = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(logoAltText)) {
            return logoAltText;
        } else {
            if (Objects.nonNull(fileReference)) {
                Resource resource = resourceResolver.getResource(fileReference);
                Asset asset = resource.adaptTo(Asset.class);
                title = asset.getMetadataValue(DC_TITLE);
            }
            return title;
        }
    }
}
