package com.nswbc.commons.core.models.page;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

/**
 * Page model. This model is used to inject conditional clientlibs on the page.
 *
 * @author rajat.pachouri
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/tagDisplay",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class PageModel {

    /** Script Variable for servlet request. */
    @Self
    private SlingHttpServletRequest request;

    /**
     * This method tells if the modernaward list comp is available on the page or
     * not.
     *
     * @return true is modern awards list comp is available on the page
     */
    public boolean isModernAwardsListAvailable() {
        if (null != request.getAttribute("injectModerAwardsListClientlib")) {
            return true;
        }
        return false;
    }

    /**
     * This method tells if the MyAccount comp is available on the page or not.
     *
     * @return true is tMyAccount comp is available on the page
     */
    public boolean isMyAccountAvailable() {
        if (null != request.getAttribute("injectMyAccountClientlib")) {
            return true;
        }
        return false;
    }
}
