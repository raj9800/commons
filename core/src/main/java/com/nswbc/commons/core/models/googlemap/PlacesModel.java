package com.nswbc.commons.core.models.googlemap;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

/**
 * Places model.
 *
 * @author rajat.pachouri
 *
 */
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class PlacesModel {

    /**
     * private inject variable for title.
     */
    @ValueMapValue
    @Default(values = "")
    private String title;

    /**
     * private inject variable for address.
     */
    @ValueMapValue
    @Default(values = "")
    private String address;

    /**
     * private inject variable for phone.
     */
    @ValueMapValue
    @Default(values = "")
    private String phone;

    /**
     * private inject variable for email.
     */
    @ValueMapValue
    @Default(values = "")
    private String email;

    /**
     * private inject variable for latitude.
     */
    @ValueMapValue
    @Default(values = "")
    private String latitude;

    /**
     * private inject variable for longitude.
     */
    @ValueMapValue
    @Default(values = "")
    private String longitude;

    /**
     * Getter for title.
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Getter for address.
     *
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Getter for phone.
     *
     * @return phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Getter for email.
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Getter for latitude.
     *
     * @return latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * Getter for longitude.
     *
     * @return longitude
     */
    public String getLongitude() {
        return longitude;
    }

}
