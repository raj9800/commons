package com.nswbc.commons.core.services.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.Cookie;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.commons.core.configuration.myaccount.MyAccountAPIConfigurationsInstance;
import com.nswbc.commons.core.configuration.webinarUserVerification.WebinarUserVerificationConfigurationsInstance;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.WebinarVerificationService;
import com.nswbc.commons.core.utils.ResourceUtils;
import com.nswbc.commons.core.utils.RestUtil;
import com.nswbc.commons.core.utils.ServerSideValidationUtil;

/**
 * UpdateModernAwardsListServiceImpl Class.
 *
 * @author riccardo.teruzzi
 */
@Component(immediate = true, service = WebinarVerificationService.class)
public class WebinarVerificationServiceImpl implements WebinarVerificationService {

    /**
     * Constant for Logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(WebinarVerificationServiceImpl.class);
    /**
     * Constant for status.
     */
    private static final String STATUS = "status";

    /**
     * Constant for Subscription key.
     */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    /**
     * Constant for Email Param.
     */
    private static final String PARAM_EMAIL = "Email";
    /**
     * Constant for EventCode Param.
     */
    private static final String PARAM_EVENTCODE = "EventCode";
    /** private constant for logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(WebinarVerificationServiceImpl.class);

    /** private reference variable for my account configuration. */
    @Reference
    private MyAccountAPIConfigurationsInstance myAccountConfig;

    /** Webinar user verification configuration object. */
    @Reference
    private WebinarUserVerificationConfigurationsInstance webinarUserVerificationConfiguration;
    /**
     * res type.
     */
    private static final String WEBINAR_VERIFICATION_RESOURCE_TYPE = "nswbc-commons/components/content/webinar-verification";

    /**
     * This method makes a post call to integration api and get the response as 200
     * or 400 status code.
     *
     * @return JsonObject of status.
     */
    @Override
    public JsonObject checkVerification(SlingHttpServletRequest request, SlingHttpServletResponse response,
            JsonObject bodyJson) {
        LOGGER.error(webinarUserVerificationConfiguration.getConfig().webinarUserVerificationAPI());
        if (!bodyJson.isJsonNull()) {
            if ((bodyJson.has(PARAM_EMAIL)
                    && ServerSideValidationUtil.checkEmail(bodyJson.get(PARAM_EMAIL).getAsString()))) {
                if (bodyJson.has(PARAM_EVENTCODE)
                        && StringUtils.isNotBlank(bodyJson.get(PARAM_EVENTCODE).getAsString())) {
                    Map<String, String> headerMap = setHeadersMap();
                    CloseableHttpClient client = HttpClientBuilder.create().build();
                    HttpResponse restResponse = RestUtil.processRestPostRequest(client,
                            webinarUserVerificationConfiguration.getConfig().webinarUserVerificationAPI(), headerMap,
                            bodyJson);
                    if (Objects.nonNull(restResponse)
                            && !(restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED)) {
                        if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED) {
                            // sending custom error message when there is an error from the backend.
                            try {

                                String responseEntity = EntityUtils.toString(restResponse.getEntity());
                                RestUtil.closeHttpClient(client);
                                JsonObject verificationApiResult = new JsonParser().parse(responseEntity)
                                        .getAsJsonObject();
                                if (verificationApiResult.has("errorCode") && verificationApiResult.has("message")) {
                                    response.setStatus(Constants.Number.FIVE_HUNDRED);
                                    return verificationApiResult;
                                }
                                RestUtil.closeHttpClient(client);
                            } catch (ParseException | IOException e) {
                                return RestUtil.getServerErrorResponse(response);
                            }
                            return RestUtil.getServerErrorResponse(response);
                        } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.TWO_HUNDRED) {
                            // when the status is 200
                            try {
                                String responseEntity = EntityUtils.toString(restResponse.getEntity());
                                RestUtil.closeHttpClient(client);
                                JsonObject verificationApiResult = new JsonParser().parse(responseEntity)
                                        .getAsJsonObject();
                                if (verificationApiResult.has("message") && !verificationApiResult.has("errorCode")) {
                                    verificationApiResult.addProperty(STATUS, Constants.Number.ONE);
                                }
                                response.setStatus(Constants.Number.TWO_HUNDRED);
                                // this cookie will be used to verify the user on the webinar page
                                Cookie loggedInCookie = new Cookie("webinar.user.loggedin", "true");
                                Cookie emailCookie = new Cookie("webinar.user.email",
                                        bodyJson.get(PARAM_EMAIL).getAsString());
                                loggedInCookie.setPath("/");
                                emailCookie.setPath("/");
                                response.addCookie(loggedInCookie);
                                response.addCookie(emailCookie);

                                return verificationApiResult;
                            } catch (ParseException | IOException e) {
                                return RestUtil.getServerErrorResponse(response);
                            }
                        }
                    }
                    return RestUtil.getBadRequestResponse(response);
                }
            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

    /**
     * @return header map in request.
     */
    private Map<String, String> setHeadersMap() {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(SUBSCRIPTION_KEY, myAccountConfig.getConfig().subscriptionKey());
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        return headerMap;
    }

    /**
     * This method fetches event code from the webinar verification page and verify
     * the user.
     */
    @Override
    public JsonObject getEventCodeAndcheckVerification(SlingHttpServletRequest request,
            SlingHttpServletResponse response, JsonObject bodyJson) {
        String verificationPagePath = bodyJson.get("redirecturi").getAsString();
        Resource comp = ResourceUtils.getResourceByResourceType(request.getResourceResolver(), verificationPagePath,
                WEBINAR_VERIFICATION_RESOURCE_TYPE);
        // read values
        if (null != comp) {
            ValueMap vm = comp.adaptTo(ValueMap.class);
            // return error if event is already finished.
            if (vm.containsKey("isEventFinished")) {
                return RestUtil.getServerErrorResponse(response);
            }
            String eventID = vm.containsKey("eventID") ? vm.get("eventID").toString() : StringUtils.EMPTY;
            bodyJson.addProperty(PARAM_EVENTCODE, eventID);
            return checkVerification(request, response, bodyJson);
        }
        return RestUtil.getServerErrorResponse(response);
    }
}
