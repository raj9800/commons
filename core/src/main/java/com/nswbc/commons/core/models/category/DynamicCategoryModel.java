package com.nswbc.commons.core.models.category;

import static com.nswbc.commons.core.models.carousel.ArticlePageModel.WEB_1280_1280;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.models.carousel.ArticlePageModel;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * DynamicCarouselModel Class.
 *
 * @author riccardo.teruzzi
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL,
        resourceType = "nswbc-commons/components/content/carousel")
public class DynamicCategoryModel {

    /**
     * private sling object for resource resolver.
     */
    @Inject
    private ResourceResolver resourceResolver;

    /**
     * private current page.
     */
    @Inject
    private Page page;

    /**
     * The constant ARTICLE_PAGE_TEMPLATE.
     */
    public static final String ARTICLE_PAGE = "article-page";

    /**
     * The Ordered article list.
     */
    private List<ArticlePageModel> orderedArticleList;

    /**
     * The File reference.
     */
    @Inject
    @Via("resource")
    private String fileReference;

    /**
     * The Img alt text.
     */
    @Inject
    @Via("resource")
    private String imgAltText;

    /**
     * The max tiles option.
     */
    @Inject
    @Via("resource")
    private int maxTiles;

    /**
     * Init.
     */
    @PostConstruct
    public void init() {
        String pageCategory = page.getProperties().get(Constants.Application.PAGE_CATEGORY, String.class);
        if (Objects.nonNull(pageCategory)) {
            PageFilter pageFilter = ResourceUtils.getPageFilterTemplatePatternAndProperty(ARTICLE_PAGE,
                    Constants.Application.PAGE_CATEGORY, pageCategory);
            Page homepage = page.getAbsoluteParent(Constants.Number.FOUR);
            orderedArticleList = new ArrayList<>();
            if (homepage != null) {
                Iterator<Page> pageIterator = homepage.listChildren(pageFilter, true);
                String currentPath = page.getPath();
                List<ArticlePageModel> pageList = new ArrayList<>();
                while (pageIterator.hasNext()) {
                    Page next = pageIterator.next();
                    if (!StringUtils.equals(currentPath, next.getPath())) {
                        Resource contentResource = next.getContentResource();
                        pageList.add(contentResource.adaptTo(ArticlePageModel.class));
                    }
                }
                orderedArticleList = pageList.stream().sorted(Comparator.comparing(ArticlePageModel::getLastModified))
                        .collect(Collectors.toList());
                Collections.reverse(orderedArticleList);
            }
        }
    }

    /**
     * Gets article pages.
     *
     * @return the article pages
     */
    public List<ArticlePageModel> getArticlePages() {
        if (Objects.nonNull(orderedArticleList) && orderedArticleList.size() != 0) {
            if (Objects.nonNull(maxTiles) && orderedArticleList.size() >= maxTiles && maxTiles > 0) {
                return orderedArticleList.subList(0, maxTiles);
            }
        }
        return orderedArticleList;
    }

    /**
     * Gets file reference.
     *
     * @return the file reference
     */
    public String getFileReference() {
        return ResourceUtils.getRenditionPath(resourceResolver, fileReference, WEB_1280_1280);
    }

    /**
     * Gets img alt text.
     *
     * @return the img alt text
     */
    public String getImgAltText() {
        return imgAltText;
    }

}
