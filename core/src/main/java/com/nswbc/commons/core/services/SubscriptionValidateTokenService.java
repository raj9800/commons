package com.nswbc.commons.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author yuvraj.bansal
 *
 */
public interface SubscriptionValidateTokenService {

    /**
     * @param request
     * @param response
     * @return validate token status
     */
    JsonObject validateToken(SlingHttpServletRequest request, SlingHttpServletResponse response);
}
