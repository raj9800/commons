package com.nswbc.commons.core.models.dtm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.json.JSONException;
import org.json.JSONObject;

import com.day.cq.wcm.api.Page;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * This class is used to generate dtm data from AEM.
 *
 * @author rajat.pachouri
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class GenerateDTMData {
    /** key for page property. */
    private static final String PAGE = "page";
    /** key for pageinfo property. */
    private static final String PAGE_INFO = "pageInfo";
    /** key for pagename property. */
    private static final String PAGE_NAME = "pageName";
    /** key for pageURL property. */
    private static final String PAGE_URL = "pageURL";
    /** key for brand property. */
    private static final String BRAND = "brand";
    /** key for previous page URL property. */
    private static final String PREVIOUS_PAGE_URL = "previousPageURL";
    /** key for devicetype property. */
    private static final String DEVICE_TYPE = "deviceType";
    /** key for category property. */
    private static final String CATEGORY = "category";
    /** key for primaryCategory property. */
    private static final String PRIMARY_CATEGORY = "primaryCategory";
    /** key for sectionLevelOne property. */
    private static final String SECTION_LEVEL_ONE = "sectionLevelOne";
    /** key for sectionLevelTwo property. */
    private static final String SECTION_LEVEL_TWO = "sectionLevelTwo";
    /** key for sectionLevelThree property. */
    private static final String SECTION_LEVEL_THREE = "sectionLevelThree";
    /** key for sectionLevelFour property. */
    private static final String SECTION_LEVEL_FOUR = "sectionLevelFour";
    /** key for sectionLevelFive property. */
    private static final String SECTION_LEVEL_FIVE = "sectionLevelFive";

    /** page json object. */
    private JSONObject pageJson = new JSONObject();
    /** pageinfo json object. */
    private JSONObject pageInfoJson = new JSONObject();
    /** category json object. */
    private JSONObject categoryJson = new JSONObject();
    /** injecting page category. */
    @Inject
    @Via("resource")
    @Default(values = StringUtils.EMPTY)
    private String pageCategory;
    /** brand property. */
    private String brand = StringUtils.EMPTY;
    /** sectionLevelOne property. */
    private String sectionLevelOne = StringUtils.EMPTY;
    /** sectionLevelTwo property. */
    private String sectionLevelTwo = StringUtils.EMPTY;
    /** sectionLevelThree property. */
    private String sectionLevelThree = StringUtils.EMPTY;
    /** sectionLevelFour property. */
    private String sectionLevelFour = StringUtils.EMPTY;
    /** sectionLevelFive property. */
    private String sectionLevelFive = StringUtils.EMPTY;
    /**
     * list of pages with hierarchy from root.
     */
    private List<Page> pageList;
    /**
     * Current page object.
     */
    @Inject
    private Page currentPage;

    /** private sling object for resource resolver. */
    @SlingObject
    private ResourceResolver resourceResolver;

    /**
     * this method will collect all the data and populate the json object.
     *
     * @throws JSONException the JSONOException object
     */
    @PostConstruct
    public void postConstruct() throws JSONException {
        pageList = generatePageList(currentPage);
        brand = ResourceUtils.populateBrand(currentPage);
        populateSectionVariables(pageList);
        pageInfoJson.put(PAGE_NAME, getPageName(pageList)).put(PAGE_URL, getPageURL())
        .put(PREVIOUS_PAGE_URL, StringUtils.EMPTY).put(DEVICE_TYPE, StringUtils.EMPTY).put(BRAND, brand);
        categoryJson.put(PRIMARY_CATEGORY, pageCategory).put(SECTION_LEVEL_ONE, sectionLevelOne)
        .put(SECTION_LEVEL_TWO, sectionLevelTwo).put(SECTION_LEVEL_THREE, sectionLevelThree)
        .put(SECTION_LEVEL_FOUR, sectionLevelFour).put(SECTION_LEVEL_FIVE, sectionLevelFive);
        pageJson.put(PAGE, (new JSONObject()).put(PAGE_INFO, pageInfoJson)).put(CATEGORY, categoryJson);
    }

    /**
     * This method populated section variables.
     *
     * @param pageList the pagelist.
     */
    public void populateSectionVariables(List<Page> pageList) {
        // to get the list from home
        if (pageList.size() > Constants.Number.THREE) { // this prevents it to run on templates page.
            List<Page> filteredList = pageList.subList(Constants.Number.THREE, pageList.size());
            Page page = null;
            if (filteredList.size() > Constants.Number.ZERO) {
                page = filteredList.get(Constants.Number.ZERO);
                sectionLevelOne = page.getName();
            }
            if (filteredList.size() > Constants.Number.ONE) {
                page = filteredList.get(Constants.Number.ONE);
                sectionLevelTwo = page.getName();
            }
            if (filteredList.size() > Constants.Number.TWO) {
                page = filteredList.get(Constants.Number.TWO);
                sectionLevelThree = page.getName();
            }
            if (filteredList.size() > Constants.Number.THREE) {
                page = filteredList.get(Constants.Number.THREE);
                sectionLevelFour = page.getName();
            }
            if (filteredList.size() == Constants.Number.FIVE) {
                page = filteredList.get(Constants.Number.FOUR);
                sectionLevelFive = page.getName();
            }
            if (filteredList.size() > Constants.Number.FIVE) {
                String pageName = StringUtils.EMPTY;
                for (int i = Constants.Number.FOUR; i < filteredList.size(); i++) {
                    page = filteredList.get(i);
                    pageName = (StringUtils.isEmpty(pageName) ? pageName : pageName + "|") + page.getName();

                }
                sectionLevelFive = pageName;
            }
        }
    }

    /**
     * getter for page name.
     *
     * @param pageList the page list
     * @return Concatenated string of page names seperated by a seperator.
     */
    public String getPageName(List<Page> pageList) {
        String pageName = StringUtils.EMPTY;
        for (Page page : pageList) {
            if (page.getDepth() > Constants.Number.TWO) {
                pageName = (StringUtils.isEmpty(pageName) ? pageName : pageName + "|") + page.getName();
            }
        }
        return pageName;
    }

    /**
     * To get page URL filtered via resource resolver.
     *
     * @return filtered page url
     */
    public String getPageURL() {
        String pageUrl = StringUtils.EMPTY;
        pageUrl = ResourceUtils.getResolvedPath(resourceResolver, currentPage.getPath());
        return pageUrl;
    }

    /**
     * returns the json data.
     *
     * @return json string data
     */
    public String getData() {
        return pageJson.toString().replace("\"", "'");
    }

    /**
     * method to generate list of pages which are a part of the hierarchy.
     *
     * @param page
     * @return list of all the parent pages.
     */
    public List<Page> generatePageList(Page page) {
        List<Page> pageList = new ArrayList<Page>();
        while (Objects.nonNull(page)) {
            pageList.add(page);
            page = page.getParent();
        }
        Collections.reverse(pageList);
        return pageList;
    }
}
