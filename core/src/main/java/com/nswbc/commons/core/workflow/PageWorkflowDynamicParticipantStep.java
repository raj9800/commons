package com.nswbc.commons.core.workflow;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.HistoryItem;
import com.adobe.granite.workflow.exec.ParticipantStepChooser;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.metadata.MetaDataMap;

/**
 * This step automatically chooses participant for dynamic participant step based on the value given in the previous workflow step.
 */
@Component(service = ParticipantStepChooser.class, property = {"chooser.label= Dynamic participant script for Business Australia"})
public class PageWorkflowDynamicParticipantStep implements ParticipantStepChooser {

    /**
     * LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PageWorkflowDynamicParticipantStep.class);

    /**
     * This method returns a user id of the user selected in the last dyanamic participant step.
     * @param workItem the workflow item
     * @param workflowSession the workflow session
     * @param args args metadatamap for the workflow step
     * @return user id of a single user
     * @throws WorkflowException the workflow exception
     */
    public String getParticipant(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap args)
            throws WorkflowException {
        List<HistoryItem> wfHistoryList = workflowSession.getHistory(workItem.getWorkflow());
        String user = StringUtils.EMPTY;
        Collections.reverse(wfHistoryList);
        for (HistoryItem hi : wfHistoryList) {
            if (hi.getWorkItem().getNode().getType().equals("DYNAMIC_PARTICIPANT")) {
                MetaDataMap map = hi.getWorkItem().getMetaDataMap();
                if (map.containsKey(getUserPropertyFromArgument(args))) {
                    user = map.get(getUserPropertyFromArgument(args), String.class);
                    LOGGER.debug(user);
                    break;
                }
            }
        }
        return user;
    }
    /**
     * Returns the property value to look for based on the args provided in workflow participant steps.
     * @param args
     * @return <tt>userProperty</tt> if found
     */
    public String getUserPropertyFromArgument(MetaDataMap args) {
        String arguments = args.get("PROCESS_ARGS", String.class);
        String userProperty = StringUtils.EMPTY;
        String arg = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(arguments)) {
            String[] argsArray = arguments.split(",");
            arg = argsArray[0];
            if (arg.equals("group2")) {
                userProperty = "user2";
            }
            if (arg.equals("group3")) {
                userProperty = "user3";
            }
        }
        return userProperty;
    }
}

