package com.nswbc.commons.core.models.carousel;

import java.util.Date;
import java.util.Objects;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

/**
 * ArticlePageModel Class.
 *
 * @author riccardo.teruzzi
 */

import com.adobe.acs.commons.genericlists.GenericList;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * The type Article page model.
 */
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ArticlePageModel {

    /**
     * The constant RENDITION_319_319.
     */
    public static final String WEB_1280_1280 = "cq5dam.web.1280.1280";

    /**
     * private current page object.
     */
    @Inject
    @Self
    private Resource resource;

    /**
     * private sling object for resource resolver.
     */
    @Inject
    private ResourceResolver resourceResolver;

    /**
     * private sling object for page manager.
     */
    @Inject
    private PageManager pageManager;

    /**
     * The page category.
     */
    @Inject
    private String pageCategory;

    /**
     * The cq:lastModified date.
     */
    @Inject
    @Named("cq:lastModified")
    private Date lastModified;

    /**
     * The jcr:title .
     */
    @Inject
    @Named("jcr:title")
    private String title;

    /**
     * Is article premium.
     */
    @Inject
    @Named("isPremium")
    private String isArticlePremium;

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * getter to check if an article is a premium article.
     *
     * @return isArticlePremium
     */
    public String getIsArticlePremium() {
        return isArticlePremium;
    }

    /**
     * Gets Page Category.
     *
     * @return the pageCategory
     */
    public String getPageCategory() {
        InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(resource);
        String brandName = ivm.getInherited("brandName", String.class);
        Page listPage = pageManager
                .getPage(Constants.Application.SEARCH_CATEGORIES_LIST_PATH + Constants.Application.HYPHEN + brandName);
        if (Objects.nonNull(listPage)) {
            GenericList list = listPage.adaptTo(GenericList.class);
            if (list != null) {
                return list.lookupTitle(pageCategory);
            }
        }
        return pageCategory;
    }

    /**
     * This method iterates through layout container and grabs the first image it
     * finds authored using image component. The assumption here is that doing so
     * would capture the banner image of an article page
     *
     * @return the banner image
     */
    public String getFileReference() {
        String fileReference = "";
        Resource root = null;
        Resource responsiveGrid = null;
        Iterable<Resource> components = null;
        root = resource.getChild("root");
        if (Objects.nonNull(root)) {
            responsiveGrid = root.getChild("responsivegrid");
        }
        if (Objects.nonNull(responsiveGrid)) {
            components = responsiveGrid.getChildren();
        }
        // iterate through components
        for (Resource res : components) {
            ValueMap map = res.getValueMap();
            // check if the component is layout container
            if (map.containsKey("sling:resourceType")
                    && map.get("sling:resourceType").equals("nswbc-commons/components/content/layoutcontainer")) {
                for (Resource childRes : res.getChildren()) {
                    ValueMap childMap = childRes.getValueMap();
                    // check if the component is image component
                    if (childMap.containsKey("sling:resourceType")
                            && childMap.get("sling:resourceType").equals("nswbc-commons/components/content/image")) {
                        // if the image component is authored
                        if (childMap.containsKey("fileReference")) {
                            fileReference = childMap.get("fileReference").toString();
                            break;
                        }
                    }
                }
            }
            if (StringUtils.isNotBlank(fileReference)) {
                break;
            }
        }
        return ResourceUtils.getRenditionPath(resourceResolver, fileReference, WEB_1280_1280);

    }

    /**
     * Gets last modified.
     *
     * @return the last modified
     */
    public Date getLastModified() {
        return lastModified;
    }

    /**
     * Gets link.
     *
     * @return the link
     */
    public String getLink() {
        return ResourceUtils.getResolvedPath(resourceResolver, pageManager.getContainingPage(resource).getPath());
    }

}
