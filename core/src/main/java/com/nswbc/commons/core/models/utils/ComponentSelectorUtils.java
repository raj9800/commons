package com.nswbc.commons.core.models.utils;

import java.util.LinkedList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.osgi.annotation.versioning.ConsumerType;

import com.adobe.cq.wcm.core.components.models.ListItem;
import com.day.cq.wcm.api.components.Component;
import com.day.cq.wcm.api.components.ComponentManager;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author yuvraj.bansal Model Util for component selector.
 */
@ConsumerType
public class ComponentSelectorUtils {

    /** private variable for sling http request. */
    @Self
    private SlingHttpServletRequest request;

    /** private variable for resource. */
    @SlingObject
    private Resource resource;

    /** private variable for list items. */
    private List<ListItem> items;
    /** private variable for child components. */
    private List<Resource> childComponents;

    /**
     * @return selected components items.
     */
    @JsonIgnore
    public List<ListItem> getItems() {
        if (this.items == null) {
            this.items = readItems();
        }
        return this.items;
    }

    /**
     * @return items.
     */
    private List<ListItem> readItems() {
        List<ListItem> items = new LinkedList<ListItem>();
        getChildren().forEach(res -> items.add(new ComponentSelectorResourceItems(this.request, res)));
        return items;
    }

    /**
     * @return children resources of the component.
     */
    private List<Resource> getChildren() {
        if (this.childComponents == null) {
            this.childComponents = readChildren();
        }
        return this.childComponents;
    }

    /**
     * @return children read.
     */
    private List<Resource> readChildren() {
        List<Resource> children = new LinkedList<Resource>();
        if (this.resource != null) {
            ComponentManager componentManager = this.request.getResourceResolver()
                    .adaptTo(ComponentManager.class);
            if (componentManager != null) {
                this.resource.getChildren().forEach(res -> {
                    Component component = componentManager.getComponentOfResource(res);
                    if (component != null && res.getName().startsWith("item")) {
                        children.add(res);
                    }
                });
            }
        }
        return children;
    }
}
