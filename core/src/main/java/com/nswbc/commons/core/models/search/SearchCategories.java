package com.nswbc.commons.core.models.search;

/**
 * @author yuvraj.bansal
 *
 */
public class SearchCategories {

    /** private String variable for categoryTitle. */
    private String categoryTitle;
    /** private String variable for categoryValue. */
    private String categoryValue;

    /**
     * @param categoryTitle
     * @param categoryValue
     */
    public SearchCategories(String categoryTitle, String categoryValue) {
        this.categoryTitle = categoryTitle;
        this.categoryValue = categoryValue;
    }

    /**
     * @return categoryTitle
     */
    public String getCategoryTitle() {
        return categoryTitle;
    }

    /**
     * @return categoryValue
     */
    public String getCategoryValue() {
        return categoryValue;
    }

}
