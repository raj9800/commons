package com.nswbc.commons.core.models.breadcrumb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.adobe.cq.wcm.core.components.models.Breadcrumb;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.designer.Style;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * The type Breadcrumb model.
 *
 * @author Yuvraj.Bansal. Breadcrumb Model Class
 */
@Model(adaptables = SlingHttpServletRequest.class,
        adapters = {Breadcrumb.class, com.adobe.cq.export.json.ComponentExporter.class},
        resourceType = "nswbc-commons/components/content/breadcrumb")
@Exporter(name = "jackson", extensions = {"json"})
public class BreadcrumbModel implements Breadcrumb {

    /** Script Variable for value map. */
    @ScriptVariable
    @Optional
    private ValueMap properties;
    /** Script Variable for current Style. */
    @ScriptVariable
    @Optional
    private Style currentStyle;
    /** Script Variable for current page. */
    @ScriptVariable
    @Optional
    private Page currentPage;
    /** Script Variable for servlet request. */
    @Self
    private SlingHttpServletRequest request;

    /** private variable to show hidden page in breadcrumb. */
    private boolean showHidden;
    /** private variable to hide current page in breadcrumb. */
    private boolean hideCurrent;
    /** private variable to show start level in breadcrumb. */
    private int startLevel;
    /** private variable for breadcrumb items. */
    private List<BreadcrumbNavigationItem> items;

    /** private sling object for resource resolver. */
    @SlingObject
    private ResourceResolver resourceResolver;

    /**
     * Init method for this model class.
     */
    @PostConstruct
    private void initModel() {
        if (Objects.nonNull(this.currentStyle)) {
            this.startLevel = this.properties.get("startLevel", this.currentStyle.get("startLevel", Integer.valueOf(2)))
                    .intValue();
            this.showHidden = this.properties
                    .get("showHidden", this.currentStyle.get("showHidden", Boolean.valueOf(false))).booleanValue();
            this.hideCurrent = this.properties
                    .get("hideCurrent", this.currentStyle.get("hideCurrent", Boolean.valueOf(false))).booleanValue();
        }
    }

    /**
     * Gets breadcrumb items.
     *
     * @return breadcrumb items This public method returns the breadcrumb item for
     *         current page.
     */
    public Collection<BreadcrumbNavigationItem> getBreadcrumbItems() {
        if (this.items == null) {
            this.items = createItems();
        }
        return this.items;
    }

    /**
     * @return array list of breadcrumb items. This private method return the array
     *         list of breadcrumb items.
     */
    private List<BreadcrumbNavigationItem> createItems() {
        items = new ArrayList<BreadcrumbNavigationItem>();
        int currentLevel = this.currentPage.getDepth();
        while (this.startLevel < currentLevel) {
            BreadcrumbNavigationItem breadCrumbItem = new BreadcrumbNavigationItem();
            Page page = this.currentPage.getAbsoluteParent(this.startLevel);
            ValueMap vm = page.getProperties();

            boolean removeLinkInBreadcrumb = vm.containsKey("removelinkinbreadcrumb");
            if (page != null) {
                boolean isActivePage = page.equals(this.currentPage);
                if (isActivePage && this.hideCurrent) {
                    break;
                }
                boolean checkIfNotHidden = (!page.isHideInNav() || this.showHidden);
                if (checkIfNotHidden) {
                    String vanityURL = page.getVanityUrl();
                    String resolvedPath = StringUtils.isEmpty(vanityURL)
                            ? (ResourceUtils.getResolvedPath(resourceResolver, page.getPath()))
                            : vanityURL;
                    if (removeLinkInBreadcrumb) {
                        breadCrumbItem.setUrl(StringUtils.EMPTY);
                        breadCrumbItem.setLinkRemoved(true);
                    } else {
                        breadCrumbItem.setUrl(resolvedPath);
                    }
                    breadCrumbItem.setTitle(Objects.nonNull(page.getNavigationTitle()) ? page.getNavigationTitle()
                            : Objects.nonNull(page.getTitle()) ? page.getTitle() : page.getName());
                    breadCrumbItem.setCurrentPage(isActivePage);
                    items.add(breadCrumbItem);
                }
            }
            this.startLevel++;
        }
        return items;
    }

    /**
     * Sets current page.
     *
     * @param currentPage the current page
     */
    public void setCurrentPage(Page currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * Sets show hidden.
     *
     * @param showHidden the show hidden
     */
    public void setShowHidden(boolean showHidden) {
        this.showHidden = showHidden;
    }

    /**
     * Sets hide current.
     *
     * @param hideCurrent the hide current
     */
    public void setHideCurrent(boolean hideCurrent) {
        this.hideCurrent = hideCurrent;
    }

    /**
     * Sets start level.
     *
     * @param startLevel the start level
     */
    public void setStartLevel(int startLevel) {
        this.startLevel = startLevel;
    }

}
