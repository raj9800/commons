package com.nswbc.commons.core.configuration.products;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;

/**
 * ModernAwardsConfigurationsInstance Class.
 *
 * @author riccardo.teruzzi
 */
@Component(immediate = true, service = ModernAwardsAPIConfigurationsInstance.class,
        configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = ModernAwardsAPIConfigurations.class)
public class ModernAwardsAPIConfigurationsInstance {

    /**
     * private configuration variable.
     */
    private ModernAwardsAPIConfigurations config;

    /**
     * @param config the osgi configuration
     */
    @Activate
    @Modified
    public void activate(ModernAwardsAPIConfigurations config) {
        this.config = config;
    }

    /**
     * Gets modern awards api configurations.
     *
     * @return the modern awards api configurations
     */
    public ModernAwardsAPIConfigurations getConfig() {
        return config;
    }
}
