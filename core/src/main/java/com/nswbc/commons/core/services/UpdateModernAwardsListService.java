package com.nswbc.commons.core.services;

import com.google.gson.JsonObject;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

/**
 * UpdateModernAwardsListService Class.
 *
 * @author riccardo.teruzzi
 */
public interface UpdateModernAwardsListService {
    /**
     * This method returns the update modern awards list.
     *
     * @param request  the request
     * @param response the response
     * @return the update modern awards list
     */
    JsonObject getUpdateModernAwardsList(SlingHttpServletRequest request, SlingHttpServletResponse response);
}
