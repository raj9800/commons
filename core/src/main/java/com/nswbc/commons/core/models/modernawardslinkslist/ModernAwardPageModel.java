package com.nswbc.commons.core.models.modernawardslinkslist;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.day.cq.wcm.api.Page;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * ModernAwardPageModel Class.
 *
 * @author rajat.pachouri
 */
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ModernAwardPageModel {

    /**
     * Resource resolver object.
     */
    @Inject
    private ResourceResolver resourceResolver;

    /**
     * page object.
     */
    @Inject
    @Self
    private Page selfPage;

    /**
     * Get page path string.
     *
     * @return the path
     */
    public String getPagePath() {
        return ResourceUtils.getResolvedPath(resourceResolver, selfPage.getPath());
    }

    /**
     * Get page title string.
     *
     * @return the title
     */
    public String getPageTitle() {
        return selfPage.getTitle();
    }
}
