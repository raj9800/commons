package com.nswbc.commons.core.models.iframe;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import javax.inject.Inject;

/**
 * IframeModel Class.
 *
 * @author riccardo.teruzzi
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/iframe",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class IframeModel {

    /**
     * The Height value.
     */
    @Inject
    @Via("resource")
    private String heightValue;

    /**
     * The Iframe url.
     */
    @Inject
    @Via("resource")
    private String iframeUrl;

    /**
     * The Width value.
     */
    @Inject
    @Via("resource")
    private String widthValue;

    /**
     * The Allow full screen.
     */
    @Inject
    @Via("resource")
    private Boolean allowFullScreen;

    /**
     * Gets height value.
     *
     * @return the height value
     */
    public String getHeightValue() {
        return heightValue;
    }

    /**
     * Gets iframe url.
     *
     * @return the iframe url
     */
    public String getIframeUrl() {
        return iframeUrl;
    }

    /**
     * Gets width value.
     *
     * @return the width value
     */
    public String getWidthValue() {
        return widthValue;
    }

    /**
     * Gets allow full screen.
     *
     * @return the allow full screen
     */
    public Boolean getAllowFullScreen() {
        return allowFullScreen;
    }
}
