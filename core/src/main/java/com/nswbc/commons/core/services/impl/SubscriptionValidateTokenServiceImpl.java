package com.nswbc.commons.core.services.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.JsonObject;
import com.nswbc.commons.core.configuration.emailsubscribe.SubscriptionContentAPIServiceConfigurationInstance;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.SubscriptionValidateTokenService;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = SubscriptionValidateTokenService.class)
public class SubscriptionValidateTokenServiceImpl implements SubscriptionValidateTokenService {
    /** Constant for Subscription key. */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    /** Constant for status. */
    private static final String STATUS = "status";
    /** Constant for Authorization. */
    private static final String AUTHORIZATION = "Authorization";
    /**
     * private reference for gated content configuration.
     */
    @Reference
    private SubscriptionContentAPIServiceConfigurationInstance gatedContentConfig;

    /**
     * return the result Json object. This method makes a post call to integration
     * api and get the response as token.
     */
    @Override
    public JsonObject validateToken(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        String token = request.getParameter("token");
        if (StringUtils.isNotEmpty(token)) {
            Map<String, String> headerMap = setHeadersMap(token);
            JsonObject resultJson = new JsonObject();
            CloseableHttpClient client = HttpClientBuilder.create().build();
            StringBuilder builder = new StringBuilder();
            HttpResponse restResponse = RestUtil.processRestGetRequest(client, gatedContentConfig.getConfig().tokenValidationAPI(), builder.append(
                    Constants.Application.EMPTY), headerMap);
            if (Objects.nonNull(restResponse)) {
                if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED) {
                    return RestUtil.getServerErrorResponse(response);
                } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.TWO_HUNDRED) {
                    resultJson.addProperty(STATUS, Constants.Number.ONE);
                    return resultJson;
                } else {
                    resultJson.addProperty(STATUS, Constants.Number.ZERO);
                    return resultJson;
                }
            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

    /**
     * @return header map in request.
     */
    private Map<String, String> setHeadersMap(String token) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        headerMap.put(SUBSCRIPTION_KEY, gatedContentConfig.getConfig().subscriptionKey());
        headerMap.put(AUTHORIZATION, "Bearer " + token);
        return headerMap;
    }
}
