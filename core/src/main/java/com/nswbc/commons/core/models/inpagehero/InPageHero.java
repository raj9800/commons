package com.nswbc.commons.core.models.inpagehero;

import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.nswbc.commons.core.models.image.ImageModel;

/**
 * @author rajat.pachouri. Image Model Class.
 */
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class InPageHero {

    /** private variable to show image in Inpagehero. */
    private ImageModel image;
    /** private current resource object. */
    @Inject
    private Resource resource;
    /** alignment of the image. */
    @Inject
    private String alignment;
    /** bgcolor of the component. */
    @Inject
    private String bgColor;
    /** bgcolor of the component. */
    @Inject
    private boolean isImageSquare;

    /**
     * Post construct method to inject image.
     */
    @PostConstruct
    public void init() {
        if (!Objects.isNull(resource)) {
            Resource image = resource.getChild("image");
            if (!Objects.isNull(image)) {
                this.image = image.adaptTo(ImageModel.class);
            }
        }
    }

    /**
     * Returns image.
     *
     * @return imageModel object
     */
    public ImageModel getImage() {
        return image;
    }

    /**
     * getter for image alignment.
     *
     * @return left,right or square.
     */
    public String getAlignment() {
        return alignment;
    }

    /**
     * to check if the image is square.
     *
     * @return true if image is square
     */
    public boolean getIsImageSquare() {
        return isImageSquare;
    }

    /**
     * getter for bg color for component.
     *
     * @return on of the brand colors.
     */
    public String getBgColor() {
        return bgColor;
    }
}
