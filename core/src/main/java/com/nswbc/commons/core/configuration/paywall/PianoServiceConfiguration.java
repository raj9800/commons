package com.nswbc.commons.core.configuration.paywall;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author rajat.pachouri Interface for configuration
 *
 */
@ObjectClassDefinition(name = "Client ID Service Configuration", description = "Service Configuration")
public @interface PianoServiceConfiguration {

    /**
     * @return scriptAPI
     */
    @AttributeDefinition(name = "Script API", description = "Provide the URL of the script for piano along with AID.")
    String scriptAPI();
}
