package com.nswbc.commons.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author yuvraj.bansal
 *
 */
public interface MyAccountDeactivateService {
    /**
     * @param request
     * @param response
     * @return deactivate user
     */
    JsonObject deactivateMember(SlingHttpServletRequest request, SlingHttpServletResponse response);
}
