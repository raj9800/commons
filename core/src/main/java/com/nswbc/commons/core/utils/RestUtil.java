package com.nswbc.commons.core.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.commons.core.constants.Constants;

/**
 * @author yuvraj.bansal
 *
 */
public final class RestUtil {

    /** Constant for Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(RestUtil.class);

    /**
     * default constructor.
     */
    private RestUtil() {
    }

    /**
     * @param client
     * @param api
     * @param headers
     * @param json
     * @return http response
     */
    public static HttpResponse processRestPostRequest(CloseableHttpClient client, String api, Map<String, String> headers, JsonObject json) {
        HttpResponse response = null;
        try {
            HttpPost post = new HttpPost(api);
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                post.setHeader(entry.getKey(), entry.getValue());
            }
            if (Objects.nonNull(json)) {
                StringEntity reqEntity = new StringEntity(json.toString());
                post.setEntity(reqEntity);
            }
            response = client.execute(post);
        } catch (IOException e) {
            LOG.error("Rest call failed : ", api + e.getMessage());
        }
        return response;
    }

    /**
     * @param client
     * @param api
     * @param headers
     * @param json
     * @return http response
     */
    public static HttpResponse processRestGetRequest(CloseableHttpClient client, String api, StringBuilder builder, Map<String, String> headers) {
        HttpResponse response = null;
        try {
            String queryParams = Constants.Application.EMPTY;
            if (StringUtils.isNotEmpty(builder)) {
                queryParams = builder.toString();
            }
            HttpGet get = new HttpGet(api + queryParams);
            if (Objects.nonNull(headers)) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    get.setHeader(entry.getKey(), entry.getValue());
                }
            }
            response = client.execute(get);
        } catch (IOException e) {
            LOG.error("Rest get call failed : ", api + e.getMessage());
        }
        return response;
    }

    /**
     * @param response
     * @return response json object This method set the response status to 400
     */
    public static JsonObject getBadRequestResponse(SlingHttpServletResponse response) {
        JsonObject jsonObject = new JsonObject();
        response.setStatus(Constants.Number.FOUR_HUNDRED);
        jsonObject.addProperty("status", "Bad Request");
        return jsonObject;
    }

    /**
     * @param response
     * @return response json object This method set the response status to 400
     */
    public static JsonObject getServerErrorResponse(SlingHttpServletResponse response) {
        JsonObject jsonObject = new JsonObject();
        response.setStatus(Constants.Number.FIVE_HUNDRED);
        jsonObject.addProperty("status", "Server Error");
        return jsonObject;
    }

    /**
     * @param request
     * @return the bodyJson
     */
    public static JsonObject getBodyJson(SlingHttpServletRequest request) {
        JsonObject bodyJson = new JsonObject();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    request.getInputStream()));
            String inputLine;
            StringBuffer requestBody = new StringBuffer();
            while ((inputLine = br.readLine()) != null) {
                requestBody.append(inputLine);
            }
            br.close();
            bodyJson = new JsonParser().parse(requestBody.toString()).getAsJsonObject();
        } catch (IOException e) {
            LOG.error("Exception in input stream ", e.getMessage());
        }
        return bodyJson;
    }

    /**
     * @param client This method closes the http client.
     */
    public static void closeHttpClient(CloseableHttpClient client) {
        try {
            client.close();
        } catch (IOException e) {
            LOG.error("Unable to close http client");
        }
    }

    /**
     * @param request
     * @return token from request header
     */
    public static String getTokenFromHeader(SlingHttpServletRequest request, String key) {
        String initialValue = request.getHeader(key);
        String processedValue = (Objects.nonNull(initialValue) && StringUtils.isNotEmpty(initialValue) ? initialValue : null);
        return processedValue;
    }

    /**
     * @param response
     * @return response json object This method set the response status to 401
     */
    public static JsonObject getUnAuthorizedResponse(SlingHttpServletResponse response) {
        JsonObject jsonObject = new JsonObject();
        response.setStatus(Constants.Number.FOUR_HUNDRED_ONE);
        jsonObject.addProperty("status", "UnAuthorized");
        return jsonObject;
    }

}
