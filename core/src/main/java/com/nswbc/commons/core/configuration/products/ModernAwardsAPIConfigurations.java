package com.nswbc.commons.core.configuration.products;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * ModernAwardsConfigurations Class.
 *
 * @author riccardo.teruzzi
 */
@ObjectClassDefinition(name = "Modern Awards List API Configurations", description = "Service Configuration")
public @interface ModernAwardsAPIConfigurations {

    /**
     * @return get modernAwardsListAPI
     */
    @AttributeDefinition(name = "Get Modern Awards List API", description = "Provide the api url for modern awards list.")
    String modernAwardsListAPI();

    /**
     * @return updateModernAwardsListAPI
     */
    @AttributeDefinition(name = "Update Modern Awards List API", description = "Provide the api url to update modern awards list.")
    String updateModernAwardsListAPI();

    /**
     * @return subscriptionkey
     */
    @AttributeDefinition(name = "Subscription Key", description = "Provide the subscription key.")
    String subscriptionKey();
}
