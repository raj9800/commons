package com.nswbc.commons.core.models.modernawardslist;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.google.gson.JsonObject;
import com.nswbc.commons.core.services.ProductCheckGuidService;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * ModernAwardsListModel Class.
 *
 * @author riccardo.teruzzi
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-ba/components/content/modernAwardsList",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class ModernAwardsListModel {

    /**
     * The constant PRODUCT_GUID.
     */
    public static final String PRODUCT_GUID = "productGuid";

    /**
     * The constant LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ModernAwardsListModel.class);

    /** Script Variable for servlet request. */
    @Self
    private SlingHttpServletRequest request;

    /**
     * private inject variable for my account page path.
     */
    @Inject
    @Via("resource")
    private String accountPagePath;

    /**
     * Resource resolver object.
     */
    @Inject
    private ResourceResolver resourceResolver;

    /**
     * private reference for modern awards configuration.
     */
    @Inject
    private ProductCheckGuidService productCheckGuidService;

    /**
     * Gets page list.
     *
     * @return the page list
     */
    public String getPageList() {
        Page securePage = productCheckGuidService.getSecurePage(resourceResolver);
        JsonObject result = productCheckGuidService.getAwardsList(securePage);
        return result.toString().replaceAll("'", "&#39;");
    }

    /**
     * Gets account page path.
     *
     * @return the account page path
     */
    public String getAccountPagePath() {
        return ResourceUtils.getResolvedPath(resourceResolver, accountPagePath);
    }

    /**
     * Post construct method.
     */
    @PostConstruct
    public void init() {
        // setting up request attribute to 'true'. This will be used to
        // add clientlibs conditionally on the page
        request.setAttribute("injectModerAwardsListClientlib", true);
    }
}
