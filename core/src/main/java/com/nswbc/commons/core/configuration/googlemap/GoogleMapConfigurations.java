package com.nswbc.commons.core.configuration.googlemap;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author rajat.pachouri Interface for configuration
 *
 */
@ObjectClassDefinition(name = "Google map config", description = "Google map config")
public @interface GoogleMapConfigurations {
    /**
     * @return googleMapApiKey
     */
    @AttributeDefinition(name = "Google map api key", description = "Provide the api key for google map.")
    String googleMapApiKey();

}
