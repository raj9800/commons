package com.nswbc.commons.core.models.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.adobe.acs.commons.genericlists.GenericList;
import com.adobe.cq.wcm.core.components.models.Search;
import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * @author yuvraj.bansal SearchModel class.
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/search",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class SearchModel {

    /** private sling object for resource resolver. */
    @SlingObject
    private ResourceResolver resourceResolver;

    /** Script Variable for servlet request. */
    @Self
    private SlingHttpServletRequest request;

    /** Script Variable for current page. */
    @ScriptVariable
    @Optional
    private Page currentPage;

    /**
     * private inject variable to display search heading text.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String headingText;

    /**
     * private inject variable to determine searchType.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String searchType;

    /**
     * private inject variable to display search categories list.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String[] searchCategories;

    /** private List Variable for searchCategoryList. */
    private List<SearchCategories> searchCategoryList;
    /** private int Variable for min character for search result. */
    private int minChar;
    /** private int Variable for result per page. */
    private int resultSize;
    /** private String Variable for searchRootPath. */
    private String searchRoot;

    /**
     * init model method.
     */
    @PostConstruct
    private void init() {
        if (Objects.nonNull(searchCategories)) {
            searchCategoryList = new ArrayList<SearchCategories>();
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            Page page = pageManager.getContainingPage(request.getResource());
            InheritanceValueMap ivm = new HierarchyNodeInheritanceValueMap(page.getContentResource());
            String brandName = ivm.getInherited("brandName", String.class);
            Page listPage = pageManager.getPage(
                    Constants.Application.SEARCH_CATEGORIES_LIST_PATH + Constants.Application.HYPHEN + brandName);
            if (Objects.nonNull(listPage)) {
                GenericList list = listPage.adaptTo(GenericList.class);
                for (String category : searchCategories) {
                    SearchCategories objSearch = new SearchCategories(list.lookupTitle(category), category);
                    searchCategoryList.add(objSearch);
                }
            }
            ValueMap searchContentPolicyMap = ResourceUtils.getPolicyDetails(request,
                    Constants.Application.SEARCH_RESOURCE_TYPE);
            minChar = searchContentPolicyMap.get(Search.PN_SEARCH_TERM_MINIMUM_LENGTH, Constants.Number.THREE);
            resultSize = searchContentPolicyMap.get(Search.PN_RESULTS_SIZE, Constants.Number.TEN);
            searchRoot = searchContentPolicyMap.get(Search.PN_SEARCH_ROOT, Constants.Application.NSWBC_CONTENT_ROOT);
        }
    }

    /**
     * @return headingText This method is use to get the search heading title.
     */
    public String getHeadingText() {
        return headingText;
    }

    /**
     * @return searchType
     */
    public String getSearchType() {
        return searchType;
    }

    /**
     * @return searchCategories This method is use to get the searchCategory list.
     */
    public List<SearchCategories> getSearchCategories() {
        return searchCategoryList;
    }

    /**
     * @return current Page Path.
     */
    public String getCurrentPagePath() {
        return ResourceUtils.getURL(resourceResolver, currentPage.getPath() + Constants.Extensions.HTML_EXTENSION);
    }

    /**
     * @return minChar
     */
    public int getMinChar() {
        return minChar;
    }

    /**
     * @return resultSize
     */
    public int getResultSize() {
        return resultSize;
    }

    /**
     * @return searchRoot
     */
    public String getSearchRoot() {
        return searchRoot;
    }

}
