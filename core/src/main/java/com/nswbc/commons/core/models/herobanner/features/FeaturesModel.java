package com.nswbc.commons.core.models.herobanner.features;

import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.nswbc.commons.core.constants.Constants;

/**
 * Benefits component sling model.
 *
 * @author rajat.pachouri
 */
@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class FeaturesModel {

    /**
     * title.
     */
    @Inject
    @Via("resource")
    private String title;
    /**
     * color of the title.
     */
    @Inject
    @Via("resource")
    private String fontColor;

    /**
     * getter for title.
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * features.
     */
    @Inject
    @Via("resource")
    private List<FeaturesColumnModel> features;

    /**
     * features.
     */
    @Inject
    private List<FeaturesColumnModel> featuresOne;

    /**
     * features.
     */
    @Inject
    private List<FeaturesColumnModel> featuresTwo;

    /**
     * Getter for features.
     *
     * @return features
     */
    public List<FeaturesColumnModel> getFeaturesOne() {
        return featuresOne;
    }

    /**
     * Getter for features.
     *
     * @return features
     */
    public List<FeaturesColumnModel> getFeaturesTwo() {
        return featuresTwo;
    }

    /** private current resource object. */
    @Inject
    private Resource resource;

    /**
     * Init method.
     */
    @PostConstruct
    public void init() {
        if (StringUtils.isNotBlank(resource.getPath())) {
            if (Objects.nonNull(features) && features.size() > Constants.Number.ZERO) {
                if (features.size() == Constants.Number.ONE) {
                    featuresOne = features;
                } else if (features.size() > Constants.Number.ONE) {
                    featuresOne = features.subList(Constants.Number.ZERO, (features.size() / Constants.Number.TWO));
                    featuresTwo = features.subList((features.size()) / Constants.Number.TWO, features.size());
                }
            }
        }
    }

    /**
     * get font color.
     *
     * @return fontColor
     */
    public String getFontColor() {
        return fontColor;
    }
}
