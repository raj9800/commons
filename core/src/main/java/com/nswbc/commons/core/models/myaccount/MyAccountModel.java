package com.nswbc.commons.core.models.myaccount;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;

import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * My Account Model Class.
 *
 * @author rajat.pachouri.
 */
@Model(adaptables = {SlingHttpServletRequest.class}, resourceType = "nswbc-commons/components/content/myaccount",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class MyAccountModel {

    /** Script Variable for servlet request. */
    @Self
    private SlingHttpServletRequest request;
    /**
     * Resource resolver object.
     */
    @Inject
    private ResourceResolver resourceResolver;

    /**
     * eventUrl.
     */
    @Inject
    @Via("resource")
    private String eventUrl;

    /**
     * deactivationUrl.
     */
    @Inject
    @Via("resource")
    private String deactivationUrl;

    /**
     * modernListUrl.
     */
    @Inject
    @Via("resource")
    private String modernListUrl;

    /**
     * memberResourceUrl.
     */
    @Inject
    @Via("resource")
    private String memberResourceUrl;

    /**
     * paywallSubscriptionPageUrl.
     */
    @Inject
    @Via("resource")
    private String paywallSubscriptionPageUrl;

    /**
     * getter for eventurl.
     *
     * @return eventUrl
     */
    public String getEventUrl() {
        return ResourceUtils.getResolvedPath(resourceResolver, eventUrl);
    }

    /**
     * getter for deactivationUrl.
     *
     * @return deactivationUrl
     */
    public String getDeactivationUrl() {
        return ResourceUtils.getResolvedPath(resourceResolver, deactivationUrl);
    }

    /**
     * getter for modernListUrl.
     *
     * @return modernListUrl
     */
    public String getModernListUrl() {
        return ResourceUtils.getResolvedPath(resourceResolver, modernListUrl);
    }

    /**
     * getter for memberResource url.
     *
     * @return eventUrl
     */
    public String getMemberResourceUrl() {
        return ResourceUtils.getResolvedPath(resourceResolver, memberResourceUrl);
    }

    /**
     * getter for paywallSubscriptionPageUrl.
     *
     * @return paywallSubscriptionPageUrl
     */
    public String getPaywallSubscriptionPageUrl() {
        return ResourceUtils.getResolvedPath(resourceResolver, paywallSubscriptionPageUrl);
    }

    /**
     * Post construct method.
     */
    @PostConstruct
    public void init() {
        // setting up request attribute to 'true'. This will be used to
        // add clientlibs conditionally on the page
        request.setAttribute("injectMyAccountClientlib", true);
    }
}
