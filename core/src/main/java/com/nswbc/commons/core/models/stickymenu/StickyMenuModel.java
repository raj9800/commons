package com.nswbc.commons.core.models.stickymenu;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import com.nswbc.commons.core.models.utils.ComponentSelectorUtils;

/**
 * @author yuvraj.bansal. Model for Sticky Menu.
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/stickymenu",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class StickyMenuModel extends ComponentSelectorUtils {

}
