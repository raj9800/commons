package com.nswbc.commons.core.models.podcast;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

/**
 * @author yuvraj.bansal
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/podcast-player",
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class PodcastPlayerModel {

    /**
     * private inject variable to iframe url.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String podcastUrl;

    /**
     * private inject variable for podcast height.
     */
    @Inject
    @Via("resource")
    @Default(values = "90")
    private String podcastHeight;

    /**
     * @return the podcastUrl
     */
    public String getPodcastUrl() {
        return podcastUrl;
    }

    /**
     * @return the podcastHeight
     */
    public String getPodcastHeight() {
        return podcastHeight;
    }


}
