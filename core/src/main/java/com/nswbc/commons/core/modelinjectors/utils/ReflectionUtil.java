/*
 * #%L
 * ACS AEM Commons Bundle
 * %%
 * Copyright (C) 2018 Adobe
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package com.nswbc.commons.core.modelinjectors.utils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

/**
 * Contains reflection utility methods.
 */
public final class ReflectionUtil {

    /**
     * Constructor.
     */
    private ReflectionUtil() {
        // static methods only
    }

    /**
     * method to check if the given type is array type.
     *
     * @param declaredType declaredType
     * @return true is given type is array type
     */
    public static boolean isArray(Type declaredType) {
        if (declaredType instanceof Class<?>) {
            Class<?> clazz = (Class<?>) declaredType;
            return isArray(clazz);
        }
        return false;
    }

    /**
     * method to check if the given class is array.
     *
     * @param clazz given class type
     * @return true is given class type is array
     */
    public static boolean isArray(Class<?> clazz) {
        return clazz.isArray();
    }

    /**
     * method to check if the given type is collection type.
     *
     * @param declaredType declaredType
     * @return true is given type is collection type
     */
    public static boolean isCollectionType(Type declaredType) {
        if (declaredType instanceof Class<?>) {
            Class<?> clazz = (Class<?>) declaredType;
            return isCollectionType(clazz);
        } else {
            ParameterizedType parameterizedType = (ParameterizedType) declaredType;
            return isCollectionType(parameterizedType.getRawType());
        }
    }

    /**
     * method to check if the given class is collection type.
     *
     * @param collectionType given collection type
     * @return true is given class type is collection
     */
    public static boolean isCollectionType(Class<?> collectionType) {
        return collectionType.equals(Collection.class);
    }

    /**
     * method to check if the given type is list type.
     *
     * @param declaredType given class type
     * @return true is given type is collection type
     */
    public static boolean isListType(Type declaredType) {
        if (declaredType instanceof Class<?>) {
            Class<?> clazz = (Class<?>) declaredType;
            return isListType(clazz);
        } else {
            ParameterizedType parameterizedType = (ParameterizedType) declaredType;
            return isListType(parameterizedType.getRawType());
        }
    }

    /**
     * method to check if the given class is Collection type.
     *
     * @param collectionType
     * @return true is class is collection type.
     */
    public static boolean isListType(Class<?> collectionType) {
        return collectionType.equals(List.class);
    }
}
