package com.nswbc.commons.core.configuration.emailsubscribe;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = SubscriptionContentAPIServiceConfigurationInstance.class, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = SubscriptionAPIServiceConfiguration.class)
public class SubscriptionContentAPIServiceConfigurationInstance {
    /** private configuration variable. */
    private SubscriptionAPIServiceConfiguration config;

    /**
     * @param config
     */
    @Activate
    public void activate(SubscriptionAPIServiceConfiguration config) {
        this.config = config;
    }

    /**
     * @return the config
     */
    public SubscriptionAPIServiceConfiguration getConfig() {
        return config;
    }

}
