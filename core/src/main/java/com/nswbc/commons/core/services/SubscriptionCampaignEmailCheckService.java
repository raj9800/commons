package com.nswbc.commons.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author yuvraj.bansal
 *
 */
public interface SubscriptionCampaignEmailCheckService {

    /**
     * @param request
     * @param response
     * @return status as email check
     */
    JsonObject verifyEmail(SlingHttpServletRequest request, SlingHttpServletResponse response);
}
