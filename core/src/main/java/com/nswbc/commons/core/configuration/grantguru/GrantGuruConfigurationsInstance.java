package com.nswbc.commons.core.configuration.grantguru;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;

import com.nswbc.commons.core.configuration.myaccount.MyAccountAPIConfigurations;

/**
 * @author rajat.pachouri
 *
 */
@Component(immediate = true, service = GrantGuruConfigurationsInstance.class,
        configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = MyAccountAPIConfigurations.class)
public class GrantGuruConfigurationsInstance {
    /** private configuration variable. */
    private GrantGuruConfigurations config;

    /**
     * @param config
     */
    @Activate
    public void activate(GrantGuruConfigurations config) {
        this.config = config;
    }

    /**
     * @return the config
     */
    public GrantGuruConfigurations getConfig() {
        return config;
    }
}
