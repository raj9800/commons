package com.nswbc.commons.core.models.socialshare;

import java.util.Objects;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.wcm.api.Page;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.models.utils.SearchResourceItems;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * @author yuvraj.bansal
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/socialshare",
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class SocialShareModel {

    /**
     * private inject variable for socialShareItems.
     */
    @Inject
    @Via("resource")
    private String[] socialShareItems;

    /**
     * private self variable for request.
     */
    @Self
    private SlingHttpServletRequest request;

    /**
     * private script variable for currentPage.
     */
    @ScriptVariable
    @Optional
    private Page currentPage;

    /**
     * private sling object of resource resolver.
     */
    @SlingObject
    private ResourceResolver resourceResolver;

    /**
     * @return URL of the current page.
     */
    public String getCurrentPageURL() {
        return ResourceUtils.getURL(resourceResolver, request.getRequestURI()) + (Objects.nonNull(request.getQueryString())
                ? (Constants.Application.QUESTION_MARK + request
                        .getQueryString()) : "");
    }

    /**
     * @return social share items.
     */
    public String[] getSocialShareItems() {
        return socialShareItems;
    }

    /**
     * @return current page title.
     */
    public String getPageTitle() {
        return new SearchResourceItems(request, currentPage).getTitle();
    }
}
