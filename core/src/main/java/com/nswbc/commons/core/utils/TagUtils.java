package com.nswbc.commons.core.utils;

import com.day.cq.tagging.Tag;
import com.day.cq.wcm.api.Page;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * TagUtils Class.
 *
 * @author sandeep.rawat
 */
public final class TagUtils {

    /**
     * default constructor.
     */
    private TagUtils() {
    }

    /**
     * The constant TAGNAMESPACE.
     */
    private static final String TAGNAMESPACE = "nswbc";

    /**
     * Is nswbc namespace boolean.
     *
     * @param tag the tag.
     * @return the boolean.
     */
    public static boolean isNswbcNamespace(final Tag tag) {
        return tag != null && tag.getNamespace() != null && StringUtils.equalsIgnoreCase(tag.getNamespace().getName(), TAGNAMESPACE);
    }

    /**
     * Get page tags filter by namespace tag [ ].
     *
     * @param page the page.
     * @return the tag [ ].
     */
    public static Tag[] getPageTagsFilterByNamespace(final Page page) {
        if (page == null) {
            return null;
        }
        Tag[] tags = page.getTags();
        if (tags == null) {
            return null;
        }
        List<Tag> filteredTags = new ArrayList<Tag>();
        for (int i = 0; i < tags.length; i++) {
            if (isNswbcNamespace(tags[i])) {
                filteredTags.add(tags[i]);
            }
        }
        if (filteredTags.size() > 0) {
            tags = new Tag[filteredTags.size()];
            filteredTags.toArray(tags);
            return tags;
        } else {
            return null;
        }
    }

}
