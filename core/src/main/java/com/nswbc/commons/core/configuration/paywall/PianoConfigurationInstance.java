package com.nswbc.commons.core.configuration.paywall;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;

/**
 * Piano configuration.
 *
 * @author rajat.pachouri
 *
 */
@Component(immediate = true, service = PianoConfigurationInstance.class,
        configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = PianoServiceConfiguration.class)
public class PianoConfigurationInstance {
    /** private configuration variable. */
    private PianoServiceConfiguration config;

    /**
     * @param config
     */
    @Activate
    public void activate(PianoServiceConfiguration config) {
        this.config = config;
    }

    /**
     * @return the config
     */
    public PianoServiceConfiguration getConfig() {
        return config;
    }

}
