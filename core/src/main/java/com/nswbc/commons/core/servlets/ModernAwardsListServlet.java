package com.nswbc.commons.core.servlets;

import static com.nswbc.commons.core.servlets.ModernAwardsListServlet.SERVLET_PATH;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Servlet;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.JsonObject;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.ModernAwardsListService;
import com.nswbc.commons.core.services.UpdateModernAwardsListService;

/**
 * ModernAwardsServlet Class.
 *
 * @author riccardo.teruzzi
 */
@Component(service = Servlet.class,
        property = {"sling.servlet.selectors=" + ModernAwardsListServlet.AWARDS_LIST_SELECTOR,
                "sling.servlet.selectors=" + ModernAwardsListServlet.UPDATE_AWARDS_LIST_SELECTOR,
                "sling.servlet.paths=" + SERVLET_PATH, "sling.servlet.extensions=" + Constants.Text.JSON,
                "sling.servlet.methods=GET", "sling.servlet.methods=POST"})
public class ModernAwardsListServlet extends SlingAllMethodsServlet {

    /**
     * serial version UID.
     */
    private static final long serialVersionUID = 3275654229359712945L;

    /**
     * protected variable for servlet path.
     */
    protected static final String SERVLET_PATH = "/bin/nswbc/modernawards";

    /**
     * protected selector variable to list modern awards pages.
     */
    protected static final String AWARDS_LIST_SELECTOR = "awardslist";

    /**
     * protected selector variable to update account details.
     */
    protected static final String UPDATE_AWARDS_LIST_SELECTOR = "updateawardslist";

    /**
     * private reference variable for modern awards list service.
     */
    @Reference
    private ModernAwardsListService modernAwardsListService;

    /**
     * private reference variable for update modern awards list service.
     */
    @Reference
    private UpdateModernAwardsListService updateModernAwardsListService;

    /**
     * doGetMethod to call various services.
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        String selector = request.getRequestPathInfo().getSelectorString();
        if (StringUtils.equals(selector, AWARDS_LIST_SELECTOR)) {
            JsonObject result = modernAwardsListService.getModernAwardsList(request, response);
            printResponseResult(response, result);
        }
    }

    /**
     * doPost method for update modern awards list.
     */
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        String selector = request.getRequestPathInfo().getSelectorString();
        if (StringUtils.equals(selector, UPDATE_AWARDS_LIST_SELECTOR)) {
            JsonObject result = updateModernAwardsListService.getUpdateModernAwardsList(request, response);
            printResponseResult(response, result);
        }
    }

    /**
     * @param response
     * @param result
     * @throws IOException This method checks the output result
     */
    private void printResponseResult(SlingHttpServletResponse response, JsonObject result) throws IOException {
        if (Objects.nonNull(result)) {
            response.getWriter().print(result);
        }
    }

}
