package com.nswbc.commons.core.servlets;

import java.io.IOException;
import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.servlet.Servlet;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.inherit.HierarchyNodeInheritanceValueMap;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.UsersService;
import com.nswbc.commons.core.utils.ResourceUtils;
import com.nswbc.commons.core.workflow.configuration.BrandGroupMappingServiceInstance;

//@SlingServlet(paths = "/bin/aussie/allpages", methods = "GET", metatype = true)
/**
 * This servlet uses a service and returns a list of users from the group passed
 * as parameter.
 *
 * @author rajat.pachouri
 *
 */
@Component(service = Servlet.class,
        property = {"sling.servlet.methods=" + HttpConstants.METHOD_GET, "sling.servlet.paths=" + "/bin/nswbc/users"})
public class GetUsersList extends SlingSafeMethodsServlet {
    /**
     * collecting all the brandgroup mapping configurations.
     */
    @Reference(service = BrandGroupMappingServiceInstance.class, cardinality = ReferenceCardinality.MULTIPLE,
            policy = ReferencePolicy.DYNAMIC)
    private final List<BrandGroupMappingServiceInstance> brandGroupMappingList = new ArrayList<BrandGroupMappingServiceInstance>();

    private static final long serialVersionUID = 1L;

    /**
     * LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(GetUsersList.class);

    /**
     * type of the group to the get the users from.It can be 'approver' or
     * 'publisher'.
     */
    private static final String GROUP_TYPE = "gt";
    /**
     * path of the payload.
     */
    private static final String PAYLOAD_PATH = "p";
    /**
     * approver group type string.
     */
    private static final String GROUP_TYPE_APPROVER = "approver";
    /**
     * publisher group type string.
     */
    private static final String GROUP_TYPE_PUBLISHER = "publisher";

    /**
     * UsersService which allows the servlet to fetch the list of users from a
     * particular group.
     */
    @Reference
    private UsersService usersService;

    /**
     * THe resource resolver factory.
     */
    @Reference
    private ResourceResolverFactory resourceFactory;

    /**
     * Default method which is executed when the servlet is called.
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServerException, IOException {
        String brandId = StringUtils.EMPTY;
        String groupName = StringUtils.EMPTY;
        String groupType = request.getParameter(GROUP_TYPE);
        String payloadPath = request.getParameter(PAYLOAD_PATH);

        // checking that both params are present
        if (StringUtils.isNotBlank(groupType) && StringUtils.isNotBlank(payloadPath)) {
            brandId = getBrandID(payloadPath);
            if (StringUtils.isNotBlank(payloadPath)) {
                for (BrandGroupMappingServiceInstance config : brandGroupMappingList) {
                    if (brandId.equalsIgnoreCase(config.getConfig().brandID())) {
                        if (groupType.equalsIgnoreCase(GROUP_TYPE_APPROVER)) {
                            groupName = config.getConfig().approverGroup();
                        } else if (groupType.equalsIgnoreCase(GROUP_TYPE_PUBLISHER)) {
                            groupName = config.getConfig().publisherGroup();
                        }
                        break;
                    }
                }
            }
            if (StringUtils.isNotBlank(groupName)) {
                String usersList = usersService.getUsers(groupName);
                response.getWriter().write(usersList);
                response.setContentType(Constants.Application.APPLICATION_JSON);
                response.getWriter().flush();
            }
        }
    }

    /**
     * This is to get brand name for a given path.
     *
     * @param payloadPath payload path
     * @return brandId
     */
    private String getBrandID(String payloadPath) {
        String brandId = StringUtils.EMPTY;
        Resource pageResource = ResourceUtils.getResource(resourceFactory, payloadPath);
        if (Objects.nonNull(pageResource)) {
            InheritanceValueMap inheritanceValueMap = new HierarchyNodeInheritanceValueMap(pageResource);
            brandId = inheritanceValueMap.getInherited("brandName", StringUtils.EMPTY);
        }
        return brandId;
    }
}
