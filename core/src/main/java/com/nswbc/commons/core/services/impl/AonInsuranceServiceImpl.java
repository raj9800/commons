package com.nswbc.commons.core.services.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.nswbc.commons.core.configuration.aoninsurance.AonInsuranceConfigurationsInstance;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.AonInsuranceService;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * AonInsurance Service Impl. author ranjeet.singh
 */
@Component(service = AonInsuranceService.class, immediate = true)
public class AonInsuranceServiceImpl implements AonInsuranceService {

    /**
     * Logger for the class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AonInsuranceServiceImpl.class);

    /**
     * Constant for Subscription Key.
     */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";

    /**
     * Constant for Authorization.
     */
    private static final String AUTHORIZATION = "Authorization";

    /**
     * Reference object of the AonInsuranceConfigurationsInstance.
     */
    @Reference
    private AonInsuranceConfigurationsInstance aonInsConfig;

    /**
     * This method provides the AonInsurance URL where the customer will be
     * redirected after CRM successfully registers the aon insurance interest of the
     * logged in customer.
     *
     * @param request
     * @param response
     * @return JsonObject
     */
    @Override
    public JsonObject getAonInsuranceURI(SlingHttpServletRequest request, SlingHttpServletResponse response) {

        String token = RestUtil.getTokenFromHeader(request, "token");
        String email = request.getParameter("email");
        if (StringUtils.isNotBlank(token) && StringUtils.isNotBlank(email)) {
            Map<String, String> headerMap = setHeadersMap(token);
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("email", email);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse restResponse = RestUtil.processRestPostRequest(client,
                    aonInsConfig.getConfig().aonInsuranceAPI(), headerMap, jsonObject);
            if (Objects.nonNull(restResponse)) {
                if (restResponse.getStatusLine().getStatusCode() == Constants.Number.TWO_HUNDRED) {
                    RestUtil.closeHttpClient(client);
                    JsonObject resultJson = new JsonObject();
                    resultJson.addProperty("url", aonInsConfig.getConfig().aonInsuranceRedirectURL());
                    return resultJson;
                } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED_ONE) {
                    RestUtil.getUnAuthorizedResponse(response);
                } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED) {
                    RestUtil.getServerErrorResponse(response);
                }
            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

    /**
     * This method return the header map.
     *
     * @param token
     * @return headerMap.
     */
    private Map<String, String> setHeadersMap(String token) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        headerMap.put(SUBSCRIPTION_KEY, aonInsConfig.getConfig().subscriptionKey());
        headerMap.put(AUTHORIZATION, "Bearer " + token);
        return headerMap;
    }
}
