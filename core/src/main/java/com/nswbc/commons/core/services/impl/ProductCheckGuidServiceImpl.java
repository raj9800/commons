package com.nswbc.commons.core.services.impl;

import static com.nswbc.commons.core.constants.Constants.Application.EMPTY;
import static com.nswbc.commons.core.models.modernawardslist.ModernAwardsListModel.PRODUCT_GUID;
import static com.nswbc.commons.core.utils.ResourceUtils.getListOfPageFromIterator;
import static com.nswbc.commons.core.utils.ResourceUtils.getPageFilterTemplateAndProperty;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.commons.core.configuration.products.ProductGuidCheckConfiguration;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.models.modernawardslist.ModernAwardPageModel;
import com.nswbc.commons.core.services.ProductCheckGuidService;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * ProductCheckGuidServiceImpl Class.
 *
 * @author riccardo.teruzzi
 */

@Component(immediate = true, service = ProductCheckGuidService.class, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = ProductGuidCheckConfiguration.class)
public class ProductCheckGuidServiceImpl implements ProductCheckGuidService {

    /**
     * The constant GUID.
     */
    public static final String GUID = "guid";
    /**
     * The constant PATH.
     */
    public static final String PATH = "path";

    /**
     * The constant FAILED_TO_CREATE_JSON.
     */
    public static final String FAILED_TO_PROCESS_JSON = "Failed to process JSON";

    /**
     * The constant PRODUCT_PAGE_TEMPLATE.
     */
    public static final String PRODUCT_PAGE_TEMPLATE = "/conf/nswbc-ba/settings/wcm/templates/nswbc-ba-product-page-template";

    /**
     * private variable for log.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ProductCheckGuidServiceImpl.class);

    /**
     * The constant PAGE_LIST.
     */
    private static final String PAGE_LIST = "pageList";

    /**
     * Constant for status.
     */
    private static final String STATUS = "status";

    /**
     * private configuration variable.
     */
    private ProductGuidCheckConfiguration config;

    /**
     * @param config the osgi configuration
     */
    @Activate
    @Modified
    public void activate(ProductGuidCheckConfiguration config) {
        this.config = config;
    }

    /**
     * @return the config
     */
    @Override
    public ProductGuidCheckConfiguration getConfig() {
        return config;
    }

    /**
     * Gets secure page.
     *
     * @param resourceResolver the resource resolver
     * @return the secure page
     */
    @Override
    public Page getSecurePage(ResourceResolver resourceResolver) {
        if (resourceResolver != null) {
            Resource securePageResource = resourceResolver.getResource(getConfig().securePath());
            if (securePageResource != null) {
                return securePageResource.adaptTo(Page.class);
            }
        }
        return null;
    }

    /**
     * @param request  the sling request
     * @param response the sling response
     * @return status of the validation
     */
    @Override
    public JsonObject checkGuidCode(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        String guid = request.getParameter(GUID);
        LOG.debug("guid:" + guid);
        // current page must not be considered
        String currentPagePath = request.getParameter(PATH);
        ResourceResolver resourceResolver = request.getResourceResolver();
        Page securePage = getSecurePage(resourceResolver);
        if (securePage != null) {
            PageFilter modernAwardPageFilter = getPageFilterTemplateAndProperty(PRODUCT_PAGE_TEMPLATE, PRODUCT_GUID,
                    EMPTY);
            List<Page> listOfPageFromIterator = getListOfPageFromIterator(securePage, modernAwardPageFilter, true);
            List<Page> productGuid = listOfPageFromIterator.stream()
                    .filter(page -> !StringUtils.equals(page.getPath(), currentPagePath)
                            && StringUtils.equals(page.getProperties().get(PRODUCT_GUID, String.class), guid))
                    .collect(Collectors.toList());
            if (productGuid.isEmpty()) {
                JsonObject resultJson = new JsonObject();
                resultJson.addProperty(STATUS, Constants.Number.ONE);
                return resultJson;
            } else {
                return RestUtil.getBadRequestResponse(response);
            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

    /**
     * @param securePage
     * @return awards list json
     */
    @Override
    public JsonObject getAwardsList(Page securePage) {
        if (securePage != null) {
            PageFilter modernAwardPageFilter = getPageFilterTemplateAndProperty(PRODUCT_PAGE_TEMPLATE, PRODUCT_GUID,
                    EMPTY);
            Iterator<Page> pageIterator = securePage.listChildren(modernAwardPageFilter, true);
            List<ModernAwardPageModel> modernAwardPageModelList = new ArrayList<>();
            while (pageIterator.hasNext()) {
                Page next = pageIterator.next();
                modernAwardPageModelList.add(next.adaptTo(ModernAwardPageModel.class));
            }
            ObjectMapper objectMapper = new ObjectMapper();
            JsonObject awards = new JsonObject();
            try {
                String jsonString = objectMapper.writeValueAsString(modernAwardPageModelList);
                JsonParser parser = new JsonParser();
                JsonElement tradeElement = parser.parse(jsonString);
                awards.add(PAGE_LIST, tradeElement);
            } catch (JsonProcessingException e) {
                LOG.error(FAILED_TO_PROCESS_JSON, e);
            }
            return awards;
        }
        return new JsonObject();
    }
}
