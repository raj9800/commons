package com.nswbc.commons.core.servlets;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.JsonObject;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.MyAccountChangePasswordService;
import com.nswbc.commons.core.services.MyAccountDeactivateService;
import com.nswbc.commons.core.services.MyAccountMemberDetailsService;
import com.nswbc.commons.core.services.MyAccountTokenValidationService;
import com.nswbc.commons.core.services.MyAccountUpdateMemberDetailsService;

/**
 * @author yuvraj.bansal
 *
 */
@Component(service = Servlet.class, property = {"sling.servlet.selectors=" + MyAccountServlet.VALIDATE_TOKEN,
        "sling.servlet.selectors=" + MyAccountServlet.MEMBER_DETAILS,
        "sling.servlet.selectors=" + MyAccountServlet.UPDATE_MEMBER_DETAILS,
        "sling.servlet.selectors=" + MyAccountServlet.CHANGE_PASSWORD,
        "sling.servlet.selectors=" + MyAccountServlet.MEMBER_DEACTIVATION,
        "sling.servlet.paths=" + MyAccountServlet.SERVLET_PATH, "sling.servlet.extensions=" + Constants.Text.JSON,
        "sling.servlet.methods=GET", "sling.servlet.methods=POST"})
public class MyAccountServlet extends SlingAllMethodsServlet {

    /** generated serial version uid. */
    private static final long serialVersionUID = 7865632056267603993L;
    /** protected variable for servlet path. */
    protected static final String SERVLET_PATH = "/bin/nswbc/myaccount";
    /** protected selector variable for token validation. */
    protected static final String VALIDATE_TOKEN = "validatetoken";
    /** protected selector variable for member details. */
    protected static final String MEMBER_DETAILS = "memberdetails";
    /** protected selector variable to update member details. */
    protected static final String UPDATE_MEMBER_DETAILS = "updatedetails";
    /** protected selector variable to change password. */
    protected static final String CHANGE_PASSWORD = "changepassword";
    /** protected selector variable for member deactivation. */
    protected static final String MEMBER_DEACTIVATION = "memberdeactivate";
    /** private reference variable for myaccount validation service. */
    @Reference
    private MyAccountTokenValidationService validationToken;
    /** private reference variable for myaccount member details service. */
    @Reference
    private MyAccountMemberDetailsService getMemberDetails;
    /**
     * private reference variable for myaccount member deactivation service service.
     */
    @Reference
    private MyAccountDeactivateService deactivateMember;
    /** private reference variable for myaccount member product service. */
    @Reference
    private MyAccountUpdateMemberDetailsService updateMemberDetails;
    /** private reference variable for myaccount change password service. */
    @Reference
    private MyAccountChangePasswordService changeMemberPassword;

    /**
     * doGetMethod to call various services.
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        String selector = request.getRequestPathInfo().getSelectorString();
        if (selector.equals(VALIDATE_TOKEN)) {
            JsonObject result = validationToken.validateToken(request, response);
            printResponseResult(response, result);
        } else if (selector.equals(MEMBER_DETAILS)) {
            JsonObject result = getMemberDetails.getMemberDetails(request, response);
            printResponseResult(response, result);
        }
    }

    /**
     * doPost method for myaccount.
     */
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        String selector = request.getRequestPathInfo().getSelectorString();
        if (selector.equals(UPDATE_MEMBER_DETAILS)) {
            JsonObject result = updateMemberDetails.updateMemberDetails(request, response);
            printResponseResult(response, result);
        } else if (selector.equals(CHANGE_PASSWORD)) {
            JsonObject result = changeMemberPassword.changePassword(request, response);
            printResponseResult(response, result);
        } else if (selector.equals(MEMBER_DEACTIVATION)) {
            JsonObject result = deactivateMember.deactivateMember(request, response);
            printResponseResult(response, result);
        }
    }

    /**
     * @param response
     * @param result
     * @throws IOException This method checks the output result
     */
    private void printResponseResult(SlingHttpServletResponse response, JsonObject result) throws IOException {
        if (Objects.nonNull(result)) {
            response.getWriter().print(result);
        }
    }

}
