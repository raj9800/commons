package com.nswbc.commons.core.services.impl;

import java.util.Objects;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.JsonObject;
import com.nswbc.commons.core.configuration.emailsubscribe.SubscriptionContentAPIServiceConfigurationInstance;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.SubscriptionCampaignEmailCheckService;
import com.nswbc.commons.core.utils.RestUtil;
import com.nswbc.commons.core.utils.ServerSideValidationUtil;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = SubscriptionCampaignEmailCheckService.class)
public class SubscriptionCampaignEmailCheckServiceImpl implements SubscriptionCampaignEmailCheckService {
    /** Constant for email. */
    private static final String EMAIL = "email";
    /** Constant for status. */
    private static final String STATUS = "status";
    /** Constant for cipher padding. */
    private static final String CIPHER_PADDING = "AES/CBC/PKCS5PADDING";
    /**
     * private reference for gated content configuration.
     */
    @Reference
    private SubscriptionContentAPIServiceConfigurationInstance gatedContentConfig;

    /**
     * return json object.
     */
    @Override
    public JsonObject verifyEmail(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        String encryptedEmail = request.getParameter(EMAIL);
        if (Objects.nonNull(encryptedEmail) && StringUtils.isNotEmpty(encryptedEmail)) {
            try {
                IvParameterSpec iv = new IvParameterSpec(gatedContentConfig.getConfig().decryptionInitializationVectorKey().getBytes(
                        Constants.Application.UTF8));
                SecretKeySpec skeySpec = new SecretKeySpec(gatedContentConfig.getConfig().decryptionSecretKey().getBytes(Constants.Application.UTF8),
                        "AES");
                Cipher cipher = Cipher.getInstance(CIPHER_PADDING);
                cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
                byte[] original = cipher.doFinal(DatatypeConverter.parseHexBinary(encryptedEmail));
                String decryptedEmail = new String(original);
                boolean validateEmail = ServerSideValidationUtil.checkEmail(decryptedEmail);
                JsonObject result = new JsonObject();
                if (validateEmail) {
                    result.addProperty(STATUS, Constants.Number.ONE);
                    result.addProperty(EMAIL, decryptedEmail);
                } else {
                    result.addProperty(STATUS, Constants.Number.ZERO);
                }
                return result;
            } catch (Exception ex) {
                return RestUtil.getServerErrorResponse(response);
            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

}
