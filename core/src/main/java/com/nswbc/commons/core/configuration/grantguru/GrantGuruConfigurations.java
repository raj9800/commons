package com.nswbc.commons.core.configuration.grantguru;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author rajat.pachouri Interface for configuration
 *
 */
@ObjectClassDefinition(name = "My Account API Configurations", description = "Service Configuration")
public @interface GrantGuruConfigurations {
    /**
     * @return tokenValidationAPI
     */
    @AttributeDefinition(name = "Grant guru tokens API", description = "Provide the api url for grant guru tokens.")
    String grantGuruTokensAPI();

    /**
     * @return subscriptionkey
     */
    @AttributeDefinition(name = "Subscription Key", description = "Provide the subscription key.")
    String subscriptionKey();

    /**
     * @return tokenValidationAPI
     */
    @AttributeDefinition(name = "Grant guru Redirect API", description = "Provide the redirect api url to grant guru.")
    String grantGuruRedirectAPI();
}
