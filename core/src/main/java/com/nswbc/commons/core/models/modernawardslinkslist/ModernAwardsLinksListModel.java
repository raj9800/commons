package com.nswbc.commons.core.models.modernawardslinkslist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageFilter;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * Modern awards links list component sling model.
 *
 * @author rajat.pachouri
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/modernAwardsLinks",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class ModernAwardsLinksListModel {
    /**
     * The constant GUID.
     */
    public static final String GUID = "guid";
    /**
     * The constant PATH.
     */
    public static final String PATH = "path";

    /**
     * The constant FAILED_TO_CREATE_JSON.
     */
    public static final String FAILED_TO_PROCESS_JSON = "Failed to process JSON";

    /**
     * The constant PRODUCT_PAGE_TEMPLATE.
     */
    public static final String PRODUCT_PAGE_TEMPLATE = "/conf/nswbc-ba/settings/wcm/templates/nswbc-ba-product-page-template";
    /**
     * The constant PRODUCT_GUID.
     */
    public static final String PRODUCT_GUID = "productGuid";
    /**
     * color of the text.
     */
    @Inject
    @Via("resource")
    private String rootPath;

    /** private sling object for resource resolver. */
    @Inject
    private ResourceResolver resourceResolver;

    /**
     * private variable for alphabetmap.
     */
    private HashMap<String, List<ModernAwardPageModel>> alphabetMap;

    /**
     * populate list of all the awards pages under root path.
     */
    public void populateAwardsList(Page rootPage) {
        List<ModernAwardPageModel> modernAwardPageModelList = new ArrayList<>();
        if (Objects.nonNull(rootPage)) {
            PageFilter modernAwardPageFilter = ResourceUtils.getPageFilterTemplateAndProperty(PRODUCT_PAGE_TEMPLATE,
                    PRODUCT_GUID, StringUtils.EMPTY);
            Iterator<Page> pageIterator = rootPage.listChildren(modernAwardPageFilter, true);
            while (pageIterator.hasNext()) {
                Page next = pageIterator.next();
                modernAwardPageModelList.add(next.adaptTo(ModernAwardPageModel.class));
            }
        }
        populateAlphabetMap(modernAwardPageModelList);
    }

    /**
     * returns list of all the awards pages under root path stored within an
     * alphabet map.
     *
     * @return alphabetMap a map with keys as alphabets and items stored as list.
     */
    public HashMap<String, List<ModernAwardPageModel>> getAwardsList() {
        return alphabetMap;
    }

    /**
     * method to get page from rootpath.
     */
    @PostConstruct
    public void init() {
        Resource rootPageResource = resourceResolver.getResource(rootPath);
        if (Objects.nonNull(rootPageResource)) {
            Page rootPage = rootPageResource.adaptTo(Page.class);
            populateAwardsList(rootPage);
        }
    }

    /**
     * Returns sorted modern awards list Lexicographically.
     *
     * @param modernAwardPageModelList pages list
     */
    public void populateAlphabetMap(List<ModernAwardPageModel> modernAwardPageModelList) {
        HashMap<String, List<ModernAwardPageModel>> alphabetMap = new HashMap<>();
        for (int i = 'A'; i <= 'Z'; i++) {
            alphabetMap.put(Character.toString((char) i), new ArrayList<ModernAwardPageModel>());
        }
        for (ModernAwardPageModel page : modernAwardPageModelList) {
            String l = Character.toString(page.getPageTitle().charAt(Constants.Number.ZERO));
            List<ModernAwardPageModel> list = alphabetMap.get(l.toUpperCase());
            list.add(page);
        }
        this.alphabetMap = alphabetMap;
    }
}
