package com.nswbc.commons.core.services.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.commons.core.configuration.myaccount.MyAccountAPIConfigurationsInstance;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.MyAccountTokenValidationService;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = MyAccountTokenValidationService.class)
public class MyAccountTokenValidationServiceImpl implements MyAccountTokenValidationService {
    /** Constant for Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(MyAccountTokenValidationServiceImpl.class);
    /** Constant for Subscription key. */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    /** Constant for status. */
    private static final String STATUS = "status";
    /** Constant for Authorization. */
    private static final String AUTHORIZATION = "Authorization";

    /** private reference variable for my account configuration. */
    @Reference
    private MyAccountAPIConfigurationsInstance myAccountConfig;

    /**
     * validateToken method to validate the id token.
     */
    @Override
    public JsonObject validateToken(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        String token = RestUtil.getTokenFromHeader(request, "token");
        if (StringUtils.isNotEmpty(token)) {
            Map<String, String> headerMap = setHeadersMap(token);
            JsonObject resultJson = new JsonObject();
            CloseableHttpClient client = HttpClientBuilder.create().build();
            StringBuilder builder = new StringBuilder();
            HttpResponse restResponse = RestUtil.processRestGetRequest(client,
                    myAccountConfig.getConfig().tokenValidationAPI(), builder.append(Constants.Application.EMPTY),
                    headerMap);
            if (Objects.nonNull(restResponse)
                    && !(restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED)) {
                if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED) {
                    RestUtil.closeHttpClient(client);
                    return RestUtil.getServerErrorResponse(response);
                } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED_ONE) {
                    RestUtil.closeHttpClient(client);
                    resultJson.addProperty(STATUS, Constants.Number.ZERO);
                    return resultJson;
                } else {
                    try {
                        String responseEntity = EntityUtils.toString(restResponse.getEntity());
                        JsonObject tokenValidationAPIResult = new JsonParser().parse(responseEntity).getAsJsonObject();
                        tokenValidationAPIResult.addProperty(STATUS, Constants.Number.ONE);
                        return tokenValidationAPIResult;
                    } catch (ParseException | IOException e) {
                        LOG.error("Unable to register : ", e.getMessage());
                        return RestUtil.getServerErrorResponse(response);
                    } finally {
                        RestUtil.closeHttpClient(client);
                    }
                }
            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

    /**
     * @return header map in request.
     */
    private Map<String, String> setHeadersMap(String token) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        headerMap.put(SUBSCRIPTION_KEY, myAccountConfig.getConfig().subscriptionKey());
        headerMap.put(AUTHORIZATION, "Bearer " + token);
        return headerMap;
    }
}
