package com.nswbc.commons.core.servlets;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.JsonObject;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.services.AonInsuranceService;

/**
 * This servlet will be used to provide the AON Insurance external page URL so
 * that logged in user can access it.
 *
 * @author ranjeet.singh
 */

@Component(service = Servlet.class,
        property = {"sling.servlet.paths=" + AonInsuranceServlet.SERVLET_PATH,
                "sling.servlet.extensions=" + Constants.Text.JSON,
                "sling.servlet.methods=" + Constants.HTTPMethodNames.GET_METHOD_NAME,
                "sling.servlet.selectors=" + AonInsuranceServlet.SELECTOR_URL})
public class AonInsuranceServlet extends SlingAllMethodsServlet {

    /**
     * generated serial version uid.
     */
    private static final long serialVersionUID = -279615144905300401L;

    /**
     * protected variable for servlet path.
     */
    protected static final String SERVLET_PATH = "/bin/nswbc/aonins";

    /**
     * protected variable for url selector.
     */
    protected static final String SELECTOR_URL = "url";

    /**
     * aonInsuranceService reference.
     */
    @Reference
    private AonInsuranceService aonInsuranceService;

    /**
     * doGet method to call the aon insurance service to call the integration API.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        String selector = request.getRequestPathInfo().getSelectorString();
        if (SELECTOR_URL.equals(selector)) {
            JsonObject result = aonInsuranceService.getAonInsuranceURI(request, response);
            printResponseResult(response, result);
        }
    }

    /**
     * This methods prints the response into the writer.
     *
     * @param response
     * @param result
     * @throws IOException This method checks the output result
     */
    private void printResponseResult(SlingHttpServletResponse response, JsonObject result) throws IOException {
        if (Objects.nonNull(result)) {
            response.getWriter().print(result);
        }
    }

}
