package com.nswbc.commons.core.models.webinarpage;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * @author rajat.pachouri
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/structure/page",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class WebinarPageModel {

    /** private sling object for resource resolver. */
    @SlingObject
    private ResourceResolver resourceResolver;

    /** Script Variable for current page. */
    @Inject
    @Via("resource")
    private String webinarVerificationPage;
    /**
     * Sling request.
     */
    @Inject
    private SlingHttpServletRequest request;
    /**
     * Webinar verification component type.
     */
    private static final String WEBINAR_VERIFICATION_RESOURCE_TYPE = "nswbc-commons/components/content/webinar-verification";

    /**
     * returns redirectlink.
     *
     * @return redirectlink
     */
    public String getRedirectLink() {
        return ResourceUtils.getResolvedPath(resourceResolver, webinarVerificationPage);
    }

    /**
     * This method returns the event title.
     *
     * @return webinar title
     */
    public String getWebinarTitle() {
        Resource comp = ResourceUtils.getResourceByResourceType(request.getResourceResolver(), webinarVerificationPage,
                WEBINAR_VERIFICATION_RESOURCE_TYPE);
        if (null != comp) {
            ValueMap vm = comp.adaptTo(ValueMap.class);
            if (vm.containsKey("eventTitle")) {
                return vm.get("eventTitle").toString();
            }
        }
        return StringUtils.EMPTY;
    }
}
