package com.nswbc.commons.core.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author yuvraj.bansal
 *
 */
public final class ServerSideValidationUtil {
    /** Constant for name regex. */
    private static final String NAME_REGEX = "^[A-Z ]*$";
    /** Constant for email regex. */
    private static final String EMAIL_REGEX = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
    /** Constant for abn regex. */
    private static final String ABN_REGEX = "^[0-9 ]{11}$";
    /** Constant for security code regex. */
    private static final String SECURITY_CODE_REGEX = "^[0-9]{6}$";
    /**
     * default constructor.
     */
    private ServerSideValidationUtil() {
    }

    /**
     * @param name
     * @return if name format is correct
     */
    public static boolean checkName(String name) {
        return validateInput(NAME_REGEX, name);
    }

    /**
     * @param email
     * @return if email format is correct
     */
    public static boolean checkEmail(String email) {
        return validateInput(EMAIL_REGEX, email);
    }

    /**
     * @param abn
     * @return validation status
     */
    public static boolean checkABNField(String abn) {
        return validateInput(ABN_REGEX, abn);
    }

    /**
     * @param securityCode
     * @return security code validation
     */
    public static boolean verifySecurityCode(String securityCode) {
        // TODO Auto-generated method stub
        return validateInput(SECURITY_CODE_REGEX, securityCode);
    }

    /**
     * @param regex
     * @param name
     * @return validation status
     */
    private static boolean validateInput(String regex, String name) {
        Pattern namePattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher match = namePattern.matcher(name);
        if (match.matches()) {
            return true;
        }
        return false;
    }
}
