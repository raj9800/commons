package com.nswbc.commons.core.servlets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.wcm.core.components.models.ListItem;
import com.adobe.cq.wcm.core.components.models.Search;
import com.day.cq.search.PredicateConverter;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.models.utils.SearchResourceItems;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * @author yuvraj.bansal
 *
 */
@Component(service = Servlet.class, property = {"sling.servlet.selectors=" + TagsSearchResultServlet.DEFAULT_SELECTOR,
        "sling.servlet.resourceTypes=cq/Page", "sling.servlet.extensions=json", "sling.servlet.methods=GET"})
public class TagsSearchResultServlet extends SlingSafeMethodsServlet {

    /** generated serial version uid. */
    private static final long serialVersionUID = -6608727472458099049L;
    /** protected constant for servlet selector. */
    protected static final String DEFAULT_SELECTOR = "tagsearch";
    /** protected constant for parameter fulltext. */
    protected static final String PARAM_SEARCH = "tag";
    /** protected constant for parameter fulltext. */
    protected static final String TAG_PREDICATE_VALUE_PROPERTY = "tagid";
    /** protected constant for parameter fulltext. */
    protected static final String TAG_PREDICATE_PROPERTY_NAME = "tagid.property";
    /** protected constant for cq pages tag property. */
    protected static final String CQ_TAG_PROPERTY = "jcr:content/cq:tags";
    /** private constant for predicate offset. */
    private static final String PARAM_RESULTS_OFFSET = "resultsOffset";
    /** private constant for logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(TagsSearchResultServlet.class);

    /** private query builder service variable. */
    @Reference
    private QueryBuilder queryBuilder;
    /** private long variable for totalMatches. */
    private long totalMatches;

    /**
     * do get method.
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        ValueMap contentPolicyMap = getSearchContentResourceValueMap(request);
        List<ListItem> results = getResults(request, contentPolicyMap);
        writeJson(results, response);
    }

    /**
     * @param results
     * @param response This method is used to write json output.
     */
    private void writeJson(List<ListItem> results, SlingHttpServletResponse response) {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        response.setCharacterEncoding(Constants.Application.UTF8);
        JSONObject jsonObject = new JSONObject();
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        try {
            jsonObject.put("totalMatches", getTotalMatches());
            mapper.writeValue(out, results);
            final byte[] data = out.toByteArray();
            jsonObject.put("results", new JSONArray(new String(data)));
            response.getWriter().print(jsonObject);
        } catch (IOException | JSONException e) {
            LOGGER.error(e.getMessage());
        }
    }

    /**
     * @param request
     * @return contentPolicyPoperties valuemap. This method is use to get policy
     *         properties for search resourceType.
     */
    private ValueMap getSearchContentResourceValueMap(SlingHttpServletRequest request) {
        ValueMap contentPolicyProperties = new ValueMapDecorator(new HashMap<>());
        Resource resource = request.getResource();
        if (Objects.nonNull(resource)) {
            PageManager pageManager = resource.getResourceResolver().adaptTo(PageManager.class);
            if (pageManager != null) {
                contentPolicyProperties = ResourceUtils.getPolicyDetails(request,
                        Constants.Application.SEARCH_RESOURCE_TYPE);
            }
        }
        return contentPolicyProperties;
    }

    /**
     * @param request
     * @param contentPolicyMap
     * @return result list. This method is use to get result list from query.
     */
    private List<ListItem> getResults(SlingHttpServletRequest request, ValueMap contentPolicyMap) {
        int searchTermMinimumLength = Constants.Number.THREE;
        int resultsSize = Constants.Number.TEN;
        searchTermMinimumLength = contentPolicyMap.get(Search.PN_SEARCH_TERM_MINIMUM_LENGTH, Constants.Number.THREE);
        resultsSize = contentPolicyMap.get(Search.PN_RESULTS_SIZE, Constants.Number.TEN);
        String searchRoot = contentPolicyMap.get(Search.PN_SEARCH_ROOT, Constants.Application.NSWBC_CONTENT_ROOT);
        List<ListItem> results = new ArrayList<>();
        String tagValue = request.getParameter(PARAM_SEARCH);
        if (tagValue == null || tagValue.length() < searchTermMinimumLength) {
            return results;
        }
        long resultsOffset = 0;
        if (request.getParameter(PARAM_RESULTS_OFFSET) != null) {
            resultsOffset = Long.parseLong(request.getParameter(PARAM_RESULTS_OFFSET));
        }
        Map<String, String> predicatesMap = new HashMap<>();
        predicatesMap.put(TAG_PREDICATE_VALUE_PROPERTY, tagValue);
        predicatesMap.put(TAG_PREDICATE_PROPERTY_NAME, CQ_TAG_PROPERTY);
        predicatesMap.put(Constants.Application.PREDICATE_PATH, searchRoot);
        predicatesMap.put(Constants.Application.PREDICATE_TYPE, NameConstants.NT_PAGE);
        PredicateGroup predicates = PredicateConverter.createPredicates(predicatesMap);
        ResourceResolver resourceResolver = request.getResource().getResourceResolver();
        Query query = queryBuilder.createQuery(predicates, resourceResolver.adaptTo(Session.class));
        if (resultsSize != 0) {
            query.setHitsPerPage(resultsSize);
        }
        if (resultsOffset != 0) {
            query.setStart(resultsOffset);
        }
        SearchResult searchResult = query.getResult();
        long totalMatches = searchResult.getTotalMatches();
        setTotalMatches(totalMatches);
        List<Hit> hits = searchResult.getHits();
        if (hits != null) {
            for (Hit hit : hits) {
                try {
                    Resource hitRes = hit.getResource();
                    Page page = getPage(hitRes);
                    if (page != null) {
                        results.add(new SearchResourceItems(request, page));
                    }
                } catch (RepositoryException e) {
                    LOGGER.error("Unable to retrieve search results for query.", e);
                }
            }
        }
        return results;
    }

    /**
     * @param totalMatches set totalMatches.
     */
    private void setTotalMatches(long totalMatches) {
        this.totalMatches = totalMatches;
    }

    /**
     * @return total no. of matches.
     */
    private String getTotalMatches() {
        return Long.toString(this.totalMatches);
    }

    /**
     * @param resource
     * @return page This method is use to get page from resource.
     */
    private Page getPage(Resource resource) {
        if (resource != null) {
            ResourceResolver resourceResolver = resource.getResourceResolver();
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            if (pageManager != null) {
                return pageManager.getContainingPage(resource);
            }
        }
        return null;
    }
}
