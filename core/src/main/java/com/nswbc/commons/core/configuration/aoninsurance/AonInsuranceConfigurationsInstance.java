package com.nswbc.commons.core.configuration.aoninsurance;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;

/**
 * Component of the Aon Insurance configuration Instance. author ranjeet.singh
 */

@Component(immediate = true, service = AonInsuranceConfigurationsInstance.class,
        configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = AonInsuranceConfigurations.class)
public class AonInsuranceConfigurationsInstance {

    /**
     * private configurations.
     */
    private AonInsuranceConfigurations config;

    /**
     * This method will run when the component is activated.
     *
     * @param config
     */
    @Activate
    public void activate(AonInsuranceConfigurations config) {
        this.config = config;
    }

    /**
     * This method returns the configuration.
     *
     * @return config
     */
    public AonInsuranceConfigurations getConfig() {
        return config;
    }
}
