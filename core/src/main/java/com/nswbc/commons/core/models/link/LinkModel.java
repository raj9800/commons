package com.nswbc.commons.core.models.link;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * @author yuvraj.bansal. Link Model Class.
 */
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class LinkModel {

    /**
     * Resource resolver object.
     */
    @Inject
    private ResourceResolver resourceResolver;
    /**
     * private inject variable for linkType.
     */
    @ValueMapValue
    @Default(values = "primary")
    private String linkType;

    /**
     * private inject variable for linkLabel.
     */
    @ValueMapValue
    @Default(values = "")
    private String linkLabel;

    /**
     * private inject variable for linkTo.
     */
    @ValueMapValue
    @Default(values = "")
    private String linkTo;

    /**
     * private inject variable for linkLabelColor.
     */
    @ValueMapValue
    @Default(values = "")
    private String linkLabelColor;

    /**
     * private inject variable for linkTarget.
     */
    @ValueMapValue
    @Default(values = "_self")
    private String linkTarget;

    /**
     * private inject variable for button alignment.
     */
    @Inject
    @Via("resource")
    @Default(values = "left")
    private String buttonAlign;

    /**
     * @return linkType
     */
    public String getButtonType() {
        return linkType;
    }

    /**
     * @return linkType
     */
    public String getLinkType() {
        return linkType;
    }

    /**
     * @return linkLabel
     */
    public String getLinkLabel() {
        return linkLabel;
    }

    /**
     * @return linkTo
     */
    public String getLinkTo() {
        return ResourceUtils.getResolvedPath(resourceResolver, linkTo);
    }

    /**
     * @return linkLabelColor
     */
    public String getLinkLabelColor() {
        return linkLabelColor;
    }

    /**
     * @return linkTarget
     */
    public String getLinkTarget() {
        return linkTarget;
    }

    /**
     * @return button alignment
     */
    public String getButtonAlign() {
        return buttonAlign;
    }

}
