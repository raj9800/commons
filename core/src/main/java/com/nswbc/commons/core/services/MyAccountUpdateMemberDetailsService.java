package com.nswbc.commons.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author yuvraj.bansal
 *
 */
public interface MyAccountUpdateMemberDetailsService {
    /**
     * @param request
     * @param response
     * @return member details
     */
    JsonObject updateMemberDetails(SlingHttpServletRequest request, SlingHttpServletResponse response);
}
