package com.nswbc.commons.core.constants;

/**
 * @author yuvraj.bansal
 *
 */
public final class Constants {
    /**
     * final class for application.
     *
     */
    public static final class Application {
        /** public constant for application/ecmascript. */
        public static final String APPLICATION_ECMASCRIPT = "application/ecmascript";
        /** public constant for application/javascript. */
        public static final String APPLICATION_JAVASCRIPT = "application/javascript";
        /** public constant for application/json. */
        public static final String APPLICATION_JSON = "application/json";
        /** public constant for application/pdf. */
        public static final String APPLICATION_PDF = "application/pdf";
        /** public constant for application/zip. */
        public static final String APPLICATION_ZIP = "application/zip";
        /** public constant for nswbc content root. */
        public static final String NSWBC_CONTENT_ROOT = "/content/nswbc";
        /** public constant for search resource type. */
        public static final String SEARCH_RESOURCE_TYPE = "nswbc-commons/components/content/search";
        /** public constant for display tags resource type. */
        public static final String DISPLAYTAG_RESOURCE_TYPE = "nswbc-commons/components/content/tagDisplay";
        /** private constant for predicate fulltext. */
        public static final String PREDICATE_FULLTEXT = "fulltext";
        /** private constant for predicate type. */
        public static final String PREDICATE_TYPE = "type";
        /** private constant for predicate path. */
        public static final String PREDICATE_PATH = "path";
        /** private constant for predicate property. */
        public static final String PREDICATE_PROPERTY = "property";
        /** private constant for predicate property. */
        public static final String PREDICATE_PROPERTY_VALUE = "property.value";
        /** public constant for utf-8. */
        public static final String UTF8 = "utf-8";
        /** public constant for breadcrumb resource type. */
        public static final String BREADCRUMB_RESOURCE_TYPE = "nswbc-commons/components/content/breadcrumb";
        /** public constant for question mark. */
        public static final String QUESTION_MARK = "?";
        /** public constant for cq:tags. */
        public static final String CQ_TAGS = "cq:tags";
        /** public constant for pageCategory. */
        public static final String PAGE_CATEGORY = "pageCategory";
        /** public constant for acs commons category list. */
        public static final String SEARCH_CATEGORIES_LIST_PATH = "/etc/acs-commons/lists/bc-category-list";
        /** public constant for acs commons category list. */
        public static final String EMPTY = "";
        /** public constant for content-type. */
        public static final String CONTENT_TYPE = "Content-type";
        /** public constant for equal. */
        public static final String EQUAL = "=";
        /** public constant for ampersand. */
        public static final String AMPERSAND = "&";
        /** public constant for hyphen. */
        public static final String HYPHEN = "-";

        /**
         * private constaructor for application class.
         */
        private Application() {
        }
    }

    /**
     * final class for image.
     *
     */
    public static final class Image {
        /** public constant for image bmp. */
        public static final String BMP = "image/bmp";
        /** public constant for image gif. */
        public static final String GIF = "image/gif";
        /** public constant for image jpeg. */
        public static final String JPEG = "image/jpeg";
        /** public constant for image png. */
        public static final String PNG = "image/png";
        /** public constant for image svg xml. */
        public static final String SVG_XML = "image/svg+xml";
        /** public constant for image tiff. */
        public static final String TIFF = "image/tiff";
        /** public constant for image webp. */
        public static final String WEBP = "image/webp";

        /**
         * private constructor for image class.
         */
        private Image() {
        }
    }

    /**
     * final class for text.
     *
     */
    public static final class Text {
        /** public constant for text/csv. */
        public static final String TEXT_CSV = "text/csv";
        /** public constant for text/html. */
        public static final String TEXT_HTML = "text/html";
        /** public constant for text/plain. */
        public static final String TEXT_PLAIN = "text/plain";
        /** public constant for json. */
        public static final String JSON = "json";
        /** public constant for html. */
        public static final String HTML = "html";

        /**
         * private constructor for text class.
         */
        private Text() {
        }
    }

    /**
     * final class for video.
     *
     */
    public static final class Video {
        /** public constant for video mp4. */
        public static final String MP4 = "video/mp4";
        /** public constant for video mpeg. */
        public static final String MPEG = "video/mpeg";

        /**
         * private constructor for video class.
         */
        private Video() {
        }
    }

    /**
     * final class for numbers.
     *
     */
    public static final class Number {
        /** private constant for number 0. */
        public static final int ZERO = 0;
        /** private constant for number 1. */
        public static final int ONE = 1;
        /** private constant for number 2. */
        public static final int TWO = 2;
        /** private constant for number 3. */
        public static final int THREE = 3;
        /** private constant for number 4. */
        public static final int FOUR = 4;
        /** private constant for number 5. */
        public static final int FIVE = 5;
        /** private constant for number 6. */
        public static final int SIX = 6;
        /** private constant for number 7. */
        public static final int SEVEN = 7;
        /** private constant for number 8. */
        public static final int EIGHT = 8;
        /** private constant for number 9. */
        public static final int NINE = 9;
        /** private constant for number 10. */
        public static final int TEN = 10;
        /** private constant for number 11. */
        public static final int ELEVEN = 11;
        /** private constant for number 400. */
        public static final int FOUR_HUNDRED = 400;
        /** private constant for number 200. */
        public static final int TWO_HUNDRED = 200;
        /** private constant for number 204. */
        public static final int TWO_HUNDRED_FOUR = 204;
        /** private constant for number 500. */
        public static final int FIVE_HUNDRED = 500;
        /** private constant for number 404. */
        public static final int FOUR_HUNDRED_FOUR = 404;
        /** private constant for number 401. */
        public static final int FOUR_HUNDRED_ONE = 401;

        /**
         * private constructor for numbers.
         */
        private Number() {
        }
    }

    /**
     * final class for extensions.
     *
     */
    public static final class Extensions {
        /** private constant for json extension. */
        public static final String JSON_EXTENSION = ".json";
        /** private constant for html extension. */
        public static final String HTML_EXTENSION = ".html";

        /**
         * private constructor for numbers.
         */
        private Extensions() {
        }
    }

    /**
     * final class for HTTPMethodNames.
     *
     */
    public static final class HTTPMethodNames {
        /** private constant for GET method name. */
        public static final String GET_METHOD_NAME = "GET";
        /** private constant for POST method name. */
        public static final String POST_METHOD_NAME = "POST";
        /** private constant for PUT method name. */
        public static final String PUT_METHOD_NAME = "PUT";
        /** private constant for HEAD method name. */
        public static final String HEAD_METHOD_NAME = "HEAD";

        /**
         * private constructor for HTTPMethodNames.
         */
        private HTTPMethodNames() {
        }
    }
}
