package com.nswbc.commons.core.models.pianopage;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.nswbc.commons.core.configuration.paywall.PianoConfigurationInstance;

/**
 * Piano Page model. This model is used to inject piano script on the page.
 *
 * @author rajat.pachouri
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/tagDisplay",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class PianoPageModel {

    /** private sling object for resource resolver. */
    @SlingObject
    private ResourceResolver resourceResolver;

    /** Script Variable for current page. */
    @Inject
    @Via("resource")
    private String isPremium;

    /** scriptAPI variable. */
    private String scriptAPI;

    /**
     * private inject variable for PianoConfigurationInstance.
     */
    @Inject
    private PianoConfigurationInstance pianoConfig;

    /**
     * init model method.
     */
    @PostConstruct
    private void init() {
        scriptAPI = pianoConfig.getConfig().scriptAPI();
    }

    /**
     * scriptApi getter.
     *
     * @return scriptAPI
     */
    public String getScriptAPI() {
        return scriptAPI;
    }

    /**
     * getter for isPremium.
     *
     * @return isPremium
     */
    public String getIsPremium() {
        return isPremium;
    }
}
