package com.nswbc.commons.core.configuration.googlemap;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;

/**
 * @author rajat.pachouri
 *
 */
@Component(immediate = true, service = GoogleMapConfigurationsInstance.class,
        configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = GoogleMapConfigurations.class)
public class GoogleMapConfigurationsInstance {
    /** private configuration variable. */
    private GoogleMapConfigurations config;

    /**
     * @param config
     */
    @Activate
    public void activate(GoogleMapConfigurations config) {
        this.config = config;
    }

    /**
     * @return the config
     */
    public GoogleMapConfigurations getConfig() {
        return config;
    }
}
