
package com.nswbc.commons.core.modelinjectors;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.spi.DisposalCallbackRegistry;
import org.apache.sling.models.spi.Injector;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;

import com.day.cq.wcm.api.Page;
import com.nswbc.commons.core.modelinjectors.utils.InjectorUtils;
import com.nswbc.commons.core.modelinjectors.utils.ReflectionUtil;

/**
 * HierarchyNodeInheritanceResource implementations.
 *
 * @author rajat.pachouri
 *
 */
@Component(property = { Constants.SERVICE_RANKING + "=5503" }, service = Injector.class)
public class HierarchyNodeInheritanceResourceInjections implements Injector {
    /**
     * unique name id to inject all the direct child resources.
     */
    private static final String ALL_CHILDRENS = "all-childrens";

    /**
     * Returns the name of the injector.
     */
    @Override
    public String getName() {
        return HierarchyNodeInheritanceResource.SOURCE;
    }

    /**
     * This method returns the value for the injection.
     */
    @Override
    public Object getValue(Object adaptable, String name, Type declaredType, AnnotatedElement element,
            DisposalCallbackRegistry callbackRegistry) {
        if (!element.isAnnotationPresent(HierarchyNodeInheritanceResource.class)) {
            return null;
        }
        Page currentPage = null;
        Resource currentResource = InjectorUtils.getResource(adaptable);
        String resourcePath = currentResource.getPath().substring(
                currentResource.getPath().indexOf(JcrConstants.JCR_CONTENT) + JcrConstants.JCR_CONTENT.length());
        ResourceResolver resolver = InjectorUtils.getResourceResolver(adaptable);
        if (adaptable instanceof SlingHttpServletRequest) {
            currentPage = InjectorUtils.getCurrentPage(adaptable);
        } else if (adaptable instanceof Resource) {
            currentPage = InjectorUtils.getResourcePage(adaptable);
        }

        if (name.equals(ALL_CHILDRENS)) {
            ArrayList<Resource> list = new ArrayList<Resource>();
            // String[] arr = null;
            list = getAllDirectChildResources(list, currentPage, resourcePath, resolver);
            if (list.size() > 0) {
                return list;
            }

        }

        // To get String Array
        if (ReflectionUtil.isArray(declaredType)) {
            ArrayList<String> list = new ArrayList<String>();
            list = getArray(list, currentPage, name, resourcePath, resolver);
            return list.toArray(new String[list.size()]);
        }
        // To get list of child of child resource
        if (ReflectionUtil.isListType(declaredType)) {
            ArrayList<Resource> list = new ArrayList<Resource>();
            list = getList(list, currentPage, name, resourcePath, resolver);
            return list;
        }
        // To get child resource
        if (((Class<?>) declaredType).isAssignableFrom(Resource.class)) {
            Resource childResource = null;
            childResource = getChildResource(childResource, currentPage, name, resourcePath, resolver);
            return childResource;
        }
        // To get single values property from valuemap
        if (((Class<?>) declaredType).isAssignableFrom(String.class)) {
            String property = StringUtils.EMPTY;
            property = getProperty(property, currentPage, name, resourcePath, resolver);
            return property;
        }
        return null;
    }

    /**
     * This method returns a single valued property string value.
     *
     * @param property     to store the value
     * @param page         current page
     * @param name         Name of the property
     * @param resourcePath path of the resource
     * @param resolver     resourceResolver
     * @return property String property
     */
    private String getProperty(String property, Page page, String name, String resourcePath,
            ResourceResolver resolver) {
        String compPath = page.getPath() + "/jcr:content" + resourcePath;
        Resource res = resolver.getResource(compPath);
        boolean compExist = false;
        if (!Objects.isNull(res)) {
            if (StringUtils.isBlank(property) && res.getValueMap().containsKey(name)) {
                compExist = true;
                return res.getValueMap().get(name, StringUtils.EMPTY);
            }
        }
        if (!compExist && null != page.getParent()) {
            property = getProperty(property, page.getParent(), name, resourcePath, resolver);
        }
        return property;
    }

    /**
     * This method returns all the direct children of resource.
     *
     * @param list         list to store all the child resources
     * @param page         current page
     * @param resourcePath path of the resource
     * @param resolver     resourceResolver
     * @return child resource
     */
    private ArrayList<Resource> getAllDirectChildResources(ArrayList<Resource> list, Page page, String resourcePath,
            ResourceResolver resolver) {
        String compPath = page.getPath() + "/jcr:content" + resourcePath;
        Resource res = resolver.getResource(compPath);
        boolean compExist = false;
        if (!Objects.isNull(res)) {
            if (list.isEmpty() && res.hasChildren()) {
                compExist = true;
                for (Resource childRes : res.getChildren()) {
                    list.add(childRes);
                }
                return list;
            }
        }
        if (!compExist && null != page.getParent()) {
            list = getAllDirectChildResources(list, page.getParent(), resourcePath, resolver);
        }
        return list;
    }

    /**
     * This method returns a child resource.
     *
     * @param resource     resource variable to store returned resource
     * @param page         current page
     * @param name         Name of the child resource
     * @param resourcePath path of the resource
     * @param resolver     resourceResolver
     * @return child resource
     */
    private Resource getChildResource(Resource resource, Page page, String name, String resourcePath,
            ResourceResolver resolver) {
        String compPath = page.getPath() + "/jcr:content" + resourcePath;
        Resource res = resolver.getResource(compPath);
        boolean compExist = false;
        if (!Objects.isNull(res)) {
            if (Objects.isNull(resource) && !Objects.isNull(res.getChild(name))) {
                compExist = true;
                return res.getChild(name);
            }
        }
        if (!compExist && null != page.getParent()) {
            resource = getChildResource(resource, page.getParent(), name, resourcePath, resolver);
        }
        return resource;
    }

    /**
     * This method returns a multi valued property string array.
     *
     * @param list         list to store the values
     * @param page         current page
     * @param name         Name of the property
     * @param resourcePath path of the resource
     * @param resolver     resourceResolver
     * @return String property
     */
    private ArrayList<String> getArray(ArrayList<String> list, Page page, String name, String resourcePath,
            ResourceResolver resolver) {
        String compPath = page.getPath() + "/jcr:content" + resourcePath;
        Resource res = resolver.getResource(compPath);
        boolean compExist = false;
        if (null != res) {
            if (list.size() == 0 && res.getValueMap().containsKey(name)) {
                compExist = true;
                String[] arr = res.getValueMap().get(name, String[].class);
                list = new ArrayList<String>(Arrays.asList(arr));
                return list;
            }
        }
        if (!compExist && null != page.getParent()) {
            list = getArray(list, page.getParent(), name, resourcePath, resolver);
        }
        return list;
    }

    /**
     * This method returns a list of child resources under a given resource.
     *
     * @param list         list to store the resources
     * @param page         current page
     * @param name         Name of the property
     * @param resourcePath path of the resource
     * @param resolver     resourceResolver
     * @return String property
     */
    private ArrayList<Resource> getList(ArrayList<Resource> list, Page page, String name, String resourcePath,
            ResourceResolver resolver) {
        String compPath = page.getPath() + "/jcr:content" + resourcePath;
        Resource res = resolver.getResource(compPath);
        boolean compExist = false;
        if (null != res) {
            Resource childRes = res.getChild(name);
            if (list.size() == 0 && null != childRes && childRes.hasChildren()) {
                compExist = true;
                for (Resource r : childRes.getChildren()) {
                    list.add(r);
                }
                return list;
            }
        }
        if (!compExist && null != page.getParent()) {
            list = getList(list, page.getParent(), name, resourcePath, resolver);
        }
        return list;
    }
}
