package com.nswbc.commons.core.models.button;

import com.nswbc.commons.core.models.link.LinkModel;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import javax.inject.Inject;

/**
 * This is the model class for button component.
 * author ranjeet.singh
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-commons/components/content/button",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = { "json" })
public class ButtonModel {

    /**
     * request object.
     */
    @SlingObject
    private Resource resource;

    /**
     * Injecting linkVariant variable.
     */
    @Inject @Via("resource")
    @Default(values = "button-normal")
    private String linkVariant;

    /**
     * private linkModel variable.
     */
    @Self
    private LinkModel linkModel;

    /**
     * Getter method of linkVariant.
     * @return linkVariant
     */

    public String getLinkVariant() {
        return linkVariant;
    }

    /**
     * Getter method of linkModel.
     * @return linkModel
     */
    public LinkModel getLinkModel() {
        return linkModel;
    }

}
