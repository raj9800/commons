package com.nswbc.commons.core.configuration.webinarUserVerification;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * Interface for Webinar user verification configurations.
 *
 * @author rajat.pachouri
 */

@ObjectClassDefinition(name = "Webinar User verification API Configurations",
        description = "This config has endpoint for webinar user verification API")
public @interface WebinarUserVerificationConfigurations {

    /**
     * @return webinarUserVerificationAPI.
     */
    @AttributeDefinition(name = "User verification API", description = "Provide the User verification API endpoint.")
    String webinarUserVerificationAPI();

}
