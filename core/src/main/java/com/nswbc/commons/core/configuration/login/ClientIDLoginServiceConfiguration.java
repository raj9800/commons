package com.nswbc.commons.core.configuration.login;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.osgi.service.metatype.annotations.Option;

/**
 * @author yuvraj.bansal Interface for configuration
 *
 */
@ObjectClassDefinition(name = "Client ID Service Configuration", description = "Service Configuration")
public @interface ClientIDLoginServiceConfiguration {
    /**
     * @return brandName
     */
    @AttributeDefinition(
            options = {
                    @Option(label = "BA", value = "ba"),
                    @Option(label = "Business NSW", value = "businessnsw")
            })
    String brandName() default "";

    /**
     * @return envHostname
     */
    @AttributeDefinition(name = "Environment Hostname", description = "Provide the hostname of the aem environment.")
    String envHostname();

    /**
     * @return apiHostname
     */
    @AttributeDefinition(name = "WebAPI Hostname", description = "Provide the hostname of the web api.")
    String apiHostname();

    /**
     * @return clientID
     */
    @AttributeDefinition(name = "Client ID", description = "Provide the clientID for login js.")
    String clientID();

    /**
     * @return authorityID
     */
    @AttributeDefinition(name = "Authority ID", description = "Provide the authorityID for login js.")
    String authorityID();

    /**
     * @return post redirect home page path
     */
    @AttributeDefinition(name = "Post Redirect Home Page Path", description = "Provide the home page url for secure pages.")
    String postRedirectHomePage();

    /**
     * @return hop page path
     */
    @AttributeDefinition(name = "Hop Page Path", description = "Provide the hop page url for secure pages.")
    String hopPagePath();

    /**
     * @return allowed redirect domains
     */
    @AttributeDefinition(name = "Allowed Redirection Domains", description = "Provide the domains which are allowed to redirect.")
    String[] allowedRedirectDomains();

    /**
     * @return webconsole namehint
     */
    String webconsole_configurationFactory_nameHint() default "Login Endpoint <b>{brandName}</b>";

}
