package com.nswbc.commons.core.servlets;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestDispatcherOptions;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.WCMMode;
import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * This servlet adds the functionality of 301 redirection on top the 302
 * redirection provided by Adobe.
 *
 * @author rajat.pachouri
 *
 */
@Component(service = Servlet.class,
        property = {Constants.SERVICE_RANKING + ":Integer=100", "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.extensions=html", "sling.servlet.selectors=redirect",
                "sling.servlet.resourceTypes=cq/Page"})

public class PageRedirectServlet extends SlingSafeMethodsServlet {
    private static final long serialVersionUID = 1L;
//    @Property(cardinality = 2147483647, label = "Excluded resource types",
//            description = "List of resource types which should be ignored by this redirect servlet.")
//    protected static final String PROPERTY_EXCLUDED_RESOURCE_TYPES = "excluded.resource.types";
//    /**
//     * Variable to store excluded resource types.
//     */
//    private Set<String> excludedResourceTypes;
    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(PageRedirectServlet.class);
    /**
     * wcmmode param.
     */
    private static final String WCM_MODE_PARAM = "wcmmode";

//    /**
//     * @param properties
//     */
//    @Activate
//    protected void activate(Map<String, Object> properties) {
//        String[] excludedResourceTypesArray = PropertiesUtil.toStringArray(properties.get("excluded.resource.types"));
//        this.excludedResourceTypes = new HashSet<String>();
//        if (!ArrayUtils.isEmpty(excludedResourceTypesArray)) {
//            Collections.addAll(this.excludedResourceTypes, excludedResourceTypesArray);
//        }
//    }

    /**
     * do get method.
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        Resource resource = request.getResource();
        Resource contentResource = resource.getChild("jcr:content");
        if (contentResource != null) {
            String redirectTarget = getRedirectTarget(contentResource);
            String redirectType = getRedirectType(contentResource);
//          below line can be used to extend the functionality to support excluded resourceType
//          if (isRedirectRequest(request, redirectTarget) && !isExcludedResourceType(contentResource)) {
            if (isRedirectRequest(request, redirectTarget)) {
                if (!isExternalRedirect(redirectTarget)) {
                    // replaced default map logic with nswbc map logic.
                    // Note: default logic doesnt work and takes an additional 'request' object.
//                  redirectTarget = resource.getResourceResolver().map(request, redirectTarget) + ".html";
                    redirectTarget = ResourceUtils.getResolvedPath(resource.getResourceResolver(), redirectTarget);
                }
                redirectTarget = appendWcmModeQueryParameter(request, redirectTarget);
                // setting the header in case of 301 else redirect for 302
                if (redirectType.equals("301")) {
                    LOG.debug("Redirecting page {} to target {} with 301", resource.getPath(), redirectTarget);
                    response.setStatus(HttpStatus.SC_MOVED_PERMANENTLY);
                    response.setHeader("Location", redirectTarget);
                } else {
                    LOG.debug("Redirecting page {} to target {} with 302", resource.getPath(), redirectTarget);
                    response.sendRedirect(redirectTarget);
                }
                return;
            }
        }
        RequestDispatcherOptions requestDispatcherOptions = new RequestDispatcherOptions();
        String selectorString = request.getRequestPathInfo().getSelectorString();
        selectorString = StringUtils.replace(selectorString, "redirect", "");
        requestDispatcherOptions.setReplaceSelectors(selectorString);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher(contentResource, requestDispatcherOptions);
        if (requestDispatcher != null) {
            requestDispatcher.include(request, response);
        }
    }

    /**
     * To return the redirected Type.
     *
     * @param resource
     * @return redirectType - http status
     */
    private String getRedirectType(Resource resource) {
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        String redirectType = "";
        if (valueMap != null) {
            redirectType = valueMap.get("redirectType", "");
        }
        return redirectType;
    }

//    /**
//     * This method is used to check if the current resource is excluded.
//     * @param contentResource
//     * @return true if resource is excluded
//     */
//    private boolean isExcludedResourceType(Resource contentResource) {
//        for (String excludedResourceType : this.excludedResourceTypes) {
//            if (contentResource.isResourceType(excludedResourceType)) {
//                return true;
//            }
//        }
//        return false;
//    }

    /**
     * @param request
     * @param redirectTarget
     * @return redirectTarget
     */
    private String appendWcmModeQueryParameter(SlingHttpServletRequest request, String redirectTarget) {
        if (isModeDisabledChangeRequest(request)) {
            redirectTarget = redirectTarget + (redirectTarget.contains("?") ? "&" : "?") + WCM_MODE_PARAM + "=disabled";
        }
        return redirectTarget;
    }

    /**
     * @param request
     * @return isModeChangeRequest
     */
    private boolean isModeDisabledChangeRequest(SlingHttpServletRequest request) {
        boolean isModeChangeRequest = false;
        String modeChange = request.getParameter(WCM_MODE_PARAM);
        if (StringUtils.equalsIgnoreCase(modeChange, WCMMode.DISABLED.name())) {
            isModeChangeRequest = true;
        }
        return isModeChangeRequest;
    }

    /**
     * @param redirectTarget
     * @return externalRedirect
     */
    private boolean isExternalRedirect(String redirectTarget) {
        boolean externalRedirect = false;
        try {
            URL url = new URL(redirectTarget);
            String protocol = url.getProtocol();
            if (StringUtils.isNotBlank(protocol)) {
                externalRedirect = true;
            }
        } catch (MalformedURLException e) {
            return false;
        }
        return externalRedirect;
    }

    /**
     * @param resource
     * @return redirectTarget the redirectTarget page
     */
    private String getRedirectTarget(Resource resource) {
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        String redirectTarget = "";
        if (valueMap != null) {
            redirectTarget = valueMap.get("cq:redirectTarget", "");
        }
        return redirectTarget;
    }

    /**
     * @param request
     * @param redirectTarget
     * @return true if it is redirecttarget
     */
    private boolean isRedirectRequest(SlingHttpServletRequest request, String redirectTarget) {
        WCMMode wcmMode = WCMMode.fromRequest(request);
        return (StringUtils.isNotEmpty(redirectTarget) && wcmMode.equals(WCMMode.DISABLED));
    }
}
