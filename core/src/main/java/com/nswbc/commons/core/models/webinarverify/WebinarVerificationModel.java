package com.nswbc.commons.core.models.webinarverify;

import java.util.Calendar;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.nswbc.commons.core.utils.ResourceUtils;

/**
 * This is the model class for Webinar verification component.
 */
@Model(adaptables = SlingHttpServletRequest.class,
        resourceType = "nswbc-commons/components/content/webinar-verification",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class WebinarVerificationModel {
    /**
     * Resource resolver object.
     */
    @Inject
    private ResourceResolver resourceResolver;
    /**
     * Injecting eventID variable.
     */
    @Inject
    @Via("resource")
    private String eventID;
    /**
     * Injecting webinarPage variable.
     */
    @Inject
    @Via("resource")
    private String webinarPage;
    /**
     * Injecting section title variable.
     */
    @Inject
    @Via("resource")
    private String sectionTitle;
    /**
     * Injecting eventTitle variable.
     */
    @Inject
    @Via("resource")
    private String eventTitle;
    /**
     * Injecting earlySubtitle variable.
     */
    @Inject
    @Via("resource")
    private String earlySubtitle;
    /**
     * Injecting lateSubtitle variable.
     */
    @Inject
    @Via("resource")
    private String lateSubtitle;
    /**
     * Injecting successMessage variable.
     */
    @Inject
    @Via("resource")
    private String successMessage;
    /**
     * Injecting earlyMessage variable.
     */
    @Inject
    @Via("resource")
    private String earlyMessage;
    /**
     * Injecting lateMessage variable.
     */
    @Inject
    @Via("resource")
    private String lateMessage;
    /**
     * Injecting isEventFinished variable.
     */
    @Inject
    @Via("resource")
    private String isEventFinished;
    /**
     * Injecting eventDate variable.
     */
    @Inject
    @Via("resource")
    private Calendar eventDate;

    /**
     * Injecting heading variable.
     */
    @Inject
    @Via("resource")
    private String heading;
    /**
     * Injecting subheading variable.
     */
    @Inject
    @Via("resource")
    private String subheading;
    /**
     * Injecting headingEventFinished variable.
     */
    @Inject
    @Via("resource")
    private String headingEventFinished;
    /**
     * Injecting subheadingEventFinished variable.
     */
    @Inject
    @Via("resource")
    private String subheadingEventFinished;
    /**
     * Injecting buttonLabel variable.
     */
    @Inject
    @Via("resource")
    private String buttonLabel;

    /**
     * getter for resource.
     *
     * @return resource
     */
    public String getEventID() {
        return eventID;
    }

    /**
     * getter for webinarPage.
     *
     * @return webinarPage
     */
    public String getWebinarPage() {
        return ResourceUtils.getResolvedPath(resourceResolver, webinarPage);
    }

    /**
     * getter for sectionTitle.
     *
     * @return sectionTitle
     */
    public String getSectionTitle() {
        return sectionTitle;
    }

    /**
     * getter for eventTitle.
     *
     * @return eventTitle
     */
    public String getEventTitle() {
        return eventTitle;
    }

    /**
     * getter for earlySubtitle.
     *
     * @return earlySubtitle
     */
    public String getEarlySubtitle() {
        return earlySubtitle;
    }

    /**
     * getter for lateSubtitle.
     *
     * @return lateSubtitle
     */
    public String getLateSubtitle() {
        return lateSubtitle;
    }

    /**
     * getter for successMessage.
     *
     * @return successMessage
     */
    public String getSuccessMessage() {
        return successMessage;
    }

    /**
     * getter for earlyMessage.
     *
     * @return earlyMessage
     */
    public String getEarlyMessage() {
        return earlyMessage;
    }

    /**
     * getter for lateMessage.
     *
     * @return lateMessage
     */
    public String getLateMessage() {
        return lateMessage;
    }

    /**
     * getter for isEventFinished.
     *
     * @return isEventFinished
     */
    public String getIsEventFinished() {
        return isEventFinished;
    }

    /**
     * getter for eventDate.
     *
     * @return eventDate
     */
    public Calendar getEventDate() {
        return eventDate;
    }

    /**
     * getter for heading.
     *
     * @return heading
     */
    public String getHeading() {
        return heading;
    }

    /**
     * getter for subheading.
     *
     * @return subheading
     */
    public String getSubheading() {
        return subheading;
    }

    /**
     * getter for headingEventFinished.
     *
     * @return headingEventFinished
     */
    public String getHeadingEventFinished() {
        return headingEventFinished;
    }

    /**
     * getter for subheadingEventFinished .
     *
     * @return subheadingEventFinished
     */
    public String getSubheadingEventFinished() {
        return subheadingEventFinished;
    }

    /**
     * getter for buttonLabel .
     *
     * @return buttonLabel
     */
    public String getButtonLabel() {
        return buttonLabel;
    }
}
