(function ($, $document) {
    $.validator.register("foundation.validation.validator", {
        selector: "#timelineItems coral-multifield-item input[is='coral-textfield']",
        validate: function(el) {
            var regex =new RegExp('^[\\d]{4}$');
            if(el.val() == ""){
				return "Please enter the year";
            }else if(!regex.test(el.val())){
                return "Please enter the year in 4 digits format";
            }
        }});
})($, $(document));