// Login() variables
var envHostname, apiHostname, clientID, authorityID, postLogoutRedirectUri, hopPageUri;
var appConfig = {};
var msalConfig = {};
var msalConfigPR = {};
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var data = JSON.parse(this.response);
        envHostname = data.envHostname;
        apiHostname = data.apiHostname;
        clientID = data.clientID
        authorityID = data.authorityID;
        postLogoutRedirectUri = data.postRedirectHomePage;
        hopPageUri = data.hopPagePath;
    }
};
xhttp.open("GET", "/bin/nswbc/loginclient." + brandName + ".json", false);
xhttp.send();
// Login() variables
// -----------------------------------------------------------
// Login JS on page load
// -----------------------------------------------------------
appConfig = {
    b2cScopes: ["https://" + envHostname + ".onmicrosoft.com/integration/api"],
    webApi: "https://" + apiHostname + ".nswbc.com.au/api/member/v4/profile"
};

// configuration to initialize msal for login
msalConfig = {
    auth: {
        clientId: clientID, // This is your client ID
        authority: "https://" + envHostname + ".b2clogin.com/" + authorityID + "/B2C_1_SI",
        validateAuthority: false
    },
    cache: {
        cacheLocation: "localStorage",
        storeAuthStateInCookie: true
    }

};

//configuration to initialize msal for forgot pwd
msalConfigPR = {
    auth: {
        clientId: clientID, // This is your client ID
        authority: "https://" + envHostname + ".b2clogin.com/" + authorityID + "/B2C_1_FP", 
        validateAuthority:false,
        redirectUri: postLogoutRedirectUri
    },
    cache: {
        cacheLocation: "localStorage",
        storeAuthStateInCookie: true
    }
    
};