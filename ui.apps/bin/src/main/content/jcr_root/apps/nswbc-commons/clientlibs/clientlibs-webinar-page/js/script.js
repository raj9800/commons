// to get value from the URL params
function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
			function(m,key,value) {
		vars[key] = value;
	});
	return vars;
}
// function to delete cookie
var deleteCookie = function(name) {
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};
var webinarUserLoggedInStatusCookie = "webinar.user.loggedin";

function verifyWebinarUser() {
var redirectPath = document.querySelector("meta[name='data-webinar-redirect-path']").getAttribute('content');
var redirectURI = document.querySelector("meta[name='data-webinar-redirect-url']").getAttribute('content');
var email = getUrlVars()["email"] == undefined ? "":getUrlVars()["email"];

// making a request to API
var webinarRequest = $.ajax({
	url: "/bin/nswbc/webinarverify.json",
	type: "post",
	contentType: 'application/json;charset=utf-8',
	dataType: 'json',
    async: false,
	data: JSON.stringify({"redirecturi":redirectPath,"Email":email})
});

// Callback handler that will be called on success
webinarRequest.done(function (response, textStatus, jqXHR){
	if(response["status"]==0 ) {
		location.href= document.querySelector("meta[name='data-webinar-redirect-url']").getAttribute('content');
	}
});

// Callback handler that will be called on failure
webinarRequest.fail(function (jqXHR, textStatus, errorThrown){
	console.log("redirecting");
	location.href= document.querySelector("meta[name='data-webinar-redirect-url']").getAttribute('content');  
});
}
// this function deleted cookie if the user has come from webinar page else tries to verify the user.
if(getCookie(webinarUserLoggedInStatusCookie)) {
	deleteCookie(webinarUserLoggedInStatusCookie);
} else {
	verifyWebinarUser();
}