(function ($, $document, nsw_workflow) {

    "use strict";
    // this is to avoid running code multiple times when the dialog is opened without refreshing page.
    if(!nsw_workflow) {

        // method to display notification is the date is already passed.
        function displayNotification(date, field, message) {
			var isBefore = date.isBefore(new Date());
            if (isBefore)
                field.after("<coral-alert variant=\"warning\">"+ message+"</coral-alert>");
        }

        // to toggle name on datepickers which handles saving or just showing the value.
        function toggleName(element) {
            element.name= element.name? "":element.dataset['name'];
        }

        //to toggle disabled and required state.
        function toggleRequiredAndDisabled(field) {
            var api = field.adaptTo("foundation-field");
            api.setDisabled(!api.isDisabled());
            api.setRequired(!api.isRequired());
        }

        // take payload path and other part of the URL from the dialog and make ajax request and populate user.
        $(".cq-inbox-dialog-injection-anchor").on("foundation-contentloaded", function(e){
            var pl = $($(e.target).find('.custom')).data('payloadpath');
            var select = $($(e.target).find('#user-select'))['0'];
            var url = $(select).data("userurl");
            if(url) {
                $.getJSON(url+pl, function( data ) {
                    data.items.forEach(function(value, index) {

                        select.items.add(value);
                    });
                });
            }
            var actchk = $(".ovract");
            var deactchk = $(".ovrdeact");
            var actDateField = $(".actdate");
            var deactDateField = $(".deactdate");

			var valueFormat = "DD/MM/YYYY - HH:mmZ";
            var displayFormat = "DD MMMM YYYY LT";

            var deactHist = $(".external-dialog-injection").data("deactdate");
            var actHist = $(".external-dialog-injection").data("actdate");
            var passedMessage = "<coral-alert-content>Above time has already passed</coral-alert-content>";


			// when checkbox is checked if adds name of the datepickers which allows the saving of date.
            if(actchk){
                actchk.on("change", function(e){
                    toggleName(document.querySelector(".actdate"));
					toggleRequiredAndDisabled(actDateField);
                });
            }
            if(deactchk){
                deactchk.on("change", function(e){
                    toggleName(document.querySelector(".deactdate"));
                    toggleRequiredAndDisabled(deactDateField);
                });
            }

            // This populates the time given in the previous steps of the workflow.
            // Dont remove setTimeout as it break population on all the browsers except chrome.
			setTimeout(function(){
                if(deactDateField.size() && deactHist) {
                var deactHistDate = moment(deactHist, valueFormat);
                document.querySelector(".deactdate").valueAsDate = deactHistDate.toDate();
                // displays notification if time has passed.
                displayNotification(deactHistDate, deactDateField, passedMessage);
            }
            if(actDateField.size() && actHist) {
                var actHistDate = moment(actHist, valueFormat);
                document.querySelector(".actdate").valueAsDate = actHistDate.toDate();
               	// displays notification if time has passed.
                displayNotification(actHistDate, actDateField, passedMessage);
            }
            }, 100);

        });

    }
    window.nsw_workflow = nsw_workflow || true;
})($, $(document), window.nsw_workflow);