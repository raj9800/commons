(function ($, window)
{
    var registry =  $(window).adaptTo("foundation-registry");
    registry.register("foundation.validation.validator",{
       	selector: "[data-validation=h1Length]",
        validate: function (el)
        {
            var element = $(el);
            if(element.find("h1").length > 0){
              if(element.find("h1").text().length > 40){
                return "Please enter less than 40 characters for h1";
              }else{
                return;
              }
            }
            

        } 
	});
    /* Registering validator against the selector registered previously - ends*/

})
($, window);