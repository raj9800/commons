if($("#piano").length) {
	console.log("piano loaded");
	var uri = $("#piano").data("piano-api");

	(function(src) {
		var a = document.createElement("script");
		a.type = "text/javascript";
		a.async = true;
		a.src = src;
		var b = document.getElementsByTagName("script")[0];
		b.parentNode.insertBefore(a, b)
	})(uri);
	tp = window.tp || [];

	tp.push(["setDebug", false]);
	tp.push(["setUseTinypassAccounts", false]);
	tp.push(["setUsePianoIdUserProvider", false ]);
	tp.push(["setUsePianoIdLiteUserProvider", true ]);
	$(document).ready(function(){

		tp.push(["setExternalJWT", localStorage.getItem("msal.idtoken")]);
		tp.push(["addHandler", "loginRequired", function (params) {
			if(tp.user.isUserValid()) {
				// Continuing checkout
				tp.offer.startCheckout(params);
			} else {
				console.log("user not valid");
			}
			return false;
		}]);

		tp.push(["addHandler", "checkoutComplete", function(conversion){ 
			console.log("checkoutcomplete");
			var postCheckoutRefirect = getUrlParameter("post_checkout_redirect");
			console.log(postCheckoutRefirect);
			if(postCheckoutRefirect) {
				location.href = getUrlParameter("post_checkout_redirect");
			}
			// Your code after successful purchase
		}]); 
	});
	// function to get query params
	function getUrlParameter(sParam) {
		var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		sURLVariables = sPageURL.split('&'),
		sParameterName,i;
		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : sParameterName[1];
			}
		}
	}

}