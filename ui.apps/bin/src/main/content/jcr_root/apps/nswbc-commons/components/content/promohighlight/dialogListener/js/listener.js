(function ($, $document) {
	"use strict";
	// when dialog gets injected
	$(document).on("foundation-contentloaded", function(e) {
		// hiding the rich text if plain text is selected
		Coral.commons.ready($(".subheading-drop"), function(element) {
			if($(element).val() == 'ptext') {
				$(".rtext").parent().hide();
			}
		});
	});
	$document.on("change", ".subheading-drop", function(e) {

		if($(e.target).val() == "ptext") {           
			$(".rtext").parent().hide();
		} else {
			$(".rtext").parent().show();
		}
	})
})($, $(document));