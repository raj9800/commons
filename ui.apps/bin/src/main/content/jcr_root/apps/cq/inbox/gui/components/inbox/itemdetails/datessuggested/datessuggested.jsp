<%--
  ADOBE CONFIDENTIAL

  Copyright 2017 Adobe Systems Incorporated
  All Rights Reserved.

  NOTICE:  All information contained herein is, and remains
  the property of Adobe Systems Incorporated and its suppliers,
  if any.  The intellectual and technical concepts contained
  herein are proprietary to Adobe Systems Incorporated and its
  suppliers and may be covered by U.S. and Foreign Patents,
  patents in process, and are protected by trade secret or copyright law.
  Dissemination of this information or reproduction of this material
  is strictly forbidden unless prior written permission is obtained
  from Adobe Systems Incorporated.
--%><%@page session="false"
          import="com.adobe.granite.security.user.UserProperties,
          com.adobe.granite.security.user.UserPropertiesManager,
          com.adobe.granite.workflow.WorkflowSession,
          com.adobe.granite.workflow.exec.HistoryItem,
          com.adobe.granite.workflow.metadata.MetaDataMap,
          com.adobe.granite.workflow.exec.WorkItem,
          com.adobe.granite.workflow.exec.Workflow,
          com.adobe.granite.security.user.util.AuthorizableUtil,
          com.day.cq.i18n.I18n,
          org.apache.commons.lang3.StringUtils,
          org.apache.sling.api.resource.Resource,
          org.apache.sling.api.wrappers.ValueMapDecorator,
		  org.apache.jackrabbit.api.security.user.Group,
          javax.jcr.RepositoryException,
		  org.slf4j.Logger,
          org.slf4j.LoggerFactory,
          java.util.LinkedHashMap,
          java.util.Map,
          java.util.HashMap,
          java.util.HashSet,
          java.util.Date,
          java.util.Collection,
          java.util.Collections,
          java.util.Iterator,
          java.util.List"%>
<%@include file="/libs/granite/ui/global.jsp"%><%
 	Logger LOG = LoggerFactory.getLogger(getClass());
    Workflow wfinfo = (Workflow) request.getAttribute("cq.inbox.workflowinfo");

    if (wfinfo==null) {
        // no item specified -> fail fast.
        return;
    }

    Map<String, ValueMapDecorator> commentHash = new LinkedHashMap<String, ValueMapDecorator>();

    WorkflowSession workflowSession = resourceResolver.adaptTo(WorkflowSession.class);
    UserPropertiesManager userPropertiesManager = resourceResolver.adaptTo(UserPropertiesManager.class);

    String hiddenCommentSwitch = "disabled";

    //Get WorkFlow Start Comment
    String startcomment = wfinfo.getMetaDataMap().get("startComment", String.class);

    if (StringUtils.isNotEmpty(startcomment)) {  //Add the start comment if it exists
        String userId = wfinfo.getInitiator();
        Date timeStarted = wfinfo.getTimeStarted();
        String key = userId + "_startcomment";

        //commentHash.put(key,getValueMap(false,userPropertiesManager,i18n,userId,timeStarted,i18n.get("Start"),startcomment,null));
    }


// getting dates from workflow history items
    if (wfinfo.getWorkflowModel() != null) {
        int i = 1;
        // loading history fails if the model wasn't found for this item
        List<HistoryItem> hist = workflowSession.getHistory(wfinfo);
        for (HistoryItem histItem : hist) {
            //i++;
	     if(histItem.getWorkItem().getNode().getType().equals("DYNAMIC_PARTICIPANT")) {
	        	MetaDataMap map = histItem.getWorkItem().getMetaDataMap();
                String deactdate = StringUtils.EMPTY;
		        	String actdate = StringUtils.EMPTY;
		        	if(map.containsKey("deactdate")) {
		        		deactdate = map.get("deactdate",String.class);
		        	}
		        	if(map.containsKey("actdate")) {
		        		actdate = map.get("actdate",String.class);
		        	}
	            //CQ-4242875 - only getComments from CompletedWorkflow Items to avoid duplicates //todo: Update comment
             //String workItemComment = histItem.getComment();
	            String histAction = histItem.getAction();
	
	            if (StringUtils.isNotEmpty(actdate) || StringUtils.isNotEmpty(deactdate)) { // a comment exists
	                WorkItem wi = histItem.getWorkItem();
	                String userId= histItem.getUserId();
	                Date timeEnded = wi.getTimeEnded();
	                String title = wi.getNode().getTitle();
	                String nodeId = wi.getId();

	                String key = Integer.toString(i++);
	                boolean isHidden = !HISTORYACTION_WORKFLOWCOMPLETED.equals(histAction); //Any non-workflowCompleted workflow event will be hidden by default

	                if (isHidden)
	                    hiddenCommentSwitch = "";
	
	                commentHash.put(key, getValueMap(isHidden, userPropertiesManager,i18n,userId,timeEnded,title,actdate,deactdate,histAction));
	          }
	     }
      }
   }


/* 
    // getting comments for workflow history items
    if (wfinfo.getWorkflowModel() != null) {
        // loading history fails if the model wasn't found for this item
        List<HistoryItem> hist = workflowSession.getHistory(wfinfo);
        for (HistoryItem histItem : hist) {
            //CQ-4242875 - only getComments from CompletedWorkflow Items to avoid duplicates //todo: Update comment
            String workItemComment = histItem.getComment();
            String histAction = histItem.getAction();

            if (StringUtils.isNotEmpty(workItemComment)) { // a comment exists
                WorkItem wi = histItem.getWorkItem();
                String userId= histItem.getUserId();
                Date timeEnded = wi.getTimeEnded();
                String title = wi.getNode().getTitle();
                String nodeId = wi.getId();

                String key = workItemComment + title;
                boolean isHidden = !HISTORYACTION_WORKFLOWCOMPLETED.equals(histAction); //Any non-workflowCompleted workflow event will be hidden by default

                if (isHidden)
                    hiddenCommentSwitch = "";

                commentHash.put(key, getValueMap(isHidden, userPropertiesManager,i18n,userId,timeEnded,title,workItemComment,histAction));
            }
        }
    } */

    if (commentHash.size()!=0) {
            for (String key : commentHash.keySet()) {
                    ValueMapDecorator commentItem = commentHash.get(key);
                    boolean isHidden = commentItem.containsKey("hidden");

                    String xssPhotoPath = xssAPI.filterHTML((String) commentItem.get("photo"));
                    String xssCommentLabel = xssAPI.filterHTML((String) commentItem.get("commentLabel"));
                    String actdate = (String) commentItem.get("actdate");
                	String deactdate = (String) commentItem.get("deactdate");

                %>


                <section>
                    <div class="task-comment-icon">
                        <%= (String) commentItem.get("photo") %>
                    </div>
                    <div class="smallText task-comment-text">
                        <div><%= xssCommentLabel%></div>
                    </div>

                    <div class="task-comment-balloon">
                        <% if(!actdate.equals("")) { 
                    %> <coral-alert size="S" variant="info">
                      <coral-alert-header>Activation Date</coral-alert-header>
                      <coral-alert-content>
                        <%=actdate%>
                      </coral-alert-content>
                    </coral-alert>
                    <% } %>
                    <% if(!deactdate.equals("")) { 
                    %> <coral-alert size="S" variant="error">
                      <coral-alert-header>Deactivation Date</coral-alert-header>
                      <coral-alert-content>
                        <%=deactdate%>
                      </coral-alert-content>
                    </coral-alert>
                    <% } %>
                    </div>
                </section>
                <% } %><script>setDefaultCommentSwitch();</script>
        <%
    } else { %>
        <section><%=i18n.get("No dates have been suggested for this workkflow yet.")%></section>
    <% } %><%!

    private static final String HISTORYACTION_WORKFLOWCOMPLETED = "WorkflowCompleted";

    private String getCommentLabel(String userName, Date date,I18n i18n, String labelSuffix) {
        String stepTitle = "";
        if (StringUtils.isNotEmpty(labelSuffix)) {
            stepTitle = " - " + i18n.getVar(labelSuffix);
        }
        return i18n.get("{0} by <span class=\"task-comment-user\">{1}</span> ", "example: {5 days ago} by {Alison Parker}","<foundation-time format=\"short\" type=\"datetime\" value=\""+date.toInstant().toString()+"\"></foundation-time>" , userName) + stepTitle;
    }

    private ValueMapDecorator getUserInfo(String userId, UserPropertiesManager userPropertiesManager) throws RepositoryException {
        String thumbnail = "<img src='/libs/cq/inbox/content/inbox/images/user.png'>";
        ValueMapDecorator  vm = new ValueMapDecorator(new HashMap<String, Object>());
        UserProperties profile = null;

        profile = AuthorizableUtil.getProfile(userPropertiesManager, userId);

        if (profile != null) {
            userId = profile.getDisplayName();
            Resource photo = profile.getResource(UserProperties.PHOTOS);
            if (photo != null) {
                thumbnail = "<img src='" + photo.getPath() + "/primary/image.prof.thumbnail.36.png'>";
            }
        }

        vm.put("userId",userId);
        vm.put("photo",thumbnail);


        return vm;
    }

	private boolean getGroupIds(Iterator<Group> groups, String groupName) throws RepositoryException {
        while (groups.hasNext()) {
        	Group group = groups.next();
        	if(StringUtils.isNotBlank(group.getID()) && group.getID().equals(groupName)) { 
            	return true;
            }
        }
        return false;
    }
    private ValueMapDecorator getValueMap(boolean isHidden, UserPropertiesManager userPropertiesManager,I18n i18n, String userId, Date commentTime, String workItemStep, String actdate, String deactdate, String action) throws RepositoryException {
        ValueMapDecorator commentVM = new ValueMapDecorator(new HashMap<String, Object>());
        ValueMapDecorator userVM = getUserInfo(userId,userPropertiesManager);
        String displayUserId = (String) userVM.get("userId");
        String photo = (String) userVM.get("photo");
        String commentLabel = getCommentLabel(displayUserId,commentTime,i18n,workItemStep);
        commentVM.put("userId",userId);
        commentVM.put("photo", photo);
        commentVM.put("commentLabel", commentLabel);
		commentVM.put("actdate", actdate);
        commentVM.put("deactdate", deactdate);
        commentVM.put("step",workItemStep);
        commentVM.put("action",action);
        if (isHidden)
            commentVM.put("hidden",null);
        return commentVM;
    }
%>

