var securePagePath = location.pathname.match(/^\/secure\//gi);
var securePagePathInAuthor = location.pathname.match(/^\/content\/nswbc\/ba\/en\/home\/secure\//gi);
//redirect to home page for secure pages
if(securePagePath !== null || securePagePathInAuthor !== null) {
    msalConfig.auth.postLogoutRedirectUri = postLogoutRedirectUri;
}

//defining global client app object 
var clientApplication = new Msal.UserAgentApplication(msalConfig);
// Register a call back for redirect flow
clientApplication.handleRedirectCallback(onAuthCallback);

//call back function which will be triggered when the request comes from login or forgot password screen
function onAuthCallback(errorDesc, token, error, tokenType) {
    errorDesc = errorDesc + '';
}
//get a cookie
function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
//function to login the user
function loginMsal(email) {
	  var loginRequest = {
	    scopes: appConfig.b2cScopes
	  };
	  if (typeof email !== 'undefined') {
	    loginRequest.extraQueryParameters = {
	      email: email
	    };
	  }
	  clientApplication.loginRedirect(loginRequest);
	}
//function to logout
function logout() {
    // Removes all sessions, need to call AAD endpoint to do full logout
    document.token = null;
    document.cookie = "access_token=" + ";path=/";
    clientApplication.logout();
}
//function to forgot password
function forgotPassword(email) {
    var loginRequestPR = {
        scopes: appConfig.b2cScopes
    };
    if (typeof email !== 'undefined'){
        loginRequestPR.extraQueryParameters = {email:email}
    }
    var clientApplicationPR = new Msal.UserAgentApplication(msalConfigPR);
    // Register a call back for redirect flow
    clientApplicationPR.handleRedirectCallback(onAuthCallback);
    clientApplicationPR.loginRedirect(loginRequestPR);
  }
//function to get access token
function checkAccessToken() {
    var tokenRequest = {
        scopes: appConfig.b2cScopes
    }
    clientApplication.acquireTokenSilent(tokenRequest).then(function(tokenResponse) {
        var access_token = tokenResponse.accessToken;
        document.cookie = "access_token=" + access_token +";path=/";
    }).catch(function(error) {
    	document.cookie = "access_token=" + ";path=/";
        location.href = hopPageUri;
    })
}

