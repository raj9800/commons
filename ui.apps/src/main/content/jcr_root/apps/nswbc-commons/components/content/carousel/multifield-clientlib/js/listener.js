(function ($, $document) {
    "use strict";
	// Validator for component selector coral-multifield and Normal Multifield
    /*
     * 
    Instructions to use(component selector multifield):
    1. add granite:class{String} : "comp-selector" on parent column container (not on items node)
    2. add granite:data node on the container and add properties cs-min{String} = "value" and cs-max{String} = "value" 
    for min and max no of panels allowed respectively
    
    Instructions to use(normal multifield):
    1. add granite:data node on multifield and add properties min{String} = "value" and max{String} = "value" 
    for min and max no of panels allowed respectively
    */
    $.validator.register("foundation.validation.validator", {
        selector: "coral-multifield",
        validate: function(el) {

            var totalPanels = el["0"].items.getAll().length;
            // logic for component selector multifield
            var cont = $(el).parent().hasClass("comp-selector")? $(el).parent() : undefined;
            var min;
            var max;
            var dropdownValue = $(".coral-Form-field.carouselType.coral3-Select").val();
            if(dropdownValue === "default") {
                if(cont) {
                    min = cont.data("cs-min") ? cont.data("cs-min"): undefined;
                    max = cont.data("cs-max") ? cont.data("cs-max"): undefined;
                    if(min && totalPanels < min) {
                        return "Minimum numbers of panels required are: " + min;
                    }
                    if(max && totalPanels > max) {
                        return "Maximum numbers of panels allowed are: " + max;
                    }
                }

            }
                // Logic for normal multifield validation
                if ($(el).data("min")){
                    min = $(el).data("min");
                    if(totalPanels < min) {
                        return "Minimum numbers of items required are: " + min;
                    }
                }
                if ($(el).data("max")){
                    max = $(el).data("max");
                    if(totalPanels > max) {
                        return "Maximum numbers of items allowed are: " + max;
                    }
                }
        }});
})($, $(document));