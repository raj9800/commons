<%@page import="org.apache.sling.api.resource.Resource,
                org.apache.sling.api.resource.ValueMap,
                org.apache.sling.api.resource.ResourceUtil,
                com.day.cq.wcm.webservicesupport.Configuration,
                com.day.cq.wcm.webservicesupport.ConfigurationManager" %>
<%@include file="/libs/foundation/global.jsp" %><%

String[] services = pageProperties.getInherited("cq:cloudserviceconfigs", new String[]{});

ConfigurationManager cfgMgr = resource.getResourceResolver().adaptTo(ConfigurationManager.class);
if(cfgMgr != null) {
    // for adobe launch.
    Configuration launchCfg = cfgMgr.getConfiguration("adobe-launch", services);
    if(launchCfg != null) {
        String script = launchCfg.get("script", null);
        if(script != null) {
             %><%= script %><%
        }
    }
    // for My account.
    Configuration cfg = cfgMgr.getConfiguration("myaccount", services);
    if(cfg != null) {
        String script = cfg.get("script", null);
        if(script != null) {
            %><%= script %><%
        }
    }
}%>