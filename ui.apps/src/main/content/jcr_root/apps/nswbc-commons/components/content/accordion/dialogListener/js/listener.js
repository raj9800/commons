(function ($, $document) {
        "use strict";
    	$document.on("click", ".cq-dialog-submit", function (){
              if($("#accordionItems").find("coral-multifield-item").length > 0){
					$("#accordionItems").find("coral-multifield-item").each(function(){
                        if($(this).find(".cmp-childreneditor__item-title").val()  == "" ){
							$(this).find(".cmp-childreneditor__item-title").attr("aria-required","true");
                        }
                    });
              }
        });
})($, $(document));